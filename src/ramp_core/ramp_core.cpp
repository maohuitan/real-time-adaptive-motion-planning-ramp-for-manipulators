/*
 * ramp_core.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_core/ramp_core.h" 

rampCore::rampCore()
{
	ROS_INFO("*********************************************************");
	ROS_INFO("Real-time Adaptive Motion Planning of Robot Manipulators");
	ROS_INFO("*********************************************************");
  
	srand(time(NULL));
	stats.num_of_ctrl_cyc = 0;
	stats.num_of_plan_cyc = 0;  
	stats.num_of_offline_plan_cyc = 0;
	stats.ramp_initialization_time = 0.;
	stats.ramp_offline_run_time = 0.;
	stats.ramp_online_run_time = 0.;
	stats.cum_planning_cyc_time = 0.;
	stats.cum_ctrl_cyc_time = 0.;
	stats.num_of_traj_switches = 0;
	stats.traj_switch_due_to_infeas = 0;
	stats.traj_switch_due_to_better_traj = 0;
	stats.num_of_ctrl_cyc_running_ntc_traj = 0;
	stats.num_of_ctrl_cyc_running_tc_traj = 0;
	stats.num_of_ins_chg_oper = 0;
	stats.num_of_success_ins_chg_oper = 0;
	ramp_params.init_traj_knot_size = 0;
	ramp_params.min_offline_iterations = 0;
	ramp_params.max_offline_iterations = 0;
	ramp_params.control_frequency = 0.0;
	ramp_params.traj_energy_weight = 0.0;
	ramp_params.traj_energy_nrm_factor = 0.0;
	ramp_params.traj_time_weight = 0.0;
	ramp_params.traj_time_nrm_factor = 0.0;
	ramp_params.traj_manipulability_weight = 0.0;
	ramp_params.traj_manipulability_nrm_factor = 0.0;
	ramp_params.traj_task_cstr_weight = 0.0;
	ramp_params.traj_task_cstr_nrm_factor = 0.0;
	ramp_params.traj_smoothness_weight = 0.0;
	ramp_params.traj_smoothness_nrm_factor = 0.0;
	ramp_params.traj_collison_checking_time_resolution = 0.0;
	ramp_params.traj_evaluate_time_resolution = 0.0;
	ramp_params.traj_evaluate_max_time = 0.0;
	ramp_params.traj_collison_checking_max_time = 0.0;
	ramp_params.population_collison_checking_max_time = 0.0;
	ramp_params.traj_jnt_vel_max_scale = 0.0;
	ramp_params.verbose = false;
	ramp_params.is_hardware_enabled = false;
	ramp_params.use_published_velocity = false;
	ramp_params.is_online_run = false;
	ramp_params.task_constraint_tolerance = 0.0;
	ramp_params.on_task_cstr_time = 0.;

	manipulate_obj_obstacle_status = false;

	first_time_ntc1 = true;
	start_ctrl_cyc = 0;

	loadParameters();  

	robot_state_topic = ramp_params.robot_state_topic;
	robot_state_sub = nh.subscribe(robot_state_topic, 1, &rampCore::robotStateCallback, this);

	if (ramp_params.is_hardware_enabled)
	{
		/* for collecting data from hardware */
		robot_hw_state_sub = nh.subscribe("/arm1_hw/joint_states", 1, &rampCore::robotStateHardWareCallback, this);
	}
}

void rampCore::loadParameters()
{
	std::cout<<"\nLoading parameters\n";
	std::cout<<"\nNode Handle NS: "<<nh.getNamespace();

	if(nh.hasParam("/ramp/test_case_name")) 
	{
		nh.getParam("/ramp/test_case_name", ramp_params.test_case_name);
		std::cout <<"\ntest_case_name: "<< ramp_params.test_case_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/test_case_name");

	if(nh.hasParam("/ramp/population_size")) 
	{
		nh.getParam("/ramp/population_size", ramp_params.population_size);
		std::cout <<"\npopulation_size: ";
		for (int i=0; i<ramp_params.population_size.size(); i++)
			std::cout << ramp_params.population_size.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/population_size");

	if(nh.hasParam("/ramp/population_name")) 
	{
		nh.getParam("/ramp/population_name", ramp_params.population_name);
		std::cout <<"\npopulation_name: ";
		for (int i=0; i<ramp_params.population_name.size(); i++)
			std::cout << ramp_params.population_name.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/population_name");

	if(nh.hasParam("/ramp/population_type")) 
	{
		nh.getParam("/ramp/population_type", ramp_params.population_type);
		std::cout <<"\npopulation_type: ";
		for (int i=0; i<ramp_params.population_type.size(); i++)
			std::cout << ramp_params.population_type.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/population_type");

	if(nh.hasParam("/ramp/init_traj_knot_size")) 
	{
		nh.getParam("/ramp/init_traj_knot_size", ramp_params.init_traj_knot_size);
		std::cout <<"\ntraj_knot_size: "<< ramp_params.init_traj_knot_size;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/init_traj_knot_size");

	if(nh.hasParam("/ramp/min_offline_iterations")) 
	{
		nh.getParam("/ramp/min_offline_iterations", ramp_params.min_offline_iterations);
		std::cout <<"\nmin_offline_iterations: "<< ramp_params.min_offline_iterations;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/min_offline_iterations");

	if(nh.hasParam("/ramp/max_offline_iterations")) 
	{
		nh.getParam("/ramp/max_offline_iterations", ramp_params.max_offline_iterations);
		std::cout <<"\nmax_offline_iterations: "<< ramp_params.max_offline_iterations;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/max_offline_iterations");

	if(nh.hasParam("/ramp/control_frequency")) 
	{
		nh.getParam("/ramp/control_frequency", ramp_params.control_frequency);
		std::cout <<"\ncontrol_frequency: "<< ramp_params.control_frequency;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/control_frequency");

	if(nh.hasParam("/ramp/traj_energy_weight")) 
	{
		nh.getParam("/ramp/traj_energy_weight", ramp_params.traj_energy_weight);
		std::cout <<"\ntraj_energy_weight: "<< ramp_params.traj_energy_weight;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_energy_weight");

	if(nh.hasParam("/ramp/traj_energy_nrm_factor")) 
	{
		nh.getParam("/ramp/traj_energy_nrm_factor", ramp_params.traj_energy_nrm_factor);
		std::cout <<"\ntraj_energy_nrm_factor: "<< ramp_params.traj_energy_nrm_factor;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_energy_nrm_factor");

	if(nh.hasParam("/ramp/traj_time_weight")) 
	{
		nh.getParam("/ramp/traj_time_weight", ramp_params.traj_time_weight);
		std::cout <<"\ntraj_time_weight: "<< ramp_params.traj_time_weight;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_time_weight");

	if(nh.hasParam("/ramp/traj_time_nrm_factor")) 
	{
		nh.getParam("/ramp/traj_time_nrm_factor", ramp_params.traj_time_nrm_factor);
		std::cout <<"\ntraj_time_nrm_factor: "<< ramp_params.traj_time_nrm_factor;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_time_nrm_factor");

	if(nh.hasParam("/ramp/traj_manipulability_weight")) 
	{
		nh.getParam("/ramp/traj_manipulability_weight", ramp_params.traj_manipulability_weight);
		std::cout <<"\ntraj_manipulability_weight: "<< ramp_params.traj_manipulability_weight;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_manipulability_weight");

	if(nh.hasParam("/ramp/traj_manipulability_nrm_factor")) 
	{
		nh.getParam("/ramp/traj_manipulability_nrm_factor", ramp_params.traj_manipulability_nrm_factor);
		std::cout <<"\ntraj_manipulability_nrm_factor: "<< ramp_params.traj_manipulability_nrm_factor;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_manipulability_nrm_factor");

	if(nh.hasParam("/ramp/traj_task_cstr_weight")) 
	{
		nh.getParam("/ramp/traj_task_cstr_weight", ramp_params.traj_task_cstr_weight);
		std::cout <<"\ntraj_task_cstr_weight: "<< ramp_params.traj_task_cstr_weight;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_task_cstr_weight");

	if(nh.hasParam("/ramp/traj_task_cstr_nrm_factor")) 
	{
		nh.getParam("/ramp/traj_task_cstr_nrm_factor", ramp_params.traj_task_cstr_nrm_factor);
		std::cout <<"\ntraj_task_cstr_nrm_factor: "<< ramp_params.traj_task_cstr_nrm_factor;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_task_cstr_nrm_factor");

	if(nh.hasParam("/ramp/traj_smoothness_weight")) 
	{
		nh.getParam("/ramp/traj_smoothness_weight", ramp_params.traj_smoothness_weight);
		std::cout <<"\ntraj_smoothness_weight: "<< ramp_params.traj_smoothness_weight;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_smoothness_weight");

	if(nh.hasParam("/ramp/traj_smoothness_nrm_factor")) 
	{
		nh.getParam("/ramp/traj_smoothness_nrm_factor", ramp_params.traj_smoothness_nrm_factor);
		std::cout <<"\ntraj_smoothness_nrm_factor: "<< ramp_params.traj_smoothness_nrm_factor;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_smoothness_nrm_factor");

	if(nh.hasParam("/ramp/traj_collison_checking_time_resolution")) 
	{
		nh.getParam("/ramp/traj_collison_checking_time_resolution", ramp_params.traj_collison_checking_time_resolution);
		std::cout <<"\ntraj_collison_checking_time_resolution: "<< ramp_params.traj_collison_checking_time_resolution;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_collison_checking_time_resolution");

	if(nh.hasParam("/ramp/traj_evaluate_time_resolution")) 
	{
		nh.getParam("/ramp/traj_evaluate_time_resolution", ramp_params.traj_evaluate_time_resolution);
		std::cout <<"\ntraj_evaluate_time_resolution: "<< ramp_params.traj_evaluate_time_resolution;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_evaluate_time_resolution");
	
	if(nh.hasParam("/ramp/traj_evaluate_max_time")) 
	{
		nh.getParam("/ramp/traj_evaluate_max_time", ramp_params.traj_evaluate_max_time);
		std::cout <<"\ntraj_evaluate_max_time: "<< ramp_params.traj_evaluate_max_time;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_evaluate_max_time");

	if(nh.hasParam("/ramp/traj_collision_checking_start_time")) 
	{
		nh.getParam("/ramp/traj_collision_checking_start_time", ramp_params.traj_collision_checking_start_time);
		std::cout <<"\ntraj_collision_checking_start_time: "<< ramp_params.traj_collision_checking_start_time;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_collision_checking_start_time");

	if(nh.hasParam("/ramp/traj_collison_checking_max_time")) 
	{
		nh.getParam("/ramp/traj_collison_checking_max_time", ramp_params.traj_collison_checking_max_time);
		std::cout <<"\ntraj_collison_checking_max_time: "<< ramp_params.traj_collison_checking_max_time;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_collison_checking_max_time");

	if(nh.hasParam("/ramp/population_collison_checking_max_time")) 
	{
		nh.getParam("/ramp/population_collison_checking_max_time", ramp_params.population_collison_checking_max_time);
		std::cout <<"\npopulation_collison_checking_max_time: "<< ramp_params.population_collison_checking_max_time;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/population_collison_checking_max_time");

	if(nh.hasParam("/ramp/traj_jnt_vel_max_scale")) 
	{
		nh.getParam("/ramp/traj_jnt_vel_max_scale", ramp_params.traj_jnt_vel_max_scale);
		std::cout <<"\ntraj_jnt_vel_max_scale: "<< ramp_params.traj_jnt_vel_max_scale;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/traj_jnt_vel_max_scale");

	if(nh.hasParam("/ramp/verbose")) 
	{
		nh.getParam("/ramp/verbose", ramp_params.verbose);
		std::cout <<"\nverbose: "<< ramp_params.verbose;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/verbose");

	if(nh.hasParam("/ramp/is_hardware_enabled")) 
	{
		nh.getParam("/ramp/is_hardware_enabled", ramp_params.is_hardware_enabled);
		std::cout <<"\nis_hardware_enabled: "<< ramp_params.is_hardware_enabled;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/is_hardware_enabled");

	if(nh.hasParam("/ramp/use_published_velocity")) 
	{
		nh.getParam("/ramp/use_published_velocity", ramp_params.use_published_velocity);
		std::cout <<"\nuse_published_velocity: "<< ramp_params.use_published_velocity;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/use_published_velocity");

	if(nh.hasParam("/ramp/is_task_constrained")) 
	{
		nh.getParam("/ramp/is_task_constrained", ramp_params.is_task_constrained);
		std::cout <<"\nis_task_constrained: "<< ramp_params.is_task_constrained;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/is_task_constrained");

	if(nh.hasParam("/ramp/robot_state_topic")) 
	{
		nh.getParam("/ramp/robot_state_topic", ramp_params.robot_state_topic);
		std::cout <<"\nrobot_state_topic: "<< ramp_params.robot_state_topic;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/robot_state_topic");

	if(nh.hasParam("/ramp/push_object_name")) 
	{
		nh.getParam("/ramp/push_object_name", ramp_params.push_object_name);
		std::cout <<"\npush_object_name: "<< ramp_params.push_object_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/push_object_name");

	if(nh.hasParam("/ramp/arm_joint_group_name")) 
	{
		nh.getParam("/ramp/arm_joint_group_name", ramp_params.arm_joint_group_name);
		std::cout <<"\narm_joint_group_name: "<< ramp_params.arm_joint_group_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_joint_group_name");

	if(nh.hasParam("/ramp/arm_eef_link_name")) 
	{
		nh.getParam("/ramp/arm_eef_link_name", ramp_params.arm_eef_link_name);
		std::cout <<"\narm_eef_link_name: "<< ramp_params.arm_eef_link_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_eef_link_name");

	if(nh.hasParam("/ramp/arm_joint_names")) 
	{
		nh.getParam("/ramp/arm_joint_names", ramp_params.arm_joint_names);
		std::cout <<"\narm_joint_names: ";
		for (int i=0; i<ramp_params.arm_joint_names.size(); i++)
			std::cout << ramp_params.arm_joint_names.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_joint_names");

	if(nh.hasParam("/ramp/follow_joint_trajectory_action_gazebo")) 
	{
		nh.getParam("/ramp/follow_joint_trajectory_action_gazebo", ramp_params.follow_joint_trajectory_action_gazebo);
		std::cout <<"\nfollow_joint_trajectory_action_gazebo: "<< ramp_params.follow_joint_trajectory_action_gazebo;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/follow_joint_trajectory_action_gazebo");

	if(nh.hasParam("/ramp/follow_joint_trajectory_action_hardware")) 
	{
		nh.getParam("/ramp/follow_joint_trajectory_action_hardware", ramp_params.follow_joint_trajectory_action_hardware);
		std::cout <<"\nfollow_joint_trajectory_action_hardware: "<< ramp_params.follow_joint_trajectory_action_hardware;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/follow_joint_trajectory_action_hardware");

	if(nh.hasParam("/ramp/sensing_topic")) 
	{
		nh.getParam("/ramp/sensing_topic", ramp_params.sensing_topic);
		std::cout <<"\nsensing_topic: "<< ramp_params.sensing_topic;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/sensing_topic");

	if(nh.hasParam("/ramp/start_eef_pose")) 
	{
		nh.getParam("/ramp/start_eef_pose", ramp_params.start_eef_pose);
		std::cout <<"\nstart_eef_pose: ";
		for (int i=0; i<ramp_params.start_eef_pose.size(); i++)
			std::cout << ramp_params.start_eef_pose.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/start_eef_pose");

	if(nh.hasParam("/ramp/goal_eef_pose")) 
	{
		nh.getParam("/ramp/goal_eef_pose", ramp_params.goal_eef_pose);
		std::cout <<"\ngoal_eef_pose: ";
		for (int i=0; i<ramp_params.goal_eef_pose.size(); i++)
			std::cout << ramp_params.goal_eef_pose.at(i) << " ";
	}
	else 
		ROS_ERROR("Did not find parameter ramp/goal_eef_pose");

	if(nh.hasParam("/ramp/trajectory_generation_option")) 
	{
		nh.getParam("/ramp/trajectory_generation_option", ramp_params.trajectory_generation_option);
		std::cout <<"\ntrajectory_generation_option: "<< ramp_params.trajectory_generation_option;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/trajectory_generation_option");

	if(nh.hasParam("/ramp/task_constraint_axis")) 
	{
		nh.getParam("/ramp/task_constraint_axis", ramp_params.task_constraint_axis);
		std::cout <<"\ntask_constraint_axis: "<< ramp_params.task_constraint_axis;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/task_constraint_axis");

	if(nh.hasParam("/ramp/task_constraint_tolerance")) 
	{
		nh.getParam("/ramp/task_constraint_tolerance", ramp_params.task_constraint_tolerance);
		std::cout <<"\ntask_constraint_tolerance: "<< ramp_params.task_constraint_tolerance;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/task_constraint_tolerance");

	if(nh.hasParam("/ramp/eef_sampling_space")) 
	{
		nh.getParam("/ramp/eef_sampling_space", ramp_params.eef_sampling_space);
		std::cout <<"\neef_sampling_space: ";
		for (int i=0; i<ramp_params.eef_sampling_space.size(); i++)
			std::cout << ramp_params.eef_sampling_space.at(i) << " ";
		std::cout << std::endl;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/eef_sampling_space");

	if(nh.hasParam("/ramp/arm_obs_description_name")) 
	{
		nh.getParam("/ramp/arm_obs_description_name", ramp_params.arm_obs_description_name);
		std::cout <<"\narm_obs_description_name: "<< ramp_params.arm_obs_description_name;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_obs_description_name");

	if(nh.hasParam("/ramp/arm_obs_sensing_topic")) 
	{
		nh.getParam("/ramp/arm_obs_sensing_topic", ramp_params.arm_obs_sensing_topic);
		std::cout <<"\narm_obs_sensing_topic: "<< ramp_params.arm_obs_sensing_topic;
	}
	else 
		ROS_ERROR("Did not find parameter ramp/arm_obs_sensing_topic");
}

void rampCore::robotStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	ROS_INFO("[Robot State Callback]-Started");
	ROS_INFO("Update robot cfg and velocity with latest information broadcast from Gazebo.");
	robot_cfg = msg->position;
	robot_cfg_vel = msg->velocity;
	ROS_INFO("Latest robot cfg:");
	rampUtil::printArmCfg(robot_cfg);
	ROS_INFO("Latest robot cfg velocity:");
	rampUtil::printArmCfg(robot_cfg_vel);
	ROS_INFO("[Robot State Callback]-Finished");

	stats.robot_cfg_vec.push_back(robot_cfg);
	stats.robot_cfg_vel_vec.push_back(robot_cfg_vel);
}

void rampCore::robotStateHardWareCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	ROS_INFO("[Robot State HW Callback]-Started");
	ROS_INFO("Update robot cfg and velocity with latest information broadcast from hardware.");
	ROS_INFO("Latest robot cfg on hw:");
	rampUtil::printArmCfg(msg->position);
	ROS_INFO("Latest robot cfg velocity on hw:");
	rampUtil::printArmCfg(msg->velocity);
	ROS_INFO("[Robot State HW Callback]-Finished");

	stats.robot_cfg_hw_vec.push_back(msg->position);
	stats.robot_cfg_hw_vel_vec.push_back(msg->velocity);
}

void rampCore::initialize()
{
	ros::Time t1 = ros::Time::now();
	ROS_INFO("Initializing RAMP...");
	/*
		Step 1
		Initialize kinematic setting and sampler
		rampSetting - load in robot kinematics 
		rampSampling - sampler of random/eef-constrained joint configurations
	*/
	ROS_INFO("Loading robot kinematics and robot state sampler...");
	ramp_setting_ptr = boost::make_shared<rampSetting>();
	ramp_setting_ptr->printJointBoundsInfo();
	ramp_sampling_ptr = boost::make_shared<rampSampling>(ramp_setting_ptr);

	/*
		Step 2
		Initialize control/rendering
	*/
	ROS_INFO("Setting up RAMP control in Gazebo...");
	ramp_rendering_ptr = boost::make_shared<rampRendering>(nh);
	if (ramp_params.is_hardware_enabled)
	{
		ROS_INFO("Setting up RAMP control on hardware...");
		ramp_control_ptr = boost::make_shared<rampControl>();
	}
	/*
		Step 3
		Initialize ramp sensing
	*/
	ROS_INFO("Initializing RAMP sensing...");
	ramp_sensing_ptr = boost::make_shared<rampSensing>(ramp_setting_ptr->getRobotModelPtr());

	/*
		Step 4-A
		Move to start eef pose
	*/
	ROS_INFO("Moving to start eef pose");
	std::vector<double> start_pose_vec = ramp_params.start_eef_pose;
	geometry_msgs::Pose start_pose;
	rampUtil::convertXYZRPYtoQuaternion(start_pose_vec, start_pose);
	armCfg init_cfg;
	ramp_sampling_ptr->sampleJntCfgGivenEefPose(init_cfg, start_pose);
	armCfg default_cfg;
	ramp_setting_ptr->getDefaultJointConfiguration(default_cfg);
	rampTrajectoryPtr init_traj = boost::make_shared<rampTrajectory>(ramp_setting_ptr, ramp_sensing_ptr);
	int nDofs = init_traj->getNumOfDofs();
	init_traj->setTrajectory(default_cfg, init_cfg, -100); // a random and non-exist ID
	
	std::vector<double> start_end_vel(nDofs, 0.0);
	init_traj->generateTrajectoryFromPath(start_end_vel, start_end_vel, ramp_params.trajectory_generation_option);
	ramp_rendering_ptr->renderJointTrajectoryGazebo(*init_traj, "NA");
	if (ramp_params.is_hardware_enabled)
	{
		ramp_control_ptr->executeJointTrajectoryOnHardware(*init_traj, "NA");
	}
	sleep(init_traj->getTrajectoryDuration());

	/*
		Step 4-B
		Set safe pose vec (predefined)
		Compute trajectories to drop cup and pick cup
	*/
	std::vector<double> safe_pose_vec(6,0.0);
	safe_pose_vec[0] = -0.1; safe_pose_vec[1] = 0.3; safe_pose_vec[2] = 0.3;
	safe_pose_vec[3] = 0.; safe_pose_vec[4] = 0.; safe_pose_vec[5] = 0.;
	rampUtil::convertXYZRPYtoQuaternion(safe_pose_vec, safe_pose);

	/*
		Step 5
		Initialize operators
	*/
	ROS_INFO("Initializing RAMP genetic operators...");
	ramp_oprt_ptr = boost::make_shared<rampOperators>(ramp_sampling_ptr);

	/*
		Step 6
		Goal Setting
		Set the goal of this planning
	*/
	ROS_INFO("Setting up RAMP goal...");
	std::vector<double> pose_vec = ramp_params.goal_eef_pose;
	geometry_msgs::Pose goal;
	rampUtil::convertXYZRPYtoQuaternion(pose_vec, goal);
	ramp_goal.setGoalEefPose(goal);

	/*
		Step 7
		Set task-constrained sampling if enabled in ramp parameters
	*/
	if (ramp_params.is_task_constrained)
	{
		std::string axis = ramp_params.task_constraint_axis;
		double tolerance = ramp_params.task_constraint_tolerance * M_PI;
		eefWorkSpace eef_space;
		eef_space.x_min = ramp_params.eef_sampling_space.at(0);
		eef_space.x_max = ramp_params.eef_sampling_space.at(1);
		eef_space.y_min = ramp_params.eef_sampling_space.at(2);
		eef_space.y_max = ramp_params.eef_sampling_space.at(3);
		eef_space.z_min = ramp_params.eef_sampling_space.at(4);
		eef_space.z_max = ramp_params.eef_sampling_space.at(5);
		ramp_sampling_ptr->setConstrainedBasePose(start_pose_vec,axis,tolerance, eef_space);
		ramp_setting_ptr->setTaskCstrPose(start_pose);
	}
	/*
		Step 8
		Add obstacles
	*/
	ROS_INFO("Setting up RAMP obstacles...");
	/* obstacle 1 ground plane*/
	geometry_msgs::Pose obs_pose;
	obs_pose.position.x = 0.;
	obs_pose.position.y = 0.;
	obs_pose.position.z = -0.02;
	obs_pose.orientation.w = 1.0;
	Eigen::Vector3f dim;
	dim << 3.0, 3.0, 0.02; 
	std::string type("box");
	std::string id("ground");
	std::string frame("world");
	rampObstacle ob1(type, id, frame, obs_pose, dim);

	if (ramp_params.test_case_name == "abb-optitrack")
	{
		/* obstacle 2 tracked by optitrack */
		obs_pose.position.x = 0.6;
		obs_pose.position.y = -1.2;
		obs_pose.position.z = 0.0;
		obs_pose.orientation.w = 1.0;
		id = std::string("optitrack");
		dim << 0.35, 0.35, 0.35;
		rampObstacle ob_track(type, id, frame, obs_pose, dim);

		ROS_INFO("Registering obstacles with RAMP sensing...");
		ramp_sensing_ptr->addObstacleToPlanningScene(ob1);
		ramp_sensing_ptr->addObstacleToPlanningScene(ob_track);
	}

	/*
		Step 9
		Initialize robot starting configuration and velocity
	*/
	ROS_INFO("Initializing robot start cfg and velocity...");
	robot_cfg = init_cfg;
	robot_cfg_vel = armCfg(nDofs,0.0);

	/*
		Step 10
		Initialize and evaluate trajectories in population
	*/
	ROS_INFO("Initializing RAMP population...");
	ramp_population_ptr = boost::make_shared<rampPopulation>(ramp_setting_ptr, ramp_sensing_ptr, ramp_oprt_ptr);
	ramp_population_ptr->setStartRobotCfgInWholePopulation(init_cfg);
	ramp_population_ptr->setGoalsInWholePopulation(ramp_sampling_ptr, goal);
	if (ramp_params.test_case_name=="abb-optitrack")
	{
		ramp_population_ptr->setGoalInSubPopulation(ramp_sampling_ptr, safe_pose, "tc-sub");
		ramp_sub_goal.setGoalEefPose(safe_pose);
	}

	ramp_population_ptr->generateTrajectories();

	/* 
		Step 11
		Initialize ramp learning (sequential knowledge transfer)	
	 */
	ROS_INFO("Initialzing ramp learning...");
	ramp_learn_ptr = boost::make_shared<rampLearning>(nDofs);

	/* 
		Step 12
		Initial evaluation of population
	*/
	ROS_INFO("Initial evaluation of RAMP population...");
	ramp_population_ptr->InitialEvaluateWholePopulation();

	ROS_INFO("RAMP Initialization Done.");
	ros::Time t2 = ros::Time::now();
	stats.ramp_initialization_time = (t2-t1).toSec();

	visualizeCurrentPlanningScene();
}

void rampCore::showPopulation(bool verbose)
{
	ramp_population_ptr->showPopulationInfo();
}

void rampCore::visualizeEefPosesDuringMotion(const geometry_msgs::PoseArray &eef_pose_array)
{
	int pose_vis_option = 2;
	switch (pose_vis_option)
	{
		case 1:
		{
			/* 
				method 2
				show using pose array (several frame axis)-
				but this currently can only show final frame 
				(cannot keep history)
			*/
			ramp_rendering_ptr->renderPoseArrayUsingFrame(eef_pose_array);
			break;
		}
		case 2:
		{
			/* 
				method 3
				show using pose array (several cylinders)
			*/
			ramp_rendering_ptr->renderMarkerArray(eef_pose_array);
			break;
		}
	}
}

void rampCore::visualizeCurrentPlanningScene()
{
	geometry_msgs::Pose tmp_pose;
	EigenSTL::vector_Affine3d tmp_affine3ds;

	geometry_msgs::PoseArray pose_array;
	std::vector<Eigen::Vector3d> dimensions;

	std::vector<std::string> obj_ids = ramp_sensing_ptr->getPlanningScenePtr()->getWorld()->getObjectIds();
	for(int i=0; i<obj_ids.size(); ++i)
	{
		tmp_affine3ds = ramp_sensing_ptr->getPlanningScenePtr()->getWorld()->getObject(obj_ids[i])->shape_poses_;
		for (int j=0; j<tmp_affine3ds.size(); ++j)
		{
			rampUtil::convertAffine3dtoQuaternion(tmp_affine3ds[j], tmp_pose);
			pose_array.poses.push_back(tmp_pose);
		}
		std::vector<shapes::ShapeConstPtr> shapes = ramp_sensing_ptr->getPlanningScenePtr()->getWorld()->getObject(obj_ids[i])->shapes_;
		for (int k=0; k<shapes.size(); ++k)
		{
			Eigen::Vector3d tmp_dim = shapes::computeShapeExtents(shapes[k].get());
			dimensions.push_back(tmp_dim);
		}
	}
	ramp_rendering_ptr->renderMarkerArray(pose_array, dimensions);
}

void rampCore::showExperimentStats(const geometry_msgs::PoseArray &eef_pose_array)
{
	/* get eef pose data */
	double cum_x_pos = 0.0;
	double cum_y_pos = 0.0;
	double cum_z_pos = 0.0;
	double cum_tilted_angle = 0.0;
	int num_of_eef_poses = eef_pose_array.poses.size();
	std::vector<double> eef_tilted_angles_along_exe_traj;
	for (int i=0; i<num_of_eef_poses; ++i)
	{
		std::vector<double> pose_vec;
		rampUtil::convertQuaterniontoXYZRPY(eef_pose_array.poses.at(i), pose_vec);
		pose_vec.at(3) = std::abs(pose_vec.at(3));
		pose_vec.at(4) = std::abs(pose_vec.at(4));
		pose_vec.at(5) = std::abs(pose_vec.at(5));
		/* 
			remove ambiguity in rpy 
			closer to 3.14 than 0.0
		*/
		if ( std::abs(pose_vec.at(3)-3.14) < std::abs(pose_vec.at(3)-0.0) )
			pose_vec.at(3) = std::abs(3.14 - pose_vec.at(3));
		if ( std::abs(pose_vec.at(4)-3.14) < std::abs(pose_vec.at(4)-0.0) )
			pose_vec.at(4) = std::abs(3.14 - pose_vec.at(4));
		if ( std::abs(pose_vec.at(5)-3.14) < std::abs(pose_vec.at(5)-0.0) )
			pose_vec.at(5) = std::abs(3.14 - pose_vec.at(5));
		/* 
			get tilted angles
		*/
		double avg_tilted_angle = 0.;
		if (pose_vec.at(3) > pose_vec.at(4))
			avg_tilted_angle = pose_vec.at(3);
		else
			avg_tilted_angle = pose_vec.at(4);

		eef_tilted_angles_along_exe_traj.push_back(avg_tilted_angle);
		cum_tilted_angle += avg_tilted_angle;

		/* 
			get position constraints
		*/
		cum_x_pos += pose_vec.at(0);
		cum_y_pos += pose_vec.at(1);
		cum_z_pos += pose_vec.at(2);
	}
	std::vector<double>::iterator it = std::max_element(eef_tilted_angles_along_exe_traj.begin(), eef_tilted_angles_along_exe_traj.end());
	double max_tiled_angle = *it;
	int max_tilted_angle_index = it - eef_tilted_angles_along_exe_traj.begin();

	ROS_INFO("**********************************************************");
	ROS_INFO("Experiment stats:");
	ROS_INFO("RAMP initilization time %.3f secs", stats.ramp_initialization_time);
	ROS_INFO("RAMP offline run time %.3f secs", stats.ramp_offline_run_time);
	ROS_INFO("RAMP online run time %.3f secs", stats.ramp_online_run_time);
	ROS_INFO("Total number of insert/change operators call %d", stats.num_of_ins_chg_oper);
	ROS_INFO("Total number of insert/change operators success %d", stats.num_of_success_ins_chg_oper);
	ROS_INFO("Number of control cycle: %d", stats.num_of_ctrl_cyc);
	ROS_INFO("Number of control cycle with whole population infeasible: %zd", stats.ctrl_cyc_ind_whole_pop_infeas.size());
	for(int i=0; i<stats.ctrl_cyc_ind_whole_pop_infeas.size(); ++i)
		std::cout << "Control Cycle " << stats.ctrl_cyc_ind_whole_pop_infeas.at(i) << std::endl;
	ROS_INFO("Number of online planning cycle: %d", stats.num_of_plan_cyc);
	ROS_INFO("Number of offline planning cycle: %d", stats.num_of_offline_plan_cyc);
	ROS_INFO("Average time of online planning cycle: %.3f secs", stats.cum_planning_cyc_time / double (stats.num_of_plan_cyc));
	ROS_INFO("Average time of control cycle: %.3f secs", stats.cum_ctrl_cyc_time / double (stats.num_of_ctrl_cyc));
	ROS_INFO("Average control frequency %.3f Hz", double (stats.num_of_ctrl_cyc) / stats.ramp_online_run_time);
	ROS_INFO("Average number of planning cycles per control cycle: %.3f", double (stats.num_of_plan_cyc) / double (stats.num_of_ctrl_cyc));
	ROS_INFO("Total number of trajectory switches: %d", stats.num_of_traj_switches);
	ROS_INFO("Number of trajectory switches due to last trajectory became infeasible: %d", stats.traj_switch_due_to_infeas);
	ROS_INFO("Number of trajectory switches due to better trajectory appeared: %d", stats.traj_switch_due_to_better_traj);
	ROS_INFO("Number of control cycles running non-task-constrained trajectory: %d", stats.num_of_ctrl_cyc_running_ntc_traj);
	ROS_INFO("Number of control cycles running task-constrained trajectory: %d", stats.num_of_ctrl_cyc_running_tc_traj);	
	if (ramp_params.task_constraint_axis == "XY-O")
	{
		ROS_INFO("Average tilted angle along executed trajectory: %.3f degs", cum_tilted_angle / double (num_of_eef_poses) * 57.29);
		ROS_INFO("Largest tilted angle along executed trajectory: %.3f degs occured near online planning cycle %d", max_tiled_angle * 57.29, max_tilted_angle_index);
	}
	ROS_INFO("Robot cfgs along the executed trajectory:");
	rampUtil::printArmCfgs(stats.robot_cfg_vec);
	ROS_INFO("Robot cfg velocities along the executed trajectory:");
	rampUtil::printArmCfgs(stats.robot_cfg_vel_vec);

	ROS_INFO("Robot cfgs on hardware along the executed trajectory:");
	rampUtil::printArmCfgs(stats.robot_cfg_hw_vec);
	ROS_INFO("Robot cfg velocities on hardware along the executed trajectory:");
	rampUtil::printArmCfgs(stats.robot_cfg_hw_vel_vec);
	ROS_INFO("Executed trajecoty subpopulation type and its ID:");
	for (int i=0; i<stats.exe_traj_per_ctrl_cyc.size(); ++i)
		ROS_INFO("Control Cycle [%d]: Subpopulation [Name %s] Trajectory ID [%d]", i, 
				stats.exe_traj_per_ctrl_cyc[i].first.c_str(), stats.exe_traj_per_ctrl_cyc[i].second);
	ROS_INFO("**********************************************************");
}

void rampCore::dropThenPickCup()
{
	geometry_msgs::Pose safe_pose_down = safe_pose;
	safe_pose_down.position.z -= 0.2;
	int err_code = -1;
	moveit_msgs::RobotTrajectory traj;
	if(jacobianBasedMotionGeneration(robot_cfg,safe_pose_down,1e2,err_code,traj))
	{
		int s = traj.joint_trajectory.points.size();
		armCfg mid_cfg = traj.joint_trajectory.points[s/2].positions;
		armCfg down_cfg = traj.joint_trajectory.points.back().positions;
		rampTrajectoryPtr drop_t = boost::make_shared<rampTrajectory>(ramp_setting_ptr, ramp_sensing_ptr);
		rampTrajectoryPtr pick_t = boost::make_shared<rampTrajectory>(ramp_setting_ptr, ramp_sensing_ptr);

		int reserved_id_1 = -333; // reserved trajectory id
		int reserved_id_2 = -444;
		drop_t->setTrajectory(robot_cfg, down_cfg, reserved_id_1); 
		pick_t->setTrajectory(down_cfg, robot_cfg, reserved_id_2);
		drop_t->knot_cfgs.push_back(mid_cfg); 
		pick_t->knot_cfgs.push_back(mid_cfg); 
		
		int nDofs = drop_t->getNumOfDofs();
		std::vector<double> start_end_vel(nDofs, 0.0);
		ramp_params.traj_jnt_vel_max_scale = 0.05;
		drop_t->generateTrajectoryFromPath(start_end_vel, start_end_vel, ramp_params.trajectory_generation_option);
		pick_t->generateTrajectoryFromPath(start_end_vel, start_end_vel, ramp_params.trajectory_generation_option);
		ramp_params.traj_jnt_vel_max_scale = 0.1;

		/* move trajectory duration longer than an instant */
		// drop_t->cfgs_durations.back() = 5.0;
		// pick_t->cfgs_durations.back() = 5.0;
		
		/* move down vertically */
		ROS_INFO("Moving down vertically to drop cup");
		ramp_rendering_ptr->renderJointTrajectoryGazebo(*drop_t, "NA");
		if (ramp_params.is_hardware_enabled)
		{
			ramp_control_ptr->executeJointTrajectoryOnHardware(*drop_t, "NA");
		}
		sleep(5.0);

		/* move up vertically */
		ROS_INFO("Moving up vertically to pick cup");
		ramp_rendering_ptr->renderJointTrajectoryGazebo(*pick_t, "NA");
		if (ramp_params.is_hardware_enabled)
		{
			ramp_control_ptr->executeJointTrajectoryOnHardware(*pick_t, "NA");
		}
		sleep(5.0);
	}
	else
		ROS_ERROR("No enough manoeuvre space to drop and pick cup");
}

bool rampCore::jacobianBasedMotionGeneration(const armCfg &start_cfg, const geometry_msgs::Pose &target_eef_pose, const int &num_of_steps, int &error_code, moveit_msgs::RobotTrajectory &trajectory)
{
	/* 
		Step 1
		Compute positional deviation w.r.t the target eef pose 
	*/
	geometry_msgs::Pose start_pose;
	ramp_setting_ptr->setJointConfiguration(start_cfg);
	ramp_setting_ptr->getEefPose(start_pose);
	Eigen::MatrixXd delta_e(6,1);
	delta_e << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

	/*
		Step 2
		determine Catesian motion step
	*/
	delta_e(0,0) = (target_eef_pose.position.x - start_pose.position.x) / (double) num_of_steps;
	delta_e(1,0) = (target_eef_pose.position.y - start_pose.position.y) / (double) num_of_steps;
	delta_e(2,0) = (target_eef_pose.position.z - start_pose.position.z) / (double) num_of_steps;

	/* 
		Step 3
		Check if start pose is already close to the target pose
	*/
	if (rampUtil::areSamePositions(start_pose, target_eef_pose))
	{
		ROS_WARN("Starting eef pose is already very close to the target eef pose.");
		error_code = 0;
		return true;
	}

	/* 
		Step 4
		Motion generation
	 */
	armCfg arm_cur_cfg = start_cfg;
	Eigen::MatrixXd jacobian, delta_theta, pseudo_inv;
	geometry_msgs::Pose cur_pose;
	int time_step = 0;

	trajectory_msgs::JointTrajectoryPoint start_point;
	start_point.positions = start_cfg;
	trajectory.joint_trajectory.points.push_back(start_point);

	while ( (time_step<num_of_steps) && ros::ok() )
	{
		jacobian = ramp_setting_ptr->getJacobian(arm_cur_cfg);
		/* check singularity */
		int method = 2;
		switch(method)
		{
			case 1:
			{
				Eigen::FullPivLU<Eigen::MatrixXd> luA(jacobian);
				int rank = luA.rank();
				if (rank != 6)
				{
					ROS_WARN("Singularity[Jacobian rank %d] occured at the following cfg", rank);
					rampUtil::printArmCfg(arm_cur_cfg);
					error_code = 1;
					return false;
				}
				break;
			}
			case 2:
			{
				double manipulability = ramp_setting_ptr->getManipulabilityIndex(arm_cur_cfg);
				if (manipulability < 0.001)
				{
					ROS_WARN("Singularity[Manipulability %f] occured at the following cfg", manipulability);
					rampUtil::printArmCfg(arm_cur_cfg);
					error_code = 1;
					return false;
				}
				break;
			}
		}

		/* generate joint motion step */
		pseudo_inv = (jacobian.transpose())*((jacobian*(jacobian.transpose())).inverse());
		delta_theta = pseudo_inv*delta_e;
		for(int i=0; i<arm_cur_cfg.size(); ++i)
			arm_cur_cfg[i] = arm_cur_cfg[i] + delta_theta(i,0);

		/* check joint limits */
		if (!ramp_setting_ptr->isInJointAngleRanges(arm_cur_cfg))
		{
			ROS_WARN("Joint out of the range at the following cfg");
			rampUtil::printArmCfg(arm_cur_cfg);
			error_code = 2;
			return false;
		}
		
		/* update new configuration in rviz */
		// renderJointConfiguration(arm_cur_cfg, ramp_setting_ptr->getJointNames());
		/* push this cfg to trajecoty */
		trajectory_msgs::JointTrajectoryPoint point;
		point.positions = arm_cur_cfg;
		trajectory.joint_trajectory.points.push_back(point);
		++time_step;
	}
	ramp_setting_ptr->setJointConfiguration(arm_cur_cfg);
	ramp_setting_ptr->getEefPose(cur_pose);
	if (rampUtil::areSamePositions(cur_pose, target_eef_pose))
	{
		ROS_INFO("Target eef pose reached via Jacobian motion genereation.");
		error_code = 0;
		return true;
	}
	else
		ROS_WARN("Number of motion steps has been exhausted but target eef pose is not reached.");
}

void rampCore::doControl()
{
	/*
		step 1
		update start config and velocity for each trajectory 
		and re-generate trajectory
	 */
	ramp_population_ptr->updatePopulationStartStates(robot_cfg, robot_cfg_vel);
	/*
		step 2
		re-evaluate trajectory
	*/
	ramp_population_ptr->updateWholePopulationEvaluation();	
	/*
		step 3
		get best trajectory and execute
		resolve conflicts if any
	*/
	if (ramp_params.test_case_name=="abb-optitrack") // control cycle with conflict resolution
	{
		rampTrajectoryPtr exe_traj;
		std::string exe_traj_sub_pop_name;
		if (!manipulate_obj_obstacle_status) // holding the cup
		{
			if (!ramp_population_ptr->isWholeSubPopulationInfeasible("tc-main"))
			{
				ROS_INFO("Executing best traj from tc-main");
				exe_traj = ramp_population_ptr->getBestTrajectoryInSubPopulation("tc-main");
				exe_traj_sub_pop_name = std::string("tc-main");
			}
			else 
			{
				ROS_INFO("Executing best traj from tc-sub");
				exe_traj = ramp_population_ptr->getBestTrajectoryInSubPopulation("tc-sub");
				exe_traj_sub_pop_name = std::string("tc-sub");

				ramp_setting_ptr->setJointConfiguration(robot_cfg);
				geometry_msgs::Pose cur_eef_pose;
				ramp_setting_ptr->getEefPose(cur_eef_pose);
				ramp_sub_goal.setCurrentEefPose(cur_eef_pose);

				if (ramp_sub_goal.isGoalReached())
				{
					ROS_INFO("Safe pose reached by traj from tc-sub");
					manipulate_obj_obstacle_status = true; // start being considered as an obstacel
					// ramp_sensing_ptr->addObsIntoPlanningSceneByID(ramp_params.push_object_name);
					dropThenPickCup();
				}
			}
		}
		else
		{
			ROS_INFO("Executing best traj from ntc1");
			exe_traj = ramp_population_ptr->getBestTrajectoryInSubPopulation("ntc1");
			exe_traj_sub_pop_name = std::string("ntc1");

			if (first_time_ntc1)
			{
				first_time_ntc1 = false;
				ramp_population_ptr->setGoalInSubPopulation(ramp_sampling_ptr, safe_pose, "ntc1");
				ramp_sub_goal.setGoalEefPose(safe_pose);
				start_ctrl_cyc = stats.num_of_ctrl_cyc;
				return;
			}

			ramp_setting_ptr->setJointConfiguration(robot_cfg);
			geometry_msgs::Pose cur_eef_pose;
			ramp_setting_ptr->getEefPose(cur_eef_pose);
			ramp_sub_goal.setCurrentEefPose(cur_eef_pose);

			if (stats.num_of_ctrl_cyc > (start_ctrl_cyc+50) && ramp_sub_goal.isGoalReached())
			{
				ROS_INFO("Safe pose reached by traj from ntc1");
				manipulate_obj_obstacle_status = false; // hold the cup again
				// ramp_sensing_ptr->addObsIntoPlanningSceneByID(ramp_params.push_object_name);
				dropThenPickCup();
			}
		}

		ramp_rendering_ptr->renderJointTrajectoryGazebo(*exe_traj, exe_traj_sub_pop_name);
		if (ramp_params.is_hardware_enabled)
			ramp_control_ptr->executeJointTrajectoryOnHardware(*exe_traj, exe_traj_sub_pop_name);
		
		/* logging */
		exe_traj->printTrajectoryInfo(true);
		std::pair<std::string, int> cur_exe_traj_info(exe_traj_sub_pop_name, exe_traj->getTrajectoryID());
		stats.exe_traj_per_ctrl_cyc.push_back(cur_exe_traj_info);
	}
	else /* normal control cycle without conflict resolution */
	{
		rampTrajectoryPtr exe_traj = ramp_population_ptr->getBestTrajectoryInPopulation();
		const std::string &exe_traj_sub_pop_type = ramp_population_ptr->getBestTrajectorySubPopulationType();
		const std::string &exe_traj_sub_pop_name = ramp_population_ptr->getBestTrajectorySubPopulationName();

		ramp_rendering_ptr->renderJointTrajectoryGazebo(*exe_traj, exe_traj_sub_pop_type);
		if (ramp_params.is_hardware_enabled)
			ramp_control_ptr->executeJointTrajectoryOnHardware(*exe_traj, exe_traj_sub_pop_type);
		
		/* logging */
		if(ramp_params.verbose)
			exe_traj->printTrajectoryInfo(true);
		std::pair<std::string, int> cur_exe_traj_info(exe_traj_sub_pop_name, exe_traj->getTrajectoryID());
		stats.exe_traj_per_ctrl_cyc.push_back(cur_exe_traj_info);
	}

	visualizeCurrentPlanningScene();
}

void rampCore::doPlanning()
{	
	std::string sub_p_type, sub_p_name;
	ramp_population_ptr->getRandomSubpopulation(sub_p_name, sub_p_type);
	if(ramp_params.verbose)	
		ROS_INFO("[Picked subpopulation]: [Name %s] [Type %s] ", sub_p_name.c_str(), sub_p_type.c_str());
	rampTrajectoryPtr traj = ramp_population_ptr->getRandomTrajectory(sub_p_name);
	if(ramp_params.verbose)
		ROS_INFO("[Picked Trajectory]: [ID %d]", traj->getTrajectoryID());
	
	/*
		get a copy of the object pointed by traj
		so the operators wont affect traj 
	*/
	ROS_INFO("[Applying Operator]-Start");
	rampTrajectoryPtr tmp_traj = boost::make_shared<rampTrajectory>(*traj);
	ramp_oprt_ptr->registerTrajectory(tmp_traj);
	bool isOperated = false;
	/*
		pick an operator 
		consider 5 operators for now
		This part is planned to be re-wrriten when I have a better and modular
		implementation of (sub)population, and we only apply task-constrained
		operators on task-constrained subpopulation
	*/
	int operator_ind = rand() % 5 + 1; 
	switch (operator_ind)
	{
		case 1:
		{
			if (sub_p_type == "tc")
				isOperated = ramp_oprt_ptr->applyTaskConstrainedInsert();
			if (sub_p_type == "ntc")
				isOperated = ramp_oprt_ptr->applyRandomInsert();
			break;
		}
		case 2:
		{
			isOperated = ramp_oprt_ptr->applyDelete();
			break;
		}
		case 3:
		{
			if (sub_p_type == "tc")
				isOperated = ramp_oprt_ptr->applyTaskConstrainedChange();
			if (sub_p_type == "ntc")
				isOperated = ramp_oprt_ptr->applyRandomChange();
			break;
		}
		case 4:
		{
			isOperated = ramp_oprt_ptr->applySwap();
			break;
		}
		case 5:
		{
			/*
				randomly return a trajectory (can be the same as traj)
			*/
			rampTrajectoryPtr traj2 = ramp_population_ptr->getRandomTrajectory(sub_p_name);
			/*
				get a copy of the object pointed by traj2
			*/
			rampTrajectoryPtr tmp_traj2 = boost::make_shared<rampTrajectory>(*traj2);
			isOperated = ramp_oprt_ptr->applyCrossOver(*tmp_traj2);
			break;
		}
	}
	ramp_oprt_ptr->dropTrajectory();
	ROS_INFO("[Applying Operator]-Finished");

	if (isOperated)
	{
		/* 
			re-generate trajectory for this new trajectory 
		*/
		ROS_INFO("[Generating Trajectory]-Start");
		armCfg e_cfg_vel(tmp_traj->getNumOfDofs(), 0.0);
		tmp_traj->generateTrajectoryFromPath(traj->cfgs_velocites.front(), e_cfg_vel, ramp_params.trajectory_generation_option);
		ROS_INFO("[Generating Trajectory]-Finished");

		/* 
			collision check
			of whole trajectory against dynamic obstacle 
		*/
		ROS_INFO("[Whole Trajectory Collison Checking]-Start");
		tmp_traj->wholeTrajectoryCollisionChecking();
		ROS_INFO("[Whole Trajectory Collison Checking]-Finished");

		ROS_INFO("[Evaluating Trajectory]-Start");
		tmp_traj->evaluateTrajectory();
		ROS_INFO("[Evaluating Trajectory]-Finished");

		/* 
			check if duplicate exists in current population 
		*/
		bool duplicate_exist = ramp_population_ptr->checkDuplicateInSubPopulation(tmp_traj, sub_p_name);

		ROS_INFO("[Update Population]-Start");
		if (tmp_traj->cost < ramp_population_ptr->getWorstTrajectoryCostInSubPopulation(sub_p_name) && !duplicate_exist)
		{
			ROS_INFO("New trajectory is better than the worst trajectory in subpopulation [Name %s].", sub_p_name.c_str());
			if (tmp_traj->feasibility)
			{
				/* randomly replace a feasible trajectory(not best) in population */
				ramp_population_ptr->replaceOneRandomTrajectoryInSubPopulation(tmp_traj, sub_p_name);
			}
			else
			{
				/* randomly replace an infeasible trajectory(not best) in population */
				ramp_population_ptr->replaceOneInfeasibleTrajectoryInSubPopulation(tmp_traj, sub_p_name);
			}
		}
		else
			ROS_INFO("Discarded new trajectory since it is worse than the worst trajectory currently in its subpopulation.");
		ROS_INFO("[Update Population]-Finished");
	}
	else
		ROS_WARN("Trajectory did not have any change after operator. Continue to next planning cycle.");
}

void rampCore::offlineRun()
{
	ros::Time t1 = ros::Time::now();

	int count = 0;
	int converge_iter_th = 1000;
	double cur_best_traj_cost = 0.0;

	while(ros::ok() && count < ramp_params.max_offline_iterations)
	{
		/* 
			Step 1
			pick a trajectory randomly
			apply operator
			re-generate trajectory
			re-evaluate trajectory
			replace one trajectory in population or discard
		*/
		std::cout << std::endl;
		ROS_INFO("[Offline Planning Cycle %d]-Start", count);
		doPlanning();
		ROS_INFO("[Offline Planning Cycle %d]-Finished", count);

		/*
			Step 2 
			sort trajectories in population by cost 
		*/
		ramp_population_ptr->sortTrajectoriesInEachSubPopulation();
		/* 
			Step 3
			check termination criteria
			detect best cost difference
			at least 300 iterations to prevent premature convergence
		*/
		if (count > ramp_params.min_offline_iterations && count % converge_iter_th == 0)
		{
			if (std::abs(cur_best_traj_cost - ramp_population_ptr->getBestTrajectoryCost()) < 0.1)
			{
				ROS_INFO("Cost of best trajectory in population not improving in %d iterations. Consider converged.", converge_iter_th);
				break;
			}
			cur_best_traj_cost = ramp_population_ptr->getBestTrajectoryCost();
		}
		++count;
	}
	showPopulation(true);
	/* 
		Step 4
		execute best trajectory after offline run
	*/
	rampTrajectoryPtr exe_traj = ramp_population_ptr->getBestTrajectoryInSubPopulation("tc-main");
	// rampTrajectoryPtr exe_traj = ramp_population_ptr->getBestTrajectoryInPopulation();
	std::string exe_traj_sub_pop_type = ramp_population_ptr->getBestTrajectorySubPopulationType();
	if (ramp_params.verbose)
	{
		ROS_INFO("Best trajectory computed at end of offline run is:");
		exe_traj->printTrajectoryInfo(true);
	}

	ROS_INFO("Press Enter to conitnue.");
	getchar();
	ramp_rendering_ptr->renderJointTrajectoryGazebo(*exe_traj, exe_traj_sub_pop_type);
	if (ramp_params.is_hardware_enabled)
	{
		ramp_control_ptr->executeJointTrajectoryOnHardware(*exe_traj, exe_traj_sub_pop_type);
	}

	ROS_INFO("RAMP Offline Run Finished.");
	ros::Time t2 = ros::Time::now();
	stats.ramp_offline_run_time = (t2-t1).toSec();
	stats.num_of_offline_plan_cyc = count;
}

void rampCore::run()
{
	ros::Time t1 = ros::Time::now();
	std::cout << std::endl;
	ROS_INFO("RAMP Online Run Started.");
	ramp_params.is_online_run = true;
	double control_freq = ramp_params.control_frequency; 
	ros::Time cur_time = ros::Time::now();
	ros::Time last_control_time = ros::Time::now();
	int count = 0;
	geometry_msgs::Pose eef_pose;
	geometry_msgs::PoseArray eef_pose_array;
	while(ros::ok() && !ramp_goal.isGoalReached())
	{
		/*
			step 1
			process latest sensing information if it arrives from sensing topics
			process callbacks for sensing cycle 
			1) rampSensing::callback() 
			2) rampSensing::obsArmCallback()
			2) rampCore::robotStateCallback()
		*/
		ros::spinOnce(); 

		/*
			step 2
			do planning cycle once
		*/
		std::cout << std::endl;
		ROS_INFO("[Planning Cycle %d]-Started", stats.num_of_plan_cyc);
		ros::Time t1_pln = ros::Time::now();
		doPlanning();
		ros::Time t2_pln = ros::Time::now();
		stats.cum_planning_cyc_time += (t2_pln - t1_pln).toSec();
		ROS_INFO("[Planning Cycle %d]-Finished", stats.num_of_plan_cyc);
		std::cout << std::endl;
		++stats.num_of_plan_cyc;
		/*
			step 3 
			do control cycle once
		*/
		cur_time = ros::Time::now();
		if ( (cur_time-last_control_time).toSec() > (1.0/control_freq) || count == 0)
		{
			std::cout << std::endl;
			ROS_INFO("[Control Cycle %d]-Started", stats.num_of_ctrl_cyc);
			last_control_time = ros::Time::now();
			ros::Time t1_ctrl = ros::Time::now();
			doControl();
			ros::Time t2_ctrl = ros::Time::now();
			stats.cum_ctrl_cyc_time += (t2_ctrl - t1_ctrl).toSec();

			if(ramp_population_ptr->isWholePopulationInfeasible())
				stats.ctrl_cyc_ind_whole_pop_infeas.push_back(stats.num_of_ctrl_cyc);
	
			ROS_INFO("[Control Cycle %d]-Finished\n", stats.num_of_ctrl_cyc);
			++stats.num_of_ctrl_cyc;
		}
		/*
			step 4
			update robot current state 
		*/
		ramp_setting_ptr->setJointConfiguration(robot_cfg);
		ramp_setting_ptr->getEefPose(eef_pose);
		ramp_goal.setCurrentEefPose(eef_pose);

		/* 
			step 5
			get on-task-cstr time
		 */
		ros::Time t_task_cstr = ros::Time::now();
		ramp_params.on_task_cstr_time += (t_task_cstr-t1_pln).toSec();
		/*
			step 5
			collect poses
		*/
		eef_pose_array.poses.push_back(eef_pose);
		++count;
	}

	if (ramp_goal.isGoalReached())
	{
		ROS_INFO("Goal reached!");
		ROS_INFO("Final population:");
		showPopulation();

		/* 
			step 1
			visualize sequence of poses
		*/
		visualizeEefPosesDuringMotion(eef_pose_array);
		/*
			step 2
			show experiment stats
		*/
		ros::Time t2 = ros::Time::now();
		stats.ramp_online_run_time = (t2-t1).toSec();
		showExperimentStats(eef_pose_array);
		/* 
			step 3
			save executed path knot cfgs
		*/
		rampUtil::saveCfgsToFile(stats.robot_cfg_vec);
	}
	else
		ROS_ERROR("RAMP main loop stopped and failed to reach the goal.");
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_node");
	ros::NodeHandle n;
	ROS_INFO("Sleeping to wait for MoveIt and Rviz setup...");
	sleep(15.0);

	rampCore ramp_core;
	ramp_core.initialize();
	ramp_core.offlineRun();
	ramp_core.run();

	ros::waitForShutdown();
	return 0;
}

