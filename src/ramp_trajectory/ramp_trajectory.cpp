
/*
 * ramp_trajectory.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 #include "ramp_trajectory/ramp_trajectory.h"

rampTrajectory::rampTrajectory( const boost::shared_ptr<rampSetting> &r_set_p,
								const boost::shared_ptr<rampSensing> &r_sens_p)
{
	trajID = -1;
	ramp_setting_ptr = r_set_p;
	ramp_sensing_ptr = r_sens_p;
	nDofs = 0;
	ramp_setting_ptr->getNumberOfDofs(nDofs);
	jnt_names = ramp_setting_ptr->getJointNames();

	ROS_INFO("New trajectory data structure constructed with %d DOFs", nDofs);
	start_config = armCfg(nDofs,0.0);
	end_config = armCfg(nDofs,0.0);

	// read in joint velocity max
	double tmp_vmax = 0.;
	for (int i=0; i<jnt_names.size(); ++i)
	{
		tmp_vmax = ramp_setting_ptr->getRobotModelPtr()->getVariableBounds(jnt_names.at(i)).max_velocity_;
		jnt_vel_max.push_back(tmp_vmax);
	}

	// set cost function weights
	cost_weights.c1 = ramp_params.traj_energy_weight; // energy
	cost_weights.c2 = ramp_params.traj_time_weight; // time
	cost_weights.c3 = ramp_params.traj_manipulability_weight; // manipulability
	cost_weights.c4 = ramp_params.traj_task_cstr_weight; // task cstr
	cost_weights.c5 = ramp_params.traj_smoothness_weight; // task cstr

	// set cost function normalization factors
	cost_nrm_factors.a1 = ramp_params.traj_energy_nrm_factor;
	cost_nrm_factors.a2 = ramp_params.traj_time_nrm_factor;
	cost_nrm_factors.a3 = ramp_params.traj_manipulability_nrm_factor;
	cost_nrm_factors.a4 = ramp_params.traj_task_cstr_nrm_factor;
	cost_nrm_factors.a5 = ramp_params.traj_smoothness_nrm_factor;

	// set moment of inertia for each joint
	// use 1.0 for now as absolute values are not important
	joints_moment_of_inertia = std::vector<double>(nDofs, 1.0);

	// trajectory cost
	cost = 0.0; 
	energy_cost = 0., time_cost = 0., manipulability_cost = 0.;
	infeasibility_cost = 0., task_cstr_cost = 0., smoothness_cost = 0.;
	goal_reach_cost = 0.;
	tc_til_knot1 = 0.;
	feasibility = true; 
	colliding_time_from_beginning = 0.0;
}

void rampTrajectory::setTrajectory(const armCfg &start_cfg, const armCfg &end_cfg, const int &trajectoryID)
{
	if (start_cfg.size()==nDofs && end_cfg.size()==nDofs)
	{
		start_config = start_cfg;
		end_config = end_cfg;
		trajID = trajectoryID;
		ROS_INFO("Trajectory %d initialized with %d DOFs", trajID, nDofs);
		ROS_INFO("Trajectory %d initialized with start configuration:", trajID);
		rampUtil::printArmCfg(start_config);
		ROS_INFO("Trajectory %d initialized with end configuration:", trajID);
		rampUtil::printArmCfg(end_config);
	}
	else
		ROS_ERROR("Joint Configuration Dimension Mismatch in Setting Up Trajectory.");
}

void rampTrajectory::getJntNames(std::vector<std::string> &jnt_ns)
{
	jnt_ns = jnt_names;
}

int rampTrajectory::getNumOfDofs() const
{
	return nDofs;
}

void rampTrajectory::setTrajectoryID(const int &id)
{
	trajID = id;
}

void rampTrajectory::setCostFunWeights(const costFunctionWeights& new_weights)
{
	cost_weights.c1 = new_weights.c1; // energy
	cost_weights.c2 = new_weights.c2; // time
	cost_weights.c3 = new_weights.c3; // manipulability
	cost_weights.c4 = new_weights.c4; // task cstr
	cost_weights.c5 = new_weights.c5; // task cstr
}

int rampTrajectory::getTrajectoryID() const
{
	return trajID;
}

// time parametrization of a joint path
void rampTrajectory::generateTrajectoryFromPathNumerically(const std::vector<double> &start_cfg_vel)
{
	ROS_INFO("Generating trajectory %d with %zd knot cfgs from path numerically...", trajID, knot_cfgs.size());
	/********************************************* 
	  step 1
	  construct moveit_msgs::robotTrajectory
	  from start_config, knot_cfgs, end_config 
	*********************************************/
	trajectory_msgs::JointTrajectoryPoint jnt_pnt;
	moveit_msgs::RobotTrajectory traj_msg;

	traj_msg.joint_trajectory.joint_names = jnt_names;

	// start config
	jnt_pnt.positions = start_config;
	// start config velocity
	jnt_pnt.velocities = start_cfg_vel;
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	// place holder to be filled in after time parameterization
	jnt_pnt.velocities = std::vector<double>(start_config.size(), 0.0);
	// knot configs
	for (std::size_t i=0; i<knot_cfgs.size(); ++i)
	{
		jnt_pnt.positions = knot_cfgs.at(i);
		traj_msg.joint_trajectory.points.push_back(jnt_pnt);
	}

	// end config
	jnt_pnt.positions = end_config;
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	/*******************************************
	  step 2
	  Construct robot_trajector::robotTrajectory
	  from moveit_msgs::robotTrajectory
	********************************************/
	// make reference robot state
	robot_state::RobotStatePtr kine_state = robot_state::RobotStatePtr(new robot_state::RobotState(ramp_setting_ptr->getRobotModelPtr()));
	kine_state->setToRandomPositions();

	robot_trajectory::RobotTrajectory rob_traj(ramp_setting_ptr->getRobotModelPtr(), ramp_setting_ptr->getJointModelGroupName());
	rob_traj.setRobotTrajectoryMsg(*kine_state, traj_msg);

	#ifdef DEBUG
	// print way point in rob_traj
	std::vector<double> joint_values(nDofs, 0.0);
	for (std::size_t i=0; i<rob_traj.getWayPointCount(); ++i)
	{
		robot_state::RobotStatePtr tmp_state = rob_traj.getWayPointPtr(i);
		tmp_state->copyJointGroupPositions(joint_group_name, joint_values);
		for(int j=0; j<joint_values.size(); ++j)
		{
			std::cout << joint_values[j] << std::endl;
		}
		std::cout << std::endl;
	}
	#endif
	
	/******************************************************************************
	  step 3
	  Time-parameterize the path and get way 
	  point duration (time from previous state
	  to current state)
	  Method 1 - Iterative parabolic time parameterization
	  Method 2 - Spline parameterization (not too much difference in running time compared to Method 1)
	  It seems that method 2 has bugs and it is not maintained
	  after ROS Indigo anymore, so we should use Method 1 for now.
	  Method 1 is also the default method in MoveIt but it may 
	  generate non-smooth accelerations. See 
	  http://docs.ros.org/kinetic/api/moveit_tutorials/html/doc/time_parameterization_tutorial.html
	  Method 1 takes about 1 or 2 ms to time parameterize a path,
	  which may be an issue for real-time motion planning.
	*******************************************************************************/
	int tp_method = 1;
	bool tp_status = false;

	switch (tp_method)
	{
		case 1:
		{
			trajectory_processing::IterativeParabolicTimeParameterization iptp;
			if (iptp.computeTimeStamps(rob_traj))
				tp_status = true;
			break;
		}
		case 2:
		{
			//trajectory_processing::IterativeSplineParameterization sp;
			//if (sp.computeTimeStamps(rob_traj))
			//	tp_status = true;
			break;
		}
	}

	// collect results
	if (tp_status)
	{
		// ROS_INFO("way point count is %zd", rob_traj.getWayPointCount());
		// update durations for each cfg
		cfgs_durations = rob_traj.getWayPointDurations();
		
		#ifdef DEBUG
		for (std::deque<double>::iterator it=cfgs_durations.begin(); it!=cfgs_durations.end(); ++it)
		{
			ROS_INFO("Duration from previous state is %f", *it);
		}
		#endif

		// update velocities for each cfg
		cfgs_velocites.clear();
		std::vector<double> tmp_velocites;
		for (std::size_t i=0; i<rob_traj.getWayPointCount(); ++i)
		{
			tmp_velocites.clear();
			for (int j=0; j<nDofs; ++j)
				tmp_velocites.push_back(rob_traj.getWayPoint(i).getVariableVelocity(j));
			
			#ifdef DEBUG
			std::cout << "[ ";
			for(int k=0; k<nDofs; ++k)
			{
				std::cout << tmp_velocites.at(k) << " ";
			}
			std::cout << "]" << std::endl;
			#endif

			cfgs_velocites.push_back(tmp_velocites);
		}

		if (cfgs_velocites.size() != cfgs_durations.size())
			ROS_ERROR("The number of velocity vectors is not equal to the number of durations!");
	}
	else
		ROS_ERROR("Trajectory Time Parameterization from Path Failed.");
}

/*
	This function currently has the known issues
	1) computeTimeIntervalsBasedOnSlowestJoint() solely based on jnt_vmax cannot guarantee to
	satisfy the joint velocity constraints (currently by multiplying jnt_vmas by a scale of 0.25)
	2) this function is only faster than IterativeParabolicTimeParameterization in moveit
	with around 20 knot cfgs. it is even slower than iptp with 1000 knot cfgs due to the time 
	consuming inversion of a large matrix
*/
void rampTrajectory::generateTrajectoryFromPathAnalytically(std::vector<double> &start_cfg_vel, std::vector<double> &end_cfg_vel)
{
	if(ramp_params.verbose)
		ROS_INFO("Generating trajectory %d with %zd knot cfgs from path analytically...", trajID, knot_cfgs.size());
	/*
		step 1 
		compute time stamps for knot cfgs
		set class member cfgs_durations
	*/
	ros::Time t1 = ros::Time::now();
	computeTimeIntervalsBasedOnSlowestJoint(ramp_params.traj_jnt_vel_max_scale);

	/* reset the velocity container */
	cfgs_velocites.clear();
	int N = cfgs_durations.size(); // total number of cfgs
	if (N == 2)
	{
			cfgs_velocites.push_back(start_cfg_vel);
			cfgs_velocites.push_back(end_cfg_vel);
	}
	else
	{
		/*
			step 2
			build matrix A(h)
		*/
		int dim_A = N-2;
		Eigen::MatrixXd A = Eigen::MatrixXd::Zero(dim_A, dim_A);
		
		ros::Time t2 = ros::Time::now();
		Eigen::VectorXd s_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(start_cfg_vel.data(), nDofs);
		Eigen::VectorXd e_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(end_cfg_vel.data(), nDofs);
		ros::Time t2_1 = ros::Time::now();
		
		// fill in diagonal elements
		for (int i=0; i<dim_A; ++i)
		{
			double h_i_1 = cfgs_durations.at(i+1);
			double h_i_2 = cfgs_durations.at(i+2);
			A(i, i) = 2.0 * (h_i_1 + h_i_2);
		}

		// fill in one line above/below diagonal elements
		for (int i=0; i<(dim_A-1); ++i)
		{
			A(i,i+1) = cfgs_durations.at(i+1); // one line above
			A(i+1,i) = cfgs_durations.at(i+3); // one line below
		}
		ros::Time t2_2 = ros::Time::now();

		#ifdef DEBUG
		std::cout << "matrix A is " << std::endl;
		std::cout << A << std::endl;
		#endif

		/*
			step 3
			build matrix b(h,q,v_1, v_n)
		*/
		int dim_B = dim_A;
		Eigen::MatrixXd b = Eigen::MatrixXd::Zero(dim_B, nDofs);

		ros::Time t3 = ros::Time::now();
		Eigen::VectorXd end_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(end_config.data(), nDofs);
		Eigen::VectorXd start_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(start_config.data(), nDofs);
		ros::Time t3_1 = ros::Time::now();
		for (int i=0; i<dim_B; ++i)
		{
			double p1 = 3.0/(cfgs_durations.at(i+1) * cfgs_durations.at(i+2));
			Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i).data(), nDofs);

			Eigen::MatrixXd p2, p3, p4;
			if (i==(dim_B-1))
			{
				p2 = pow(cfgs_durations.at(i+1),2) * (end_v-knot_i);
			}
			else
			{
				Eigen::VectorXd knot_i_plus1 = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i+1).data(), nDofs);
				p2 = pow(cfgs_durations.at(i+1),2) * (knot_i_plus1-knot_i);
			}
		
			if (i==0)
			{
				p3 = pow(cfgs_durations.at(i+2),2) * (knot_i-start_v);
			}
			else 
			{
				Eigen::VectorXd knot_i_minus1 = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
				p3 = pow(cfgs_durations.at(i+2),2) * (knot_i-knot_i_minus1);
			}

			if (i==0)
				p4 = -cfgs_durations.at(i+2) * s_v;
			else if (i==(dim_B-1))
				p4 = -cfgs_durations.at(i+1) * e_v;
			else
				p4 = Eigen::MatrixXd::Zero(nDofs,1);

			#ifdef DEBUG
			std::cout << "p1 is " << p1 << std::endl;
			std::cout << "p2 is " << p2 << std::endl;
			std::cout << "p3 is " << p3 << std::endl;
			std::cout << "p4 is " << p4 << std::endl;
			#endif 

			b.row(i) = p1*(p2+p3).transpose()+p4.transpose();
		}
		ros::Time t3_2 = ros::Time::now();

		#ifdef DEBUG
		std::cout << "matrix b is " << std::endl;
		std::cout << b << std::endl;
		#endif

		/*
			step 4
			solve for unknown knot cfgs velocities
		*/
		ros::Time t4 = ros::Time::now();
		// Eigen::MatrixXd knot_cfg_vels = A.inverse()*b; // very slow
		Eigen::MatrixXd knot_cfg_vels = A.colPivHouseholderQr().solve(b); // moderately fast and better accuracy
		// Eigen::MatrixXd knot_cfg_vels = A.llt().solve(b); // A.llt() fastest but accuracy is low
		double relative_error_vel = (A*knot_cfg_vels-b).norm() / b.norm();
		if (relative_error_vel > 1e-6)
			ROS_ERROR("failed to solve for knot cfgs velocities.");
		else 
		{
			#ifdef DEBUG
			std::cout << "computed knot cfg vels are" << std::endl;
			std::cout << knot_cfg_vels << std::endl;
			#endif

			// update class member
			cfgs_velocites.push_back(start_cfg_vel);
			std::vector<double> tmp;
			tmp.resize(nDofs);
			for (int i=0; i<knot_cfg_vels.rows(); ++i)
			{
				Eigen::VectorXd::Map(&tmp[0], nDofs) = knot_cfg_vels.row(i);
				cfgs_velocites.push_back(tmp);
			}
			cfgs_velocites.push_back(end_cfg_vel);

			/*
				step 5
				collect analytical cubic polynomial equations
			*/
			ros::Time t5 = ros::Time::now();
			splines.clear();
			splines.resize(N-1);
			for (int i=0; i<splines.size(); ++i)
			{
				if (i==0)
				{
					splines.at(i).a0 = start_v;
					splines.at(i).a1 = s_v; 
				}
				else
				{
					Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
					splines.at(i).a0 = knot_i;
					splines.at(i).a1 = knot_cfg_vels.row(i-1); 
				}
				double h_k = cfgs_durations.at(i+1);
				Eigen::MatrixXd h(2,2);
				h << pow(h_k,2), pow(h_k,3), 2.0*h_k, 3.0*pow(h_k,2);

				Eigen::MatrixXd q(2,nDofs);
				if (i==0)
				{
					Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i).data(), nDofs);
					q << knot_i.transpose() - start_v.transpose() - h_k * s_v.transpose() , knot_cfg_vels.row(i) - s_v.transpose();
				}
				else if (i==(splines.size()-1))
				{
					Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
					q << end_v.transpose() - knot_i.transpose() - h_k * knot_cfg_vels.row(i-1) , e_v.transpose() - knot_cfg_vels.row(i-1);
				}
				else
				{
					Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i).data(), nDofs);
					Eigen::VectorXd knot_i_minus1 = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
					q << knot_i.transpose() - knot_i_minus1.transpose() - h_k * knot_cfg_vels.row(i-1) , knot_cfg_vels.row(i) - knot_cfg_vels.row(i-1);
				}
				#ifdef DEBUG
				std::cout << "matrix h is " << std::endl;
				std::cout << h << std::endl;

				std::cout << "matrix q is " << std::endl;
				std::cout << q << std::endl;
				#endif

				Eigen::MatrixXd a = h.inverse()*q;
				double relative_error_a = (h*a-q).norm() / q.norm();
				if (relative_error_a < 1e-6)
				{
					splines.at(i).a2 = a.row(0);
					splines.at(i).a3 = a.row(1);
				}
				else
				{
					ROS_ERROR("Relative error is %f (greater than 1e-6). Failed to solve h*a=q for cubic polynomial coefficients.", relative_error_a);
				}
			}
		}
	}
	/* Dimension matching check */
	if (cfgs_velocites.size() != cfgs_durations.size())
		ROS_ERROR("The number of velocity vectors is not equal to the number of durations!");
}

/*
	Note this function currently does not take into account the 
	start_cfg_vel and end_cfg_vel
	This may not respect acceleration limit ! Problem ! Caution !
*/
void rampTrajectory::computeTimeIntervalsBasedOnSlowestJoint(double jt_vmax_scale)
{
	cfgs_durations.clear();
	Eigen::VectorXd jt_vmax = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(jnt_vel_max.data(), nDofs);
	jt_vmax = jt_vmax_scale * jt_vmax;

	double small_float = 1e-3;
	bool has_same_knot_cfgs = false;

	if (knot_cfgs.size()!=0)
	{
		cfgs_durations.push_back(0.0); // for start_config
		// 1st interval
		double h_max = 0.0; // longest interval
		double tmp_h = 0.0;
		for (int i=0; i<nDofs; ++i)
		{
			tmp_h = std::abs(knot_cfgs.front()[i] - start_config[i]) / jt_vmax[i];
			if (tmp_h > h_max)
				h_max = tmp_h;
		}

		// safety-ensure h_max is not a very small number when joint angles are very close
		if (h_max < small_float)
		{
			has_same_knot_cfgs = true;
		}
		cfgs_durations.push_back(h_max);
		
		// intermediate intervals
		for (int i=0; i<(knot_cfgs.size()-1); ++i)
		{
			h_max = 0.;
			tmp_h = 0.;

			for (int j=0; j<nDofs; ++j)
			{
				tmp_h = std::abs(knot_cfgs.at(i+1)[j] - knot_cfgs.at(i)[j]) / jt_vmax[j];
				if (tmp_h > h_max)
					h_max = tmp_h;
			}
			if (h_max < small_float)
			{
				has_same_knot_cfgs = true;
			}
			cfgs_durations.push_back(h_max);
		}

		// last interval
		h_max = 0.;
		tmp_h = 0.;
		for (int i=0; i<nDofs; ++i)
		{
			tmp_h = std::abs(end_config[i] - knot_cfgs.back()[i]) / jt_vmax[i];
			if (tmp_h > h_max)
				h_max = tmp_h;
		}
		if (h_max < small_float)
		{
			has_same_knot_cfgs = true;
		}
		cfgs_durations.push_back(h_max);
	}
	else
	{
		cfgs_durations.push_back(0.0); // for start_config
		// the only one interval
		double h_max = 0.0; // longest interval
		double tmp_h = 0.0;
		for (int i=0; i<nDofs; ++i)
		{
			tmp_h = std::abs(end_config[i] - start_config[i]) / jt_vmax[i];
			if (tmp_h > h_max)
				h_max = tmp_h;
		}

		// safety-ensure h_max is not a very small number when joint angles are very close
		if (h_max < small_float)
		{
			ROS_ERROR("Start and end configs are too close to each other.");
		}
		cfgs_durations.push_back(h_max);
	}

	/*
		Handle the case where there are duplicate 
		nearby knot configs in this trajectory
	*/
	if (has_same_knot_cfgs)
	{
		std::vector<armCfg> knot_cfgs_no_repeat;
		double diff = 0.0;
		for (int i=0; i<knot_cfgs.size()-1; ++i)
		{
			for (int j=0; j<nDofs; ++j)
			{
				diff += std::abs(knot_cfgs[i+1].at(j) - knot_cfgs[i].at(j));
			}
			if (diff > 1e-3)
				knot_cfgs_no_repeat.push_back(knot_cfgs[i]);
			else
				ROS_WARN("Remove one duplicate knot cfg in trajectory [%d]", trajID);
			diff = 0.;
		}
		/*assign cleaned knot cfgs*/
		knot_cfgs = knot_cfgs_no_repeat;
		/*re-compute time stamps*/
		computeTimeIntervalsBasedOnSlowestJoint(ramp_params.traj_jnt_vel_max_scale);
	}

	// show results
	#ifdef DEBUG
	for (int i=0; i<cfgs_durations.size(); ++i)
	{
		ROS_INFO("duration from previous state for state %d is %f", i, cfgs_durations[i]);
	}
	#endif
}

/*
	This function should be used after time parameterization
*/
void rampTrajectory::convertToTrajectoryMsgs(bool is_first_state_ignored)
{
	m_traj_msg.points.clear();
	trajectory_msgs::JointTrajectoryPoint jnt_pnt;
	float time_from_beginning = 0.0;
	m_traj_msg.joint_names = jnt_names;
	m_traj_msg.header.stamp = ros::Time::now();

	if (!is_first_state_ignored)
	{
		// ROS_INFO("Consider first state when converting the trajectory msg");
		jnt_pnt.positions = start_config;
		jnt_pnt.velocities = cfgs_velocites.front();
		time_from_beginning = cfgs_durations.front();
		jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
		m_traj_msg.points.push_back(jnt_pnt);
	}
	else
		ROS_INFO("Ingnored first state when converting the trajectory msg");

	for (std::size_t i=0; i<knot_cfgs.size(); ++i)
	{
		jnt_pnt.positions = knot_cfgs.at(i);
		jnt_pnt.velocities = cfgs_velocites.at(i+1);
		time_from_beginning += cfgs_durations.at(i+1);
		jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
		m_traj_msg.points.push_back(jnt_pnt);
	}

	jnt_pnt.positions = end_config;
	jnt_pnt.velocities = cfgs_velocites.back();
	time_from_beginning += cfgs_durations.back();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	m_traj_msg.points.push_back(jnt_pnt);
}

void rampTrajectory::convertToTrajectoryMsgs(trajectory_msgs::JointTrajectory &traj_msg, bool is_first_state_ignored)
{
	convertToTrajectoryMsgs(is_first_state_ignored);
	traj_msg = m_traj_msg;
}

int rampTrajectory::locateIntervalByTime(double t)
{
	// locate time interval where t belongs to
	int time_interval_ind = -1;
	double elapsed_time = 0.0;
	for (int i=0; i<(cfgs_durations.size()-1); ++i)
	{
		if (   t > elapsed_time 
			&& t < (elapsed_time+cfgs_durations.at(i+1)) )
		{
			time_interval_ind = i;
			break;
		}
		elapsed_time += cfgs_durations.at(i+1);
	}

	if (time_interval_ind == -1)
	{
		ROS_ERROR("Unable to locate interval by time t.");
	}
	else
		return time_interval_ind;
}

void rampTrajectory::getJntCfgAtTimeT(double t, armCfg &cfg_t)
{
	if (knot_cfgs.size()==0)
		getJntCfgAtTimeT(t, cfg_t, "numerically");
	else
		getJntCfgAtTimeT(t, cfg_t, ramp_params.trajectory_generation_option);
}

void rampTrajectory::getJntCfgAtTimeT(double t, armCfg &cfg_t, std::string option)
{
	cfg_t.clear();
	// locate time interval where t belongs to
	int time_interval_ind = -1;
	double duration_in_interval = 0.0;
	double elapsed_time = 0.0;
	double t_proportion = 0.0;
	for (int i=0; i<(cfgs_durations.size()-1); ++i)
	{
		if (   t > elapsed_time 
			&& t < (elapsed_time+cfgs_durations.at(i+1)) )
		{
			time_interval_ind = i;
			duration_in_interval = t - elapsed_time;
			t_proportion = duration_in_interval / cfgs_durations.at(i+1);
			break;
		}
		elapsed_time += cfgs_durations.at(i+1);
	}

	if (option=="analytically")
	{
		if (time_interval_ind == -1)
		{
			ROS_ERROR("Failed to locate trajectory interval that contains time t.");
		}
		else
		{
			// printf("interval %d duration %f\n", time_interval_ind, duration_in_interval);
			const cubicPolynomial &cp_coeffs = splines.at(time_interval_ind);
			Eigen::VectorXd cfg_at_t = cp_coeffs.a0 + cp_coeffs.a1 * duration_in_interval +
									   cp_coeffs.a2 * pow(duration_in_interval,2) +
									   cp_coeffs.a3 * pow(duration_in_interval,3);
			cfg_t.resize(nDofs);
			Eigen::VectorXd::Map(&cfg_t[0], nDofs) = cfg_at_t;
		}
	}
	else if (option=="numerically")
	{
		if (time_interval_ind == -1)
		{
			ROS_ERROR("Failed to locate trajectory interval that contains time t.");
		}
		else
		{
			cfg_t.resize(nDofs);
			if (time_interval_ind==0) // first interval
			{
				if(knot_cfgs.size()!=0)
				{
					for (int k=0; k<nDofs; ++k)
					{
						float diff = knot_cfgs.front().at(k)-start_config.at(k);
						cfg_t.at(k) = start_config.at(k) + diff*t_proportion;
					}
				}
				else
				{
					for (int k=0; k<nDofs; ++k)
					{
						float diff = end_config.at(k)-start_config.at(k);
						cfg_t.at(k) = start_config.at(k) + diff*t_proportion;
					}
				}
			}
			else if (time_interval_ind==(cfgs_durations.size()-2) ) // last interval
			{
				if(knot_cfgs.size()!=0)
				{
					for (int k=0; k<nDofs; ++k)
					{
						float diff = end_config.at(k)-knot_cfgs.back().at(k);
						cfg_t.at(k) = knot_cfgs.back().at(k) + diff*t_proportion;
					}
				}
				else
				{
					for (int k=0; k<nDofs; ++k)
					{
						float diff = end_config.at(k)-start_config.at(k);
						cfg_t.at(k) = start_config.at(k) + diff*t_proportion;
					}
				}
			}
			else
			{
				for (int k=0; k<nDofs; ++k)
				{
					float diff = knot_cfgs.at(time_interval_ind).at(k)-knot_cfgs.at(time_interval_ind-1).at(k);
					cfg_t.at(k) = knot_cfgs.at(time_interval_ind-1).at(k) + diff*t_proportion;
				}
			}
		}
	}
	else
		ROS_ERROR("Unrecognized option in getJntCfgAtTimeT().");
}

bool rampTrajectory::wholeTrajectoryCollisionChecking()
{
	double cur_time = ramp_params.traj_collision_checking_start_time;
	/* 
		flag for identifying collison has been detected and no further check needed
	*/
	bool is_col_check_finished = false;
	while (cur_time < getTrajectoryDuration())
	{
		if (cur_time < ramp_params.traj_collison_checking_max_time) 
		{
			ramp_sensing_ptr->putObsPosesAtTimeT(cur_time);
			if (collisionChecking(cur_time))
			{
				is_col_check_finished =  true;
				return true;
				break;
			}
			else
				cur_time += ramp_params.traj_collison_checking_time_resolution;
		}
		else
			cur_time += ramp_params.traj_collison_checking_time_resolution;
	}

	if (!is_col_check_finished)
	{
		feasibility = true;
		return false;
	}
}

bool rampTrajectory::collisionChecking(const double& collision_check_time)
{
	std::string col_obj_id;
	armCfg col_cfg;
	if (collisionChecking(collision_check_time, col_obj_id, col_cfg))
		return true;
	else 
		return false;
}

/*
	including self collision
*/
bool rampTrajectory::collisionChecking( const double& collision_check_time,
										std::string &colliding_object_id,
										armCfg &colliding_cfg)
{
	/*
		step 1
		get robot jnt cfg at discrete time T
	*/
	armCfg cur_time_cfg;
	getJntCfgAtTimeT(collision_check_time, cur_time_cfg);
	/*
		step 2
		given a discrete jnt cfg and updated planning
		scene at this discrete time, check for collisions  
	*/
	if (ramp_setting_ptr->isRobotColliding(cur_time_cfg, ramp_sensing_ptr->getPlanningScenePtr()))
	{
		colliding_cfg = cur_time_cfg;
		if (ramp_params.verbose)
		{
			ROS_INFO("Trajectory [ID %d] is colliding at time %f", trajID, collision_check_time);
			ROS_INFO("  Colliding cfg is");
			rampUtil::printArmCfg(colliding_cfg);
		}
		feasibility = false;
		colliding_time_from_beginning = collision_check_time;
		return true;
	}
	else
	{
		colliding_object_id = "None";
		colliding_cfg.clear();
		feasibility = true;
		colliding_time_from_beginning = -1.0;
		return false;
	}
}

bool rampTrajectory::collisionCheckingThreadSafe(const double& collision_check_time)
{
	std::string col_obj_id;
	armCfg col_cfg;
	if (collisionCheckingThreadSafe(collision_check_time, col_obj_id, col_cfg))
		return true;
	else 
		return false;
}

bool rampTrajectory::collisionCheckingThreadSafe(const double &collision_check_time, std::string &colliding_object_id, armCfg &colliding_cfg)
{
	/*
		step 1
		get robot jnt cfg at discrete time T
	*/
	armCfg cur_time_cfg;
	getJntCfgAtTimeT(collision_check_time, cur_time_cfg);
	/*
		step 2
		given a discrete jnt cfg and updated planning
		scene at this discrete time, check for collisions  
	*/
	if (ramp_setting_ptr->isRobotCollidingThreadSafe(cur_time_cfg, ramp_sensing_ptr->getPlanningScenePtr()))
	{
		colliding_cfg = cur_time_cfg;
		if (ramp_params.verbose)
		{
			ROS_INFO("Trajectory [ID %d] is colliding at time %f", trajID, collision_check_time);
			ROS_INFO("  Colliding cfg is");
			rampUtil::printArmCfg(colliding_cfg);
		}
		feasibility = false;
		colliding_time_from_beginning = collision_check_time;
		return true;
	}
	else
	{
		colliding_object_id = "None";
		colliding_cfg.clear();
		feasibility = true;
		colliding_time_from_beginning = -1.0;
		return false;
	}
}

double rampTrajectory::getTrajectoryDuration() const
{
	double dur = 0.0;
	for (int i=0; i<cfgs_durations.size(); ++i)
		dur += cfgs_durations.at(i);
	return dur;
}

double rampTrajectory::getTrajectoryKineticEnergy() const
{
	double kinetic_energy = 0.0;
	double tmp = 0.0;
	for (int i=0; i<cfgs_velocites.size(); ++i)
	{
		tmp = 0.0;
		for (int j=0; j<nDofs; ++j)
		{
			// 0.5*I*w^2
			tmp += 0.5*joints_moment_of_inertia.at(j)*pow(cfgs_velocites.at(i).at(j),2);
		}
		kinetic_energy += tmp;
	}
	return kinetic_energy;
}

double rampTrajectory::getTrajectoryAverageManipulabilityMeasure() 
{
	double mani_meas = 0.0;
	double tmp = 0.0;
	for (int i=0; i<knot_cfgs.size(); ++i)
	{
		tmp = ramp_setting_ptr->getManipulabilityIndex(knot_cfgs.at(i));
		if (tmp > 0.0)
			mani_meas += tmp;
	}
	tmp = ramp_setting_ptr->getManipulabilityIndex(end_config);
	if (tmp > 0.0)
		mani_meas += tmp;

	int total_cfg = knot_cfgs.size()+1;
	return mani_meas / double(total_cfg);
}

double rampTrajectory::getInfeasibilityCost()
{
	if (feasibility==false && colliding_time_from_beginning > 0.0)
	{
		double Q = 10000000.0; // a big number
		return Q / colliding_time_from_beginning;
	}
	else
		return 0.0;
}

double rampTrajectory::getTrajectoryTaskCstrCost()
{
	double time_resolution = 0.5*ramp_params.traj_evaluate_time_resolution;
	double traj_duration = getTrajectoryDuration();
	double task_cstr_diff = 0., cur_cost = 0.0, cur_time = 0.05;
	armCfg cur_time_cfg;
	if (ramp_params.traj_evaluate_max_time < traj_duration)
		traj_duration = ramp_params.traj_evaluate_max_time;
	while (cur_time < traj_duration)
	{
		/*
			step 1
			get robot jnt cfg at discrete time T
			if no knot cfgs only get intermediate cfgs by numerical 
			interpolation since no splines were generated
		*/
		getJntCfgAtTimeT(cur_time, cur_time_cfg);

		/*
			step 2
			measure pose difference w.r.t task constraints
		*/
		ramp_setting_ptr->setJointConfiguration(cur_time_cfg);
		geometry_msgs::Pose cur_eef_pose;
		ramp_setting_ptr->getEefPose(cur_eef_pose);
		cur_cost = ramp_setting_ptr->compareCurrentPose(cur_eef_pose);
		
		// penalize large deviation from constrained pose
		if (cur_cost > 0.15)
			cur_cost = 10. * cur_cost;
		
		task_cstr_diff += cur_cost;
		cur_time += time_resolution;
	}
	return task_cstr_diff;
}

void rampTrajectory::getTrajectoryTaskCstrCostUntilKnot1()
{
	tc_til_knot1 = 0.;
	if (!knot_cfgs.empty())
	{
		double cur_time = 0.05, cur_cost = 0.;
		double knot1_dur = cfgs_durations.at(1);
		armCfg cur_time_cfg;
		while (cur_time < knot1_dur)
		{
			getJntCfgAtTimeT(cur_time, cur_time_cfg);
			ramp_setting_ptr->setJointConfiguration(cur_time_cfg);
			geometry_msgs::Pose cur_eef_pose;
			ramp_setting_ptr->getEefPose(cur_eef_pose);
			cur_cost = ramp_setting_ptr->compareCurrentPose(cur_eef_pose);
			// penalize large deviation from constrained pose
			if (cur_cost > 0.15)
				cur_cost = 10. * cur_cost;
			
			tc_til_knot1 += cur_cost;
			cur_time += 0.5*ramp_params.traj_evaluate_time_resolution;
		}
	}
	tc_til_knot1 = cost_weights.c4 * tc_til_knot1 / cost_nrm_factors.a4;
}

double rampTrajectory::getTrajectorySmoothnessCost()
{
	double time_resolution = ramp_params.traj_evaluate_time_resolution;
	double traj_duration = getTrajectoryDuration();
	double smooth_cost = 0., cur_cost = 0., prev_time = 0., cur_time = 0.05, next_time = 0.;
	armCfg cur_time_cfg, prev_time_cfg, next_time_cfg;
	if (ramp_params.traj_evaluate_max_time < traj_duration)
		traj_duration = ramp_params.traj_evaluate_max_time;
	while (cur_time < traj_duration)
	{
		/*
			step 1
			get robot jnt cfg at discrete time T
			if no knot cfgs only get intermediate cfgs by numerical 
			interpolation since no splines were generated
		*/
		getJntCfgAtTimeT(cur_time, cur_time_cfg);
		/* 
			step 2 
			get nearby cfgs 
		*/
		prev_time = cur_time - time_resolution;
		next_time = cur_time + time_resolution;
		if (prev_time < 0.)
			prev_time_cfg = start_config;
		else
			getJntCfgAtTimeT(prev_time, prev_time_cfg);
		
		if (next_time > traj_duration)
			next_time_cfg = end_config;
		else
			getJntCfgAtTimeT(next_time, next_time_cfg);
		/* 
			step 3
			compute actual cost
		*/
		armCfg tmp(nDofs, 0.);
		for (int i=0; i<nDofs; ++i)
			tmp.at(i) = prev_time_cfg.at(i)-2.0*cur_time_cfg.at(i)+next_time_cfg.at(i);
		cur_cost = 0.;
		for (int i=0; i<nDofs; ++i)
			cur_cost += tmp.at(i)*tmp.at(i);
		smooth_cost += cur_cost;
		cur_time += time_resolution;
	}
	return smooth_cost;
}

void rampTrajectory::getTrajectoryTaskCstrAndSmoothnessCost(double &task_cstr_cost, double &smoothness_cost)
{
	double time_resolution = ramp_params.traj_collison_checking_time_resolution;
	double traj_duration = getTrajectoryDuration();
	double smooth_cost = 0., cur_smooth_cost = 0., task_cstr_diff = 0., cur_tc_cost = 0., prev_time = 0., cur_time = 0.05, next_time = 0.;
	armCfg cur_time_cfg, prev_time_cfg, next_time_cfg;
	prev_time_cfg = start_config;
	if (cur_time > traj_duration)
	{
		ROS_ERROR("Cost computation starting time is longer than trajectory duration.");
		cur_time_cfg = end_config;
	}
	else
		getJntCfgAtTimeT(cur_time, cur_time_cfg);
	next_time = cur_time + time_resolution;
	if (next_time > traj_duration)
		next_time_cfg = end_config;
	else
		getJntCfgAtTimeT(next_time, next_time_cfg);
	
	while (cur_time < traj_duration)
	{
		/* 
			smoothness cost
		*/
		armCfg tmp(nDofs, 0.);
		for (int i=0; i<nDofs; ++i)
			tmp.at(i) = prev_time_cfg.at(i)-2.0*cur_time_cfg.at(i)+next_time_cfg.at(i);
		cur_smooth_cost = 0.;
		for (int i=0; i<nDofs; ++i)
			cur_smooth_cost += tmp.at(i)*tmp.at(i);
		smooth_cost += cur_smooth_cost;

		/* 
			task cstr cost 
		*/
		ramp_setting_ptr->setJointConfiguration(cur_time_cfg);
		geometry_msgs::Pose cur_eef_pose;
		ramp_setting_ptr->getEefPose(cur_eef_pose);
		cur_tc_cost = ramp_setting_ptr->compareCurrentPose(cur_eef_pose);
		if (cur_tc_cost > 0.15)
			cur_tc_cost = 10. * cur_tc_cost;
		task_cstr_diff += cur_tc_cost;

		/* move forward in loop */
		prev_time_cfg = cur_time_cfg;
		cur_time_cfg = next_time_cfg;
		
		cur_time += time_resolution;
		next_time = cur_time + time_resolution;
		if (next_time > traj_duration)
			next_time_cfg = end_config;
		else
			getJntCfgAtTimeT(next_time, next_time_cfg);
	}
	task_cstr_cost = task_cstr_diff;
	smoothness_cost = smooth_cost;
}

void rampTrajectory::evaluateTrajectory()
{
	energy_cost = cost_weights.c1 * getTrajectoryKineticEnergy() / cost_nrm_factors.a1;
	time_cost = cost_weights.c2 * getTrajectoryDuration() / cost_nrm_factors.a2;
	manipulability_cost = cost_weights.c3 * (1.0 / getTrajectoryAverageManipulabilityMeasure()) / cost_nrm_factors.a3;
	infeasibility_cost = getInfeasibilityCost();
	if (ramp_params.is_task_constrained)
		task_cstr_cost = cost_weights.c4 * getTrajectoryTaskCstrCost() / cost_nrm_factors.a4;
	else
		task_cstr_cost = 0.0;
	smoothness_cost = cost_weights.c5 * getTrajectorySmoothnessCost() / cost_nrm_factors.a5; 

	/* goal reach cost */
	goal_reach_cost = 0.;

	cost = energy_cost + time_cost + manipulability_cost + infeasibility_cost + task_cstr_cost + smoothness_cost + goal_reach_cost;
	if (ramp_params.verbose)
	{
		ROS_INFO("Trajectory %d total cost %.1f:", trajID, cost);
		ROS_INFO("  [eneregy] %.1f [time] %.1f [manipulability] %.1f [infeas.] %.1f [task cstr] %.1f [smoothness] %.1f [goal reach] %.1f", energy_cost, time_cost, manipulability_cost, infeasibility_cost, task_cstr_cost, smoothness_cost, goal_reach_cost);
	}

	/* compute some cost terms until knot 1 */
	getTrajectoryTaskCstrCostUntilKnot1();
}

void rampTrajectory::updateTrajectoryEvaluation()
{
	/* update some cost values */
	energy_cost = cost_weights.c1 * getTrajectoryKineticEnergy() / cost_nrm_factors.a1;
	time_cost = cost_weights.c2 * getTrajectoryDuration() / cost_nrm_factors.a2;
	infeasibility_cost = getInfeasibilityCost();
	if(ramp_params.verbose)
		ROS_INFO("Removing old tc cost til knot1 %f from total tc cost %f", tc_til_knot1, task_cstr_cost);

	if (ramp_params.is_task_constrained)
	{
		task_cstr_cost -= tc_til_knot1;
		getTrajectoryTaskCstrCostUntilKnot1();
		task_cstr_cost += tc_til_knot1;
		if(ramp_params.verbose)
			ROS_INFO("Adding new tc cost til knot1 %f to have total tc cost %f", tc_til_knot1, task_cstr_cost);
	}
	else
		task_cstr_cost = 0.0;
	
	/* update overall cost */
	cost = energy_cost + time_cost + manipulability_cost + infeasibility_cost + task_cstr_cost + smoothness_cost + goal_reach_cost;
	if (ramp_params.verbose)
	{
		ROS_INFO("[Evaluation Update] Trajectory %d total cost %.1f:", trajID, cost);
		ROS_INFO("  [eneregy] %.1f [time] %.1f [manipulability] %.1f [infeas.] %.1f [task cstr] %.1f [smoothness] %.1f [goal reach] %.1f", energy_cost, time_cost, manipulability_cost, infeasibility_cost, task_cstr_cost, smoothness_cost, goal_reach_cost);
	}
}

void rampTrajectory::printTrajectoryInfo(bool verbose)
{
	convertToTrajectoryMsgs(false);
	ROS_INFO("Trajectory %d Info:", trajID);
	ROS_INFO("Duration %f", getTrajectoryDuration());
	ROS_INFO("Number of knot cfgs %zd", knot_cfgs.size());
	if (feasibility)
		ROS_INFO("Fesibility: True");
	else
		ROS_INFO("Fesibility: False");
	ROS_INFO("Colliding time from beginning %f", colliding_time_from_beginning);
	ROS_INFO("Cost %f", cost);

	if (verbose)
	{
		for (int i=0; i<m_traj_msg.points.size(); ++i)
		{
			std::cout << "Configuration " << i << std::endl;
			std::cout << "Position:" << std::endl;
			std::cout << "[ ";
			for(int k=0; k<nDofs; ++k)
			{
				std::cout << m_traj_msg.points.at(i).positions.at(k) << " ";
			}
			std::cout << "]" << std::endl;

			std::cout << "Velocity:" << std::endl;
			std::cout << "[ ";
			for(int k=0; k<nDofs; ++k)
			{
				std::cout << m_traj_msg.points.at(i).velocities.at(k) << " ";
			}
			std::cout << "]" << std::endl;

			std::cout << "time_from_start:" << std::endl;
			std::cout << m_traj_msg.points.at(i).time_from_start << std::endl;

			std::cout << "task constraint cost in terms of roll pitch yaw angle difference:" << std::endl;
			ramp_setting_ptr->setJointConfiguration(m_traj_msg.points.at(i).positions);
			geometry_msgs::Pose cur_eef_pose;
			ramp_setting_ptr->getEefPose(cur_eef_pose);
			double task_cstr_difference = ramp_setting_ptr->compareCurrentPose(cur_eef_pose);
			std::cout << task_cstr_difference << std::endl;
			std::cout << "eef pose at this cfg is " << std::endl;
			rampUtil::printGeoMsgPose(cur_eef_pose);
			std::cout << std::endl;
		}
	}
}

void rampTrajectory::generateTrajectoryFromPath(std::vector<double> &start_cfg_vel,
												std::vector<double> &end_cfg_vel,
												const std::string &option)
{
	if (option == "analytically")
	{
		generateTrajectoryFromPathAnalytically(start_cfg_vel, end_cfg_vel);
	}
	else if (option == "numerically")
	{
		/* zero end_cfg_vel by default */
		generateTrajectoryFromPathNumerically(start_cfg_vel);
	}
	else
	{
		ROS_ERROR("Unrecognized option in trajectory generation");
	}
}

void rampTrajectory::saveTrajectoryToFile()
{
	const std::string file_path("/home/huitan/Dropbox/RAMP_DATA/saved_trajectories/");
	std::string spec_path = file_path+ramp_params.task_constraint_axis+"_traj_"+std::to_string(trajID)+".txt";
	ROS_INFO("Saving Trajectory [ID %d] knot cfgs to file %s", trajID, spec_path.c_str());
	std::ofstream save_file;
 	save_file.open (spec_path);
	for(int i=0; i<knot_cfgs.size(); ++i)
	{
		/* every nDofs is one knot cfg */
		for (int j=0; j<nDofs; ++j)
			save_file << knot_cfgs.at(i).at(j) << "\n";
	}
 	save_file.close();
}
