/*
 * ramp_control.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_control/ramp_control.h"

rampControl::rampControl()
: fol_jnt_traj_action_client(ramp_params.follow_joint_trajectory_action_hardware, true)
{
 	// wait for action server to come up
    while(!fol_jnt_traj_action_client.waitForServer(ros::Duration(5.0)))
    {
      ROS_INFO("Waiting for the joint_trajectory_action server on hardware");
    }
 	sleep(1.0); //sleep to set up the connection
 	cur_exe_traj_id = -1;
	cur_exe_traj_sub_pop_name = "";
}

void rampControl::executeJointTrajectoryOnHardware(rampTrajectory &ramp_traj, const std::string &traj_sub_pop_name)
{
	if (cur_exe_traj_id == ramp_traj.getTrajectoryID() && cur_exe_traj_sub_pop_name == traj_sub_pop_name)
	{
		ROS_INFO("Conitnue executing the same trajectory[ID %d] in subpopulation [Name %s] on hardware as last control cycle.", ramp_traj.getTrajectoryID(), cur_exe_traj_sub_pop_name.c_str());
	}
	else
	{
		cur_exe_traj_id = ramp_traj.getTrajectoryID();
		cur_exe_traj_sub_pop_name = traj_sub_pop_name;
		ROS_INFO("[Trajectory Switch]Sending trajectory[ID %d] for execution on hardware", cur_exe_traj_id);

		/* convert to trajectory msg */
		trajectory_msgs::JointTrajectory traj_msg;
		bool is_first_state_ignored = false;
		ramp_traj.convertToTrajectoryMsgs(traj_msg, is_first_state_ignored);
		
		/* make control msgs */
		control_msgs::FollowJointTrajectoryGoal traj_goal;
		traj_goal.trajectory = traj_msg;
		fol_jnt_traj_action_client.sendGoal(traj_goal);
	}
}

rampControl::~rampControl()
{

}
		



