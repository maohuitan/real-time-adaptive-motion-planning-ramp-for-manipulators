/*
 * ramp_test.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 #include "ramp_test/ramp_test.h"

rampTest::rampTest()
{

}

void rampTest::setRampSettingPtr(boost::shared_ptr<rampSetting> &r_set_p)
{
	ramp_setting_ptr = r_set_p;
}

void rampTest::setRampSamplingPtr(boost::shared_ptr<rampSampling> &r_samp_p)
{
	ramp_sampling_ptr = r_samp_p;
}

void rampTest::setRampTrajectoryPtr(boost::shared_ptr<rampTrajectory> &r_traj_p)
{
	ramp_traj_ptr = r_traj_p;
}

void rampTest::setRampOperatorsPtr(boost::shared_ptr<rampOperators> &r_oprt_p)
{
	ramp_oprt_ptr = r_oprt_p;
}

void rampTest::setRampRenderingPtr(boost::shared_ptr<rampRendering> &r_rdr_p)
{
	ramp_rdr_ptr = r_rdr_p;
}


