/*
 * ramp_sampling.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_sampling/ramp_sampling.h"

constrainedPose::constrainedPose(const std::vector<double> &base_pose_vec, const std::string &axis, const double &tolerance, const eefWorkSpace &eef_space)
:m_base_pose_vec(base_pose_vec), m_axis(axis), m_tolerance(tolerance)
{
	eef_cstr_space = eef_space;
}

void constrainedPose::getOneConstrainedPose(geometry_msgs::Pose &cstr_pose)
{
	/*
		Computation Principle
		tf_from_goal_to_world_under_soft_cstr = tf_from_base_goal_to_world
											  * tf_from_task_frame_to_base_goal;
	*/
	
	
	/* 
		step 1
		constrained rotation matrix (tf_from_task_frame_to_base_goal) 
	*/
	// a random number between -1 to 1
	float prop = -1.0 + 2.0*((float)rand())/RAND_MAX;

	std::vector<double> task_frame_vec(6,0.0); // XYZRPY
	if (m_axis == "X-O")
		task_frame_vec[3] += prop*m_tolerance;
	if (m_axis == "Y-O")
		task_frame_vec[4] += prop*m_tolerance;
	if (m_axis == "Z-O")
		task_frame_vec[5] += prop*m_tolerance;
	/* holding a cup of water case */
	if (m_axis == "XY-O")
	{
		task_frame_vec[3] += prop*m_tolerance;

		/* generate another random proportion */
		prop = -1.0 + 2.0*((float)rand())/RAND_MAX;
		task_frame_vec[4] += prop*m_tolerance;

		/* Z-O is free (not constrained)!!!*/
		task_frame_vec[5] = rampUtil::randomFloat(-1.56, 1.56);
	}
	/* closing drawer case */
	if (m_axis == "XZ-P-XYZ-O")
	{
		double position_tolerance = 0.01;
		task_frame_vec[0] += prop*position_tolerance;
		
		prop = -1.0 + 2.0*((float)rand())/RAND_MAX;
		task_frame_vec[2] += prop*position_tolerance;

		prop = -1.0 + 2.0*((float)rand())/RAND_MAX;
		task_frame_vec[3] += prop*m_tolerance;

		prop = -1.0 + 2.0*((float)rand())/RAND_MAX;
		task_frame_vec[4] += prop*m_tolerance;

		prop = -1.0 + 2.0*((float)rand())/RAND_MAX;
		task_frame_vec[5] += prop*m_tolerance;
	}

	if(ramp_params.verbose)
		ROS_INFO("Randomly generated constrained task frame XYZRPY is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",  
			 task_frame_vec[0], task_frame_vec[1], task_frame_vec[2],
			 task_frame_vec[3], task_frame_vec[4], task_frame_vec[5]);
	Eigen::Affine3d task_frame_tf;
	rampUtil::convertXYZRPYtoAffine3d(task_frame_vec, task_frame_tf);

	/* 
		step 2
		base pose tf (tf_from_base_goal_to_world) 
	*/
	Eigen::Affine3d base_pose_tf;
	Eigen::Matrix3d base_pose_rot;
	base_pose_rot =  Eigen::AngleAxisd(m_base_pose_vec[3], Eigen::Vector3d::UnitX())
					*Eigen::AngleAxisd(m_base_pose_vec[4], Eigen::Vector3d::UnitY())
					*Eigen::AngleAxisd(m_base_pose_vec[5], Eigen::Vector3d::UnitZ());
	base_pose_tf = base_pose_rot;
	base_pose_tf.translation() = Eigen::Vector3d(m_base_pose_vec[0],m_base_pose_vec[1],m_base_pose_vec[2]);
	
	/* 
		step 3 
		new constrained pose (tf_from_goal_to_world_under_soft_cstr)
	*/
	Eigen::Affine3d cstr_pose_tf = base_pose_tf*task_frame_tf;

	/* 
		step 4 
		randomize the position of new cstr pose 
	*/
	Eigen::Vector3d rand_position;
	getRandomPositionInWorkSpace(rand_position);

	if (m_axis == "XY-O")
	{
		cstr_pose_tf.translation() = rand_position;
	}
	if (m_axis == "XZ-P-XYZ-O")
	{
		cstr_pose_tf.translation()[1] = rand_position[1];
	}

	/*
		convert eigen::affine3d to geometry_msgs::Pose
	*/
	rampUtil::convertAffine3dtoQuaternion(cstr_pose_tf, cstr_pose);
}

void constrainedPose::getRandomPositionInWorkSpace(Eigen::Vector3d &rand_pos)
{
	float x, y, z;
	x = rampUtil::randomFloat(eef_cstr_space.x_min, eef_cstr_space.x_max);
	y = rampUtil::randomFloat(eef_cstr_space.y_min, eef_cstr_space.y_max);
	z = rampUtil::randomFloat(eef_cstr_space.z_min, eef_cstr_space.z_max);
	rand_pos[0] = x;
	rand_pos[1] = y;
	rand_pos[2] = z;
}

rampSampling::rampSampling(const boost::shared_ptr<rampSetting> &rs_ptr)
{
	ramp_setting_ptr = rs_ptr;
}

void rampSampling::sampleRandomJointConfiguration(armCfg &jnt_cfg)
{
	armCfg cur_cfg;
	ramp_setting_ptr->getCurrentJointConfiguration(cur_cfg);
	ramp_setting_ptr->setRandomJointConfiguration();
	ramp_setting_ptr->getCurrentJointConfiguration(jnt_cfg);
	ramp_setting_ptr->setJointConfiguration(cur_cfg);
}

bool rampSampling::sampleJntCfgGivenEefPose(armCfg &jnt_cfg,const geometry_msgs::Pose &eef_pose)
{
	std::vector<armCfg> ik_jvs;
	if (ramp_setting_ptr->getAllIKJointConfigurations(ik_jvs, eef_pose))
	{
		if(ramp_params.verbose)	
			ROS_INFO("Sampling Joint Configuration given EEF Pose Successful.");
		/*randomly select one when multiple IK solutions exist*/
		int index = rand() % ik_jvs.size();
		jnt_cfg = ik_jvs.at(index);
		return true;
	}
	else
	{
		ROS_WARN("Sampling Joint Configuration given EEF Pose Failed");
		return false;
	}
}

void rampSampling::setConstrainedBasePose(const std::vector<double> &base_pose_vec,
										  const std::string &axis,
										  const double &tolerance,
										  const eefWorkSpace &eef_s)
{
	cstr_base_pose = boost::make_shared<constrainedPose>(base_pose_vec, axis, tolerance, eef_s);
}

bool rampSampling::sampleJntCfgUnderTaskCstrs(armCfg &jnt_cfg)
{
	if(ramp_params.verbose)	
	{
		ROS_INFO("[Sampling]-Started");
		ROS_INFO("Sampling jnt cfg under task constraints...");
	}
	geometry_msgs::Pose sampled_cstr_pose;
	cstr_base_pose->getOneConstrainedPose(sampled_cstr_pose);
	
	if(ramp_params.verbose)	
	{
		ROS_INFO("Sampled constrained pose is");
		rampUtil::printGeoMsgPose(sampled_cstr_pose);
	}

	if (sampleJntCfgGivenEefPose(jnt_cfg, sampled_cstr_pose))
	{
		if(ramp_params.verbose)
		{
			ROS_INFO("Sampled jnt cfg under task constraint is");
			rampUtil::printArmCfg(jnt_cfg);
		}
		return true;
	}
	else
	{
		ROS_WARN("Sampling jnt cfg under task constraints failed.");
		return false;
	}
	if(ramp_params.verbose)	
		ROS_INFO("[Sampling]-Finished");
}





 
