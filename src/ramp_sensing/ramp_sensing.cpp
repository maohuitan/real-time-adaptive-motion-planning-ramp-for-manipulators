/*
 * ramp_sensing.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_sensing/ramp_sensing.h"

rampSensing::rampSensing(robot_model::RobotModelPtr& robot_kinematic_model)
{
	/* uav-type obstacle */
	sensing_topic = ramp_params.sensing_topic;
	sub = ros::NodeHandle().subscribe(sensing_topic, 1, &rampSensing::callback, this);
	sub_optitrack = ros::NodeHandle().subscribe("/vrpn_client_node/RigidBody1/pose", 1, &rampSensing::optiTrackCallback, this);
	num_of_sensing_cyc = 0;

	/* arm-type obstacle */
	std::string obs_arm_sensing_topic = ramp_params.arm_obs_sensing_topic;
	sub_obs_arm = ros::NodeHandle().subscribe(obs_arm_sensing_topic, 1, &rampSensing::obsArmCallback, this);

	planning_scene_ptr = boost::make_shared<planning_scene::PlanningScene>(robot_kinematic_model);
}

void rampSensing::obsArmCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	ROS_INFO("Received info from /wam_joint_states");
	std::map<std::string,int>::iterator it;  

	/* update bbox for wam */
	it = obs_id_map.find("wam");
	if (it != obs_id_map.end())
	{
		int obs_index = it->second;
		/*obstacles.at(obs_index) is the bounding box*/
		armCfg cfg_vel(msg->velocity.size(), 0.0);
		// dont use gazebo published velocity. not reliable
		// for(int i=0; i<msg->velocity.size(); ++i)
		// {
		// 	if (std::abs(msg->velocity.at(i)) > 1e-2)
		// 		cfg_vel.at(i) = msg->velocity.at(i);
		// }

		obstacles.at(obs_index).ob_arm_ptr->setSensedRobotState(msg->position, cfg_vel);
		ROS_INFO("WAM config is");
		rampUtil::printArmCfg(msg->position);
		ROS_INFO("WAM config velocity is(not using Gazebo published vel for now)");
		rampUtil::printArmCfg(cfg_vel);
	}
}

void rampSensing::callback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
	/*
		for drone in hector_quadrotor, msg->pose is a vector and 
		has 3 elements, 1st is for "ground plane", 2nd is for "quadrotor", 
		3rd is "abb_irb120"
	*/
	ROS_INFO("[Sensing Cycle %d]-Started", num_of_sensing_cyc);
	std::map<std::string,int>::iterator it;
	for (int i=0; i<msg->name.size(); ++i)
	{	
		it = obs_id_map.find(msg->name.at(i));
		if (it != obs_id_map.end())
		{
			ROS_INFO("Sensing cycle %d updating obstacle %s", num_of_sensing_cyc, msg->name.at(i).c_str());
			int obs_index = it->second;

			ROS_INFO("   position [%.2f %.2f %.2f] orientation [%.2f %.2f %.2f %.2f]", 
					msg->pose[i].position.x, msg->pose[i].position.y, msg->pose[i].position.z,
					msg->pose[i].orientation.x,	msg->pose[i].orientation.y,	msg->pose[i].orientation.z,	msg->pose[i].orientation.w);
			ROS_INFO("   twist linear [%.2f %.2f %.2f] angular [%.2f %.2f %.2f]",
					msg->twist[i].linear.x,	msg->twist[i].linear.y,	msg->twist[i].linear.z,
					msg->twist[i].angular.x,msg->twist[i].angular.y,msg->twist[i].angular.z);
			
			/* filter out noisy values */
			geometry_msgs::Twist tmp_twist = msg->twist[i];
			if( std::abs(tmp_twist.linear.x) < 1e-2 )
				tmp_twist.linear.x = 0.0;
			if( std::abs(tmp_twist.linear.y) < 1e-2 )
				tmp_twist.linear.y = 0.0;
			if( std::abs(tmp_twist.linear.z) < 1e-2 )
				tmp_twist.linear.z = 0.0;

			/* for uav only */
			if (ramp_params.task_constraint_axis == "XZ-P-XYZ-O")
				tmp_twist.linear.x = 0.0;

			obstacles.at(obs_index).setSensedPose(msg->pose.at(i));
			obstacles.at(obs_index).setSensedVelocity(tmp_twist);
		}
		else
		{
			ROS_INFO("Sensing info %d not match with any registered obstacles.", i);
		}
	}
	ROS_INFO("[Sensing Cycle %d]-Finished", num_of_sensing_cyc);
	++num_of_sensing_cyc;
}

void rampSensing::optiTrackCallback(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
	ROS_INFO("[Sensing Cycle %d]-Started", num_of_sensing_cyc);
	std::map<std::string,int>::iterator it;
	it = obs_id_map.find("optitrack");
	if (it != obs_id_map.end())
	{
		ROS_INFO("Sensing cycle %d updating obstacle optiTrack", num_of_sensing_cyc);
		int obs_index = it->second;
		rampUtil::printGeoMsgPose(msg->pose);
		obstacles.at(obs_index).setSensedPose(msg->pose);
	}
	else
	{
		ROS_INFO("Sensing info not match with optitrack obstacle.");
	}
	ROS_INFO("[Sensing Cycle %d]-Finished", num_of_sensing_cyc);
	++num_of_sensing_cyc;
}

void rampSensing::addObstacleToPlanningScene(rampObstacle &rob)
{
	obstacles.push_back(rob);
	obs_id_map[rob.getID()] = obstacles.size()-1;

	if (rob.getType()=="box")
	{
		Eigen::Affine3d cube_pose;
		rampUtil::convertQuaterniontoAffine3d(rob.getCurrentPose(), cube_pose);
		// shapes::ShapePtr cube_shape(new shapes::Box(rob.getDimensions()[0], rob.getDimensions()[1], rob.getDimensions()[2]));
		planning_scene_ptr->getWorldNonConst()->addToObject(rob.getID(), rob.getPrimitiveShape(), cube_pose);
	}
	if (rob.getType()=="arm")
	{
		planning_scene_ptr->getWorldNonConst()->addToObject(rob.getID(), rob.ob_arm_ptr->getLinkShapes(), rob.ob_arm_ptr->getLinkShapePoses());
	}
}

void rampSensing::putObsPosesAtTimeT(const float &t)
{
	// obstacles.at(0) is the ground plane
	for(int i=1; i<obstacles.size(); ++i)
	{
		if (obstacles.at(i).getType()=="box")
		{
			geometry_msgs::Pose p;
			obstacles.at(i).getPoseAtTimeT(t,p);
			Eigen::Affine3d new_tf;
			rampUtil::convertQuaterniontoAffine3d(p, new_tf);
			if(!planning_scene_ptr->getWorldNonConst()->moveShapeInObject(obstacles.at(i).getID(), 
																		  obstacles.at(i).getPrimitiveShape(),
																		  new_tf))
			{
				if(ramp_params.verbose)
					ROS_WARN("Failed to update object %s pose. It has not been added to planning scene or it has been removed", obstacles.at(i).getID().c_str());
			}
		}
		if (obstacles.at(i).getType()=="arm")
		{
			geometry_msgs::Pose p;
			obstacles.at(i).getPoseAtTimeT(t,p);
			int link_shape_size = obstacles.at(i).ob_arm_ptr->getLinkShapes().size();
			for (int j=0; j<link_shape_size; ++j)
			{
				if(!planning_scene_ptr->getWorldNonConst()->moveShapeInObject(obstacles.at(i).getID(),
																			  obstacles.at(i).ob_arm_ptr->getLinkShapes()[j],
																			  obstacles.at(i).ob_arm_ptr->getLinkShapePoses()[j]))
				{
					if(ramp_params.verbose)
						ROS_WARN("Failed to update object %s pose. It has not been added to planning scene or it has been removed", obstacles.at(i).getID().c_str());
				}
			}
		}
	}
}

void rampSensing::updateObstacleInternalState()
{
	// no need to update ground plane 
	for(int i=1; i<obstacles.size(); ++i)
	{
		if ( obstacles.at(i).getID() != "static_box" )
			obstacles.at(i).updateInternalState();
	}
}

void rampSensing::addObsIntoPlanningSceneByID(const std::string& obs_id)
{
	std::map<std::string,int>::iterator it;
	it = obs_id_map.find(obs_id);
	if (it != obs_id_map.end())
	{
		int obs_index = it->second;
		// obstacles.at(obs_index).addToPlanningScene();
		if (obstacles.at(obs_index).getType()=="box")
		{
			Eigen::Affine3d cube_pose;
			rampUtil::convertQuaterniontoAffine3d(obstacles.at(obs_index).getCurrentPose(), cube_pose);
			planning_scene_ptr->getWorldNonConst()->addToObject(obstacles.at(obs_index).getID(), obstacles.at(obs_index).getPrimitiveShape(), cube_pose);
		}
		if (obstacles.at(obs_index).getType()=="arm")
		{
			planning_scene_ptr->getWorldNonConst()->addToObject(obstacles.at(obs_index).getID(),
																obstacles.at(obs_index).ob_arm_ptr->getLinkShapes(),
																obstacles.at(obs_index).ob_arm_ptr->getLinkShapePoses());
		}
	}
	ROS_INFO("Added obstacle %s to planning scene", obs_id.c_str());
}

void rampSensing::removeObsFromPlanningSceneByID(const std::string& obs_id)
{
	std::map<std::string,int>::iterator it;
	it = obs_id_map.find(obs_id);
	if (it != obs_id_map.end())
	{
		int obs_index = it->second;
		planning_scene_ptr->getWorldNonConst()->removeObject(obstacles.at(obs_index).getID());
	}
	ROS_INFO("Removed obstacle %s from planning scene", obs_id.c_str());
}

bool rampSensing::hasObjectByID(const std::string &obs_id)
{
	return planning_scene_ptr->getWorldNonConst()->hasObject(obs_id);
}

const boost::shared_ptr<planning_scene::PlanningScene>& rampSensing::getPlanningScenePtr()
{
	return planning_scene_ptr;
}





