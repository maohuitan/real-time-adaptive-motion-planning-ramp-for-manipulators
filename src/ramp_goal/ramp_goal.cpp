/*
 * ramp_goal.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_goal/ramp_goal.h"

rampGoal::rampGoal()
{

}

void rampGoal::setGoalEefPose(const geometry_msgs::Pose &pose)
{
	goal_pose = pose;
}

void rampGoal::setCurrentEefPose(const geometry_msgs::Pose &pose)
{
	current_pose = pose;
}

bool rampGoal::isGoalReached()
{
	double diff = 0.0;
	diff =  pow( std::abs(goal_pose.position.x - current_pose.position.x), 2)
		  + pow( std::abs(goal_pose.position.y - current_pose.position.y), 2)
		  + pow( std::abs(goal_pose.position.z - current_pose.position.z), 2)
		  + pow( std::abs(goal_pose.orientation.x - current_pose.orientation.x), 2)
		  + pow( std::abs(goal_pose.orientation.y - current_pose.orientation.y), 2)
		  + pow( std::abs(goal_pose.orientation.z - current_pose.orientation.z), 2)
		  + pow( std::abs(goal_pose.orientation.w - current_pose.orientation.w), 2);
	if (sqrt(diff) < 1e-1)
		return true;
	else 
		return false;
}

