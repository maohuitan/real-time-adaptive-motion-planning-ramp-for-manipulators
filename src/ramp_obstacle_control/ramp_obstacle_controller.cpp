/*
 * ramp_obstacle_controller.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <trajectory_msgs/JointTrajectory.h>

void controlUAV(const ros::Publisher &pub)
{
	ros::Rate loop_rate(0.3);
	int count = 1;
	geometry_msgs::Twist twist;
	twist.linear.x = 0.0;
	twist.linear.y = 0.0;
	twist.linear.z = 0.0;
	twist.angular.x = 0.0;
	twist.angular.y = 0.0;
	twist.angular.z = 0.0;
	double speed = 0.3;
	pub.publish(twist);
	ros::spinOnce();
	sleep(1.0);

	// fly up
	twist.linear.z = speed;
	pub.publish(twist);
	ros::spinOnce();

	twist.linear.z = 0.0;
	sleep(1.75);
	pub.publish(twist);
	ros::spinOnce();

	// fly along positive y
	ROS_INFO("Press enter to fly along positive Y for 10 secs");
	getchar();
	getchar();
	twist.linear.y = 0.15;
	pub.publish(twist);
	ros::spinOnce();
	sleep(15.0);
	twist.linear.y = 0.0;
	pub.publish(twist);
	ros::spinOnce();

	// while (ros::ok())
	// {
	// 	if (count%2 == 0)
	// 		twist.linear.y = -speed;
	// 	else
	// 		twist.linear.y = speed;
	// 	ROS_INFO("Sending vel cmd linear [%f, %f, %f] angular [%f, %f, %f]",
	// 			 twist.linear.x, twist.linear.y, twist.linear.z,
	// 			 twist.angular.x, twist.angular.y, twist.angular.z);
	// 	pub.publish(twist);
	// 	loop_rate.sleep();
	// 	++count;

	// 	ros::spinOnce();
	// }
}

void controlWAM(const ros::Publisher &pub)
{
	trajectory_msgs::JointTrajectory traj;
	trajectory_msgs::JointTrajectoryPoint traj_pnt;
	traj.header.stamp = ros::Time::now();
	traj.joint_names.resize(7);
	traj.joint_names.at(0) = "J1";
	traj.joint_names.at(1) = "J2";
	traj.joint_names.at(2) = "J3";
	traj.joint_names.at(3) = "J4";
	traj.joint_names.at(4) = "J5";
	traj.joint_names.at(5) = "J6";
	traj.joint_names.at(6) = "J7";

	double cfg1[] = { 0.6, 0.7, 0.0, 2.0, 0.0, 0.0, 0.0};
	double cfg2[] = { 0.0, 0.4, 0.0, 2.4, 0.0, 0.0, 0.0};
	double cfg3[] = {-0.6, 0.7, 0.0, 2.0, 0.0, 0.0, 0.0};
	double cfg4[] = { 0.0, 0.7, 0.0, 2.4, 0.0, 0.0, 0.0};
	std::vector<double> c1(cfg1, cfg1+7);
	std::vector<double> c2(cfg2, cfg2+7);
	std::vector<double> c3(cfg3, cfg3+7);
	std::vector<double> c4(cfg4, cfg4+7);

	std::vector<std::vector<double> > cs;
	cs.push_back(c1);
	cs.push_back(c2);
	cs.push_back(c3);
	cs.push_back(c4);
	double time_elapsed = 0.1;

	traj_pnt.positions = cs.at(0);
	traj_pnt.time_from_start = ros::Duration(time_elapsed);
	traj.points.push_back(traj_pnt);

	std::vector<double> diff1(7, 0.0);
	std::vector<double> diff2(7, 0.0);
	std::vector<double> diff3(7, 0.0);
	std::vector<double> diff4(7, 0.0);
	for (int i=0; i<7; ++i)
	{
		diff1.at(i) = cs.at(1).at(i) - cs.at(0).at(i);
		diff2.at(i) = cs.at(2).at(i) - cs.at(1).at(i);
		diff3.at(i) = cs.at(3).at(i) - cs.at(2).at(i);
		diff4.at(i) = cs.at(0).at(i) - cs.at(3).at(i);
	}

	int numb_interpolation = 15;
	// 1st interval
	std::vector<double> tmp = cs.at(0);
	for (int i=0; i<numb_interpolation; ++i)
	{
		time_elapsed += 0.1;
		for (int j=0; j<7; ++j)
			tmp[j] +=  (float)1.0/numb_interpolation * diff1[j];

		traj_pnt.positions = tmp;
		traj_pnt.time_from_start = ros::Duration(time_elapsed);
		traj.points.push_back(traj_pnt);
	}

	// 2nd interval
	tmp = cs.at(1);
	for (int i=0; i<numb_interpolation; ++i)
	{
		time_elapsed += 0.1;
		for (int j=0; j<7; ++j)
			tmp[j] +=  (float)1.0/numb_interpolation * diff2[j];

		traj_pnt.positions = tmp;
		traj_pnt.time_from_start = ros::Duration(time_elapsed);
		traj.points.push_back(traj_pnt);
	}

	// 3rd interval
	tmp = cs.at(2);
	for (int i=0; i<numb_interpolation; ++i)
	{
		time_elapsed += 0.1;
		for (int j=0; j<7; ++j)
			tmp[j] +=  (float)1.0/numb_interpolation * diff3[j];

		traj_pnt.positions = tmp;
		traj_pnt.time_from_start = ros::Duration(time_elapsed);
		traj.points.push_back(traj_pnt);
	}

	// 4th interval
	tmp = cs.at(3);
	for (int i=0; i<numb_interpolation; ++i)
	{
		time_elapsed += 0.1;
		for (int j=0; j<7; ++j)
			tmp[j] +=  (float)1.0/numb_interpolation * diff4[j];

		traj_pnt.positions = tmp;
		traj_pnt.time_from_start = ros::Duration(time_elapsed);
		traj.points.push_back(traj_pnt);
	}

	ros::Rate loop_rate(0.16);
	while (ros::ok())
	{
		pub.publish(traj);
		ROS_INFO("WAM trajectory published.");
		loop_rate.sleep();
	}
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_obstacle_controller");
	ros::NodeHandle n;

	ros::Publisher pub_uav = n.advertise<geometry_msgs::Twist>("/uav1/cmd_vel", 1000);
	ros::Publisher pub_wam = n.advertise<trajectory_msgs::JointTrajectory>("/joint_traj", 1000);

	int choice = 0;
	std::cout << "1-control uav" << std::endl;
	std::cout << "2-control wam" << std::endl;
	std::cin >> choice;
	switch (choice)
	{
		case 1:
		{
			controlUAV(pub_uav);
			break;
		}
		case 2:
		{
			controlWAM(pub_wam);
			break;
		}
	}

	return 0;
}