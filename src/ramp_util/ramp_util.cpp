/*
 * ramp_util.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_util/ramp_util.h"

void rampUtil::convertXYZRPYtoAffine3d(const std::vector<double> &pose_vec, Eigen::Affine3d &pose_tf)
{
	Eigen::Matrix3d rot;
	rot = Eigen::AngleAxisd(pose_vec[3], Eigen::Vector3d::UnitX())
		 *Eigen::AngleAxisd(pose_vec[4], Eigen::Vector3d::UnitY())
		 *Eigen::AngleAxisd(pose_vec[5], Eigen::Vector3d::UnitZ());
	pose_tf = rot;
	pose_tf.translation() = Eigen::Vector3d(pose_vec[0],pose_vec[1],pose_vec[2]);
}

void rampUtil::convertAffine3dtoQuaternion(const Eigen::Affine3d &pose_tf, geometry_msgs::Pose &pose_quat)
{
	pose_quat.position.x = pose_tf.translation()[0];
	pose_quat.position.y = pose_tf.translation()[1];
	pose_quat.position.z = pose_tf.translation()[2];
	Eigen::Quaterniond q(pose_tf.rotation());
	pose_quat.orientation.x = q.x();
	pose_quat.orientation.y = q.y();
	pose_quat.orientation.z = q.z();
	pose_quat.orientation.w = q.w();
}

void rampUtil::convertXYZRPYtoQuaternion(const std::vector<double> &pose_vec, geometry_msgs::Pose &pose_quat)
{
	Eigen::Affine3d tmp;
	convertXYZRPYtoAffine3d(pose_vec, tmp);
	convertAffine3dtoQuaternion(tmp, pose_quat);
}

void rampUtil::convertQuaterniontoXYZRPY(const geometry_msgs::Pose &pose_quat, std::vector<double> &pose_vec)
{
	pose_vec.resize(6, 0.0);
	pose_vec.at(0) = pose_quat.position.x;
	pose_vec.at(1) = pose_quat.position.y;
	pose_vec.at(2) = pose_quat.position.z;
	Eigen::Quaterniond q(pose_quat.orientation.w, pose_quat.orientation.x,
						 pose_quat.orientation.y, pose_quat.orientation.z); // w x y z 

	Eigen::Vector3d euler_ang = q.toRotationMatrix().eulerAngles(0, 1, 2);
	pose_vec.at(3) = euler_ang[0];
	pose_vec.at(4) = euler_ang[1];
	pose_vec.at(5) = euler_ang[2];
}

void rampUtil::convertAffine3dtoXYZRPY(const Eigen::Affine3d &pose_tf, std::vector<double> &pose_vec)
{
	pose_vec.resize(6,0.0);
	pose_vec.at(0) = pose_tf.translation()[0];
	pose_vec.at(1) = pose_tf.translation()[1];
	pose_vec.at(2) = pose_tf.translation()[2];

	Eigen::Vector3d euler_ang = pose_tf.rotation().eulerAngles(0,1,2);
	pose_vec.at(3) = euler_ang[0];
	pose_vec.at(4) = euler_ang[1];
	pose_vec.at(5) = euler_ang[2];
}

void rampUtil::convertQuaterniontoAffine3d(const geometry_msgs::Pose &pose_quat, Eigen::Affine3d &pose_tf)
{
	Eigen::Quaterniond q(pose_quat.orientation.w, pose_quat.orientation.x,
						 pose_quat.orientation.y, pose_quat.orientation.z);
	pose_tf = q.toRotationMatrix();
	pose_tf.translation() = Eigen::Vector3d(pose_quat.position.x, pose_quat.position.y, pose_quat.position.z);
}

void rampUtil::printGeoMsgPose(const geometry_msgs::Pose &p)
{
	ROS_INFO("Pose is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",p.position.x,
													p.position.y,
													p.position.z,
													p.orientation.x,
													p.orientation.y,
													p.orientation.z,
													p.orientation.w);
}

void rampUtil::printGeoMsgTwist(const geometry_msgs::Twist &t)
{
	ROS_INFO("Twist is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",t.linear.x,
												 t.linear.y,
												 t.linear.z,
												 t.angular.x,
												 t.angular.y,
												 t.angular.z);
}

void rampUtil::printArmCfg(const armCfg &jv)
{
	if (jv.size()==6)
		ROS_INFO("jnt vector is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",  jv[0],
																		jv[1],
																		jv[2],
																		jv[3],
																		jv[4],
																		jv[5]);
	if (jv.size()==7)
		ROS_INFO("jnt vector is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",jv[0],
																			jv[1],
																			jv[2],
																			jv[3],
																			jv[4],
																			jv[5],
																			jv[6]);
	if (jv.size()==8)
		ROS_INFO("jnt vector is [%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f]",jv[0],
																				  jv[1],
																				  jv[2],
																				  jv[3],
																				  jv[4],
																				  jv[5],
																				  jv[6],
																				  jv[7]);
}

void rampUtil::printArmCfgs(const std::vector<armCfg> &jvs)
{
	for (int i=0; i<jvs.size(); ++i)
		printArmCfg(jvs.at(i));
}

float rampUtil::randomFloat(float a, float b) 
{
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

void rampUtil::saveCfgsToFile(std::vector<armCfg> &cfgs)
{
	const std::string file_path("/home/huitan/Dropbox/RAMP_DATA/sequentail_knowledge_transfer/with_skt/");
	std::string spec_path = file_path+"XZ-P-XYZ-O_exe_path_cfgs_.txt";
	ROS_INFO("Saving executed path knot cfgs to file %s", spec_path.c_str());
	if (!cfgs.empty())
	{
		int n_dofs = cfgs.front().size();
		std::ofstream save_file;
		save_file.open (spec_path);
		for(int i=0; i<cfgs.size(); ++i)
		{
			/* every n_dofs is one knot cfg */
			for (int j=0; j<n_dofs; ++j)
				save_file << cfgs.at(i).at(j) << "\n";
		}
		save_file.close();
	}
	else
		ROS_ERROR("Empty cfgs to save!");

}

bool rampUtil::areSamePoses(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2)
{
	float p1, p2, p3, p4, p5, p6, p7;
	p1 = 0.;
	p2 = 0.;
	p3 = 0.;
	p4 = 0.;
	p5 = 0.;
	p6 = 0.;
	p7 = 0.;

	// compare position difference
	p5 = std::abs(pose1.position.x - pose2.position.x);
	p6 = std::abs(pose1.position.y - pose2.position.y);
	p7 = std::abs(pose1.position.z - pose2.position.z);

	// compare orientation difference
	p1 = std::abs(pose1.orientation.x - pose2.orientation.x);
	p2 = std::abs(pose1.orientation.y - pose2.orientation.y);
	p3 = std::abs(pose1.orientation.z - pose2.orientation.z);
	p4 = std::abs(pose1.orientation.w - pose2.orientation.w);
	float diff = std::abs(sqrt(p1*p1+p2*p2+p3*p3+p4*p4+p5*p5+p6*p6+p7*p7));
	if (diff < 0.1)
		return true;
	else
		return false;
}

bool rampUtil::areSamePositions(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2)
{
	float p5, p6, p7;
	p5 = 0.;
	p6 = 0.;
	p7 = 0.;

	// compare position difference
	p5 = std::abs(pose1.position.x - pose2.position.x);
	p6 = std::abs(pose1.position.y - pose2.position.y);
	p7 = std::abs(pose1.position.z - pose2.position.z);

	float diff = std::abs(sqrt(p5*p5+p6*p6+p7*p7));
	if (diff < 0.01)
		return true;
	else
		return false;
}








 
