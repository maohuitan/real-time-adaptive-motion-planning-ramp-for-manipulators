/*
 * ramp_operators.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_operators/ramp_operators.h"

rampOperators::rampOperators(const boost::shared_ptr<rampSampling> &ramp_spl_ptr)
{
	ramp_sampling_ptr = ramp_spl_ptr;
}

void rampOperators::registerTrajectory(const boost::shared_ptr<rampTrajectory> &candidate_traj_1)
{
	ROS_INFO("Registering trajectory [ID %d] with ramp operators.",candidate_traj_1->getTrajectoryID());
	traj_ptr = candidate_traj_1;
}

void rampOperators::dropTrajectory()
{
	traj_ptr.reset();
	ROS_INFO("Operator trajectory pointer reset.");
}

bool rampOperators::applyDelete()
{
	int size_knot_cfg = traj_ptr->knot_cfgs.size();
	if (size_knot_cfg==0)
	{
		ROS_WARN("[DELETE]Empty Knot Configuration. Cannot apply delete. Leave trajectory unchanged.");
		return false;
	}
	else if (size_knot_cfg==1)
	{
		ROS_WARN("[DELETE]Only 1 Knot Configuration.");
		// ROS_WARN("Leave trajectory unchanged.");
		traj_ptr->knot_cfgs.clear();
		size_knot_cfg = traj_ptr->knot_cfgs.size();
		ROS_INFO("[DELETE]One configuration has been deleted. %d knot configurations now.", size_knot_cfg);
		return true;
	}
	else
	{
		int index = rand() % size_knot_cfg;
		traj_ptr->knot_cfgs.erase(traj_ptr->knot_cfgs.begin()+index);
		size_knot_cfg = traj_ptr->knot_cfgs.size();
		ROS_INFO("[DELETE]One configuration has been deleted. %d knot configurations now.", size_knot_cfg);
		return true;
	}
}

bool rampOperators::applySwap()
{
	int size_knot_cfg = traj_ptr->knot_cfgs.size();
	if (size_knot_cfg==0)
	{
		ROS_WARN("[SWAP]Empty Knot Configurations. Leave trajectory unchanged.");
		return false;
	}
	else if (size_knot_cfg==1)
	{
		ROS_WARN("[SWAP]Only one knot cfg. Cannot apply swap operator. Leave trajectory unchanged.");
		return false;
	}
	else
	{
		int index = rand() % size_knot_cfg;
		// if last knot configuration, swap with previous
		// else swap with after
		if (index == (size_knot_cfg-1))
		{
			armCfg tmp = traj_ptr->knot_cfgs.at(index);
			traj_ptr->knot_cfgs.at(index) = traj_ptr->knot_cfgs.at(index-1);
			traj_ptr->knot_cfgs.at(index-1) = tmp;
			ROS_INFO("[SWAP]Knot configuration at index %d and %d are swapped.", index-1, index);
		}
		else
		{
			armCfg tmp = traj_ptr->knot_cfgs.at(index);
			traj_ptr->knot_cfgs.at(index) = traj_ptr->knot_cfgs.at(index+1);
			traj_ptr->knot_cfgs.at(index+1) = tmp;
			ROS_INFO("[SWAP]Knot configuration at index %d and %d are swapped.", index, index+1);
		}
		return true;
	}
}

bool rampOperators::applyCrossOver(rampTrajectory &candidate_traj_2)
{
	ROS_INFO("Crossing over Trajectory [ID %d][size %zd] and [ID %d][size %zd].", traj_ptr->getTrajectoryID(), traj_ptr->knot_cfgs.size(), candidate_traj_2.getTrajectoryID(), candidate_traj_2.knot_cfgs.size());
	
	std::vector<armCfg> traj1_seg1, traj1_seg2, traj2_seg1, traj2_seg2;
	int ind1, ind2;
	ind1 = -1;
	ind2 = -1;

	// process traj1
	int size_traj1 = traj_ptr->knot_cfgs.size();
	if (size_traj1==0)
		ROS_WARN("Empty Knot Configurations of Trajectory 1.");
	else
	{
		ind1 = rand() % (size_traj1); // nor starting config nor ending config
		std::vector<armCfg>::const_iterator first = traj_ptr->knot_cfgs.begin();
		std::vector<armCfg>::const_iterator middle = traj_ptr->knot_cfgs.begin()+ind1;
		std::vector<armCfg>::const_iterator end = traj_ptr->knot_cfgs.end();
		traj1_seg1 = std::vector<armCfg>(first, middle);
		traj1_seg2 = std::vector<armCfg>(middle, end);
	}

	// process traj2
	int size_traj2 = candidate_traj_2.knot_cfgs.size();
	if (size_traj2==0)
		ROS_WARN("Empty Knot Configurations of Trajectory 2.");
	else
	{
		ind2 = rand() % (size_traj2); // nor starting config nor ending config
		std::vector<armCfg>::const_iterator first = candidate_traj_2.knot_cfgs.begin();
		std::vector<armCfg>::const_iterator middle = candidate_traj_2.knot_cfgs.begin()+ind2;
		std::vector<armCfg>::const_iterator end = candidate_traj_2.knot_cfgs.end();
		traj2_seg1 = std::vector<armCfg>(first, middle);
		traj2_seg2 = std::vector<armCfg>(middle, end);
	}

	// swap
	traj_ptr->knot_cfgs.clear();
	traj_ptr->knot_cfgs.insert(traj_ptr->knot_cfgs.end(), traj1_seg1.begin(), traj1_seg1.end());
	traj_ptr->knot_cfgs.insert(traj_ptr->knot_cfgs.end(), traj2_seg2.begin(), traj2_seg2.end());

	candidate_traj_2.knot_cfgs.clear();
	candidate_traj_2.knot_cfgs.insert(candidate_traj_2.knot_cfgs.end(), traj2_seg1.begin(), traj2_seg1.end());
	candidate_traj_2.knot_cfgs.insert(candidate_traj_2.knot_cfgs.end(), traj1_seg2.begin(), traj1_seg2.end());

	ROS_INFO("[CROSSOVER]Finished Crossing over Trajectory [ID %d][size %zd] and [ID %d][size %zd].", traj_ptr->getTrajectoryID(), traj_ptr->knot_cfgs.size(), candidate_traj_2.getTrajectoryID(), candidate_traj_2.knot_cfgs.size());
	ROS_INFO("[CROSSOVER]Split at %d and %d respectively.", ind1, ind2);
	if (ind1 == -1 && ind2 == -1)
		return false;
	else 
		return true;
}

bool rampOperators::applyRandomInsert()
{
	++stats.num_of_ins_chg_oper;
	armCfg cfg;
	bool is_sampling_successful = false;
	ROS_INFO("Sampling cfg randomly.");
	ramp_sampling_ptr->sampleRandomJointConfiguration(cfg);
	is_sampling_successful = true; // always true if sample a random jnt cfg
	
	if (is_sampling_successful)
	{
		int size_knot_cfg = traj_ptr->knot_cfgs.size();
		if (size_knot_cfg==0)
		{
			ROS_WARN("[INSERT]Empty Knot Configurations. Inserting one random configuration.");
			traj_ptr->knot_cfgs.push_back(cfg);
			size_knot_cfg = traj_ptr->knot_cfgs.size();
			ROS_INFO("[INSERT]One configuration has been inserted. %d knot configurations now.", size_knot_cfg);
		}
		else
		{
			int index = rand() % size_knot_cfg;
			traj_ptr->knot_cfgs.insert(traj_ptr->knot_cfgs.begin()+index, cfg);
			size_knot_cfg = traj_ptr->knot_cfgs.size();
			ROS_INFO("[INSERT]One configuration has been inserted. %d knot configurations now.", size_knot_cfg);
		}
		++stats.num_of_success_ins_chg_oper;
		return true;
	}
	else
	{
		ROS_WARN("[INSERT]Failed to insert due to sampling error.");
		return false;
	}
}

bool rampOperators::applyTaskConstrainedInsert()
{
	++stats.num_of_ins_chg_oper;
	armCfg cfg;
	bool is_sampling_successful = false;
	if (ramp_params.is_task_constrained)
	{
		ROS_INFO("Sampling cfg under task constraints.");
		if (ramp_sampling_ptr->sampleJntCfgUnderTaskCstrs(cfg))
			is_sampling_successful = true;
	}
	else
		ROS_ERROR("Applying a task-cosntrained insert in a non-task-constrained case!");
	
	if (is_sampling_successful)
	{
		int size_knot_cfg = traj_ptr->knot_cfgs.size();
		if (size_knot_cfg==0)
		{
			ROS_WARN("[INSERT]Empty Knot Configurations. Inserting one random configuration.");
			traj_ptr->knot_cfgs.push_back(cfg);
			size_knot_cfg = traj_ptr->knot_cfgs.size();
			ROS_INFO("[INSERT]One configuration has been inserted. %d knot configurations now.", size_knot_cfg);
		}
		else
		{
			int index = rand() % size_knot_cfg;
			traj_ptr->knot_cfgs.insert(traj_ptr->knot_cfgs.begin()+index, cfg);
			size_knot_cfg = traj_ptr->knot_cfgs.size();
			ROS_INFO("[INSERT]One configuration has been inserted. %d knot configurations now.", size_knot_cfg);
		}
		++stats.num_of_success_ins_chg_oper;
		return true;
	}
	else
	{
		ROS_WARN("[INSERT]Failed to insert due to sampling error.");
		return false;
	}
}

bool rampOperators::applyRandomChange()
{
	++stats.num_of_ins_chg_oper;
	armCfg cfg;
	bool is_sampling_successful = false;
	ramp_sampling_ptr->sampleRandomJointConfiguration(cfg);
	is_sampling_successful = true;

	if (is_sampling_successful)
	{
		++stats.num_of_success_ins_chg_oper;
		int size_knot_cfg = traj_ptr->knot_cfgs.size();
		if (size_knot_cfg==0)
		{
			ROS_WARN("[CHANGE]Empty Knot Configurations. Leave trajectory unchanged.");
			return false;
		}
		else
		{
			int index = rand() % size_knot_cfg;
			traj_ptr->knot_cfgs.at(index) = cfg;
			ROS_INFO("[CHANGE]Knot configuration at index %d has been changed.", index);
			return true;
		}
	}
	else
	{
		ROS_WARN("[CHANGE]Failed to change due to sampling error.");
		return false;
	}
}

bool rampOperators::applyTaskConstrainedChange()
{
	++stats.num_of_ins_chg_oper;
	armCfg cfg;
	bool is_sampling_successful = false;
	if (ramp_params.is_task_constrained)
	{
		ROS_INFO("Sampling cfg under task constraints.");
		if (ramp_sampling_ptr->sampleJntCfgUnderTaskCstrs(cfg))
			is_sampling_successful = true;
	}
	else
		ROS_ERROR("Applying a task-cosntrained change in a non-task-constrained case!");

	if (is_sampling_successful)
	{
		++stats.num_of_success_ins_chg_oper;
		int size_knot_cfg = traj_ptr->knot_cfgs.size();
		if (size_knot_cfg==0)
		{
			ROS_WARN("[CHANGE]Empty Knot Configurations. Leave trajectory unchanged.");
			return false;
		}
		else
		{
			int index = rand() % size_knot_cfg;
			traj_ptr->knot_cfgs.at(index) = cfg;
			ROS_INFO("[CHANGE]Knot configuration at index %d has been changed.", index);
			return true;
		}
	}
	else
	{
		ROS_WARN("[CHANGE]Failed to change due to sampling error.");
		return false;
	}
}

/*
void rampOperators::applyStop()
{
	int size_knot_cfg = traj_ptr->knot_cfgs.size();
	if (size_knot_cfg==0)
		ROS_ERROR("Empty Knot Configurations.");
	else
	{
		// find a random knot cfg to apply Stop
		int index = rand() % size_knot_cfg;

		int nds = traj_ptr->getNumOfDofs();
		std::vector<std::string> js;
		traj_ptr->getJntNames(js);

		rampTrajectory t1(nds, js, traj_ptr->m_kine_mod, traj_ptr->m_joint_group_name);		
		rampTrajectory t2(nds, js, traj_ptr->m_kine_mod, traj_ptr->m_joint_group_name);		
		t1.setTrajectory(traj_ptr->start_config, traj_ptr->knot_cfgs.at(index),0); // dummy ID
		t2.setTrajectory(traj_ptr->knot_cfgs.at(index),traj_ptr->end_config,0);
		t1.knot_cfgs.insert(t1.knot_cfgs.end(),
							traj_ptr->knot_cfgs.begin(),
							traj_ptr->knot_cfgs.begin()+(index-1));
		t2.knot_cfgs.insert(t2.knot_cfgs.end(),
							traj_ptr->knot_cfgs.begin()+(index+1),
							traj_ptr->knot_cfgs.end());
	
		std::vector<double> start_cfg_v(traj_ptr->getNumOfDofs(), 0.0);
		t1.generateTrajectoryFromPathNumerically(start_cfg_v);
		t2.generateTrajectoryFromPathNumerically(start_cfg_v);

		// modify original traj_ptr
		traj_ptr->knot_cfgs.clear();
		traj_ptr->knot_cfgs = t1.knot_cfgs;
		traj_ptr->knot_cfgs.push_back(t1.end_config);
		traj_ptr->knot_cfgs.push_back(t2.start_config);
		traj_ptr->knot_cfgs.insert(traj_ptr->knot_cfgs.end(),
								   t2.knot_cfgs.begin(),
								   t2.knot_cfgs.end());
		
		traj_ptr->cfgs_durations = t1.cfgs_durations;
		t2.cfgs_durations.front() = 5.0; // randomly stop for 5 secs
		traj_ptr->cfgs_durations.insert(traj_ptr->cfgs_durations.end(),
										t2.cfgs_durations.begin(),
										t2.cfgs_durations.end());

		traj_ptr->cfgs_velocites = t1.cfgs_velocites;
		traj_ptr->cfgs_velocites.insert(traj_ptr->cfgs_velocites.end(),
										t2.cfgs_velocites.begin(),
										t2.cfgs_velocites.end());

		ROS_INFO("Finished applying stop operator at knot cfg ind %d.", index);
	}
}
*/

