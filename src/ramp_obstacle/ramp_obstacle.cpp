/*
 * ramp_obstacle.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_obstacle/ramp_obstacle.h"

rampObstacleRobotArm::rampObstacleRobotArm(const std::string &robot_desp_name)
:arm_obs_model_loader(robot_desp_name)
{
	arm_obs_kine_model = arm_obs_model_loader.getModel();
	arm_obs_kine_state = robot_state::RobotStatePtr(new robot_state::RobotState(arm_obs_kine_model));
	arm_obs_jnt_model_group = arm_obs_kine_model->getJointModelGroup("arm");

	// initial arm obstacle cfg
	cur_cfg = armCfg(arm_obs_jnt_model_group->getVariableCount(), 0.0);
	cur_cfg_vel = armCfg(arm_obs_jnt_model_group->getVariableCount(), 0.0);
	arm_obs_kine_state->setJointGroupPositions(arm_obs_jnt_model_group, cur_cfg);
	cfg_update_cnt = 0;
	links = arm_obs_kine_model->getLinkModelsWithCollisionGeometry();

	std::vector<double> pose_arm_base_to_world(6,0.);
	pose_arm_base_to_world[0] = 1.0;
	pose_arm_base_to_world[5] = 3.14;
	rampUtil::convertXYZRPYtoAffine3d(pose_arm_base_to_world, tf_from_arm_base_to_world);
}

void rampObstacleRobotArm::setSensedRobotState(const armCfg &cfg, const armCfg &cfg_v)
{
	sensed_cfg = cfg;
	sensed_cfg_vel = cfg_v;
}

/* this is called when calling rampPopulation::evaluateWholePopulation() */
void rampObstacleRobotArm::updateInternalState()
{
	/* update velocity */
	cur_cfg_vel = armCfg(sensed_cfg_vel.size(), 0.0);
	if (cfg_update_cnt!=0)
	{
		ros::Time now = ros::Time::now();
		double time_elpsd = (now - last_cfg_update_time).toSec();
		for (int i=0; i<sensed_cfg_vel.size(); ++i)
		{
			cur_cfg_vel.at(i) = (sensed_cfg.at(i) - cur_cfg.at(i)) / time_elpsd;
		}
	}
	last_cfg_update_time = ros::Time::now();
	++cfg_update_cnt;
	
	/* update position */
	cur_cfg = sensed_cfg;
	// cur_cfg_vel = sensed_cfg_vel;

	if (ramp_params.verbose)
	{
		ROS_INFO("Updating cfg to be");
		rampUtil::printArmCfg(cur_cfg);
		ROS_INFO("Updating cfg vel to be");
		rampUtil::printArmCfg(cur_cfg_vel);
	}
}

void rampObstacleRobotArm::createBoundingBoxesForLinks()
{
	for (std::size_t i = 0; i < links.size(); ++i)
	{
		/* tf from link frame to wam base link */
		const Eigen::Affine3d& tf_from_link_to_arm_base = arm_obs_kine_state->getGlobalLinkTransform(links[i]);
		const Eigen::Vector3d& e = links[i]->getShapeExtentsAtOrigin();

		/* manually offset link collision bounding box to middle of the link*/
		Eigen::Affine3d tf_from_col_center_to_link = Eigen::Affine3d::Identity();
		if (links[i]->getName() == "wam/base_link")
		{
			tf_from_col_center_to_link.translation()[2] = e.z() / 2.0;
		}
		if (links[i]->getName() == "wam/upper_arm_link")
		{
			tf_from_col_center_to_link.translation()[2] = e.z() / 2.0;
		}
		if (links[i]->getName() == "wam/forearm_link")
		{
			tf_from_col_center_to_link.translation()[0] = -e.x() / 2.0;
			tf_from_col_center_to_link.translation()[1] = -(e.y() / 2.0 + 0.15);
		}
		if (links[i]->getName() == "wam/wrist_palm_link")
		{
			tf_from_col_center_to_link.translation()[2] = e.z() / 2.0;
		}

		Eigen::Affine3d tf_from_col_center_to_arm_base = tf_from_link_to_arm_base * tf_from_col_center_to_link;
		Eigen::Affine3d tf_from_col_center_to_world = tf_from_arm_base_to_world * tf_from_col_center_to_arm_base;

		shape_msgs::SolidPrimitive prim;
		prim.type = prim.BOX;  
		prim.dimensions.resize(3);
		prim.dimensions[0] = e.x();
		prim.dimensions[1] = e.y();
		prim.dimensions[2] = e.z();  

		/* seems moveit returned this link bounding box wrong. tweak it by hand */
		if (links[i]->getName() == "wam/forearm_link")
		{
			prim.dimensions[1] = e.y() + 0.3;
			prim.dimensions[2] = e.z() + 0.2;
		}

		/* scale up dimensions for safety */
		prim.dimensions[0] = 1.5 * prim.dimensions[0];
		prim.dimensions[1] = 1.5 * prim.dimensions[1];
		prim.dimensions[2] = 1.5 * prim.dimensions[2]; 

		/* collect result */
		shapes::ShapeConstPtr cube_shape = std::make_shared<shapes::Box>(prim.dimensions[0], prim.dimensions[1], prim.dimensions[2]);
		link_shapes.push_back(cube_shape);
		link_shape_poses.push_back(tf_from_col_center_to_world);
	}
}

const std::vector<shapes::ShapeConstPtr>& rampObstacleRobotArm::getLinkShapes()
{
	return link_shapes;
}

const EigenSTL::vector_Affine3d& rampObstacleRobotArm::getLinkShapePoses()
{
	return link_shape_poses;
}

void rampObstacleRobotArm::getBoundingBoxesForLinksAtTimeT(double t)
{
	armCfg cfg_t(cur_cfg.size(),0.0);
	for (int i=0; i<cur_cfg.size(); ++i)
	{
		cfg_t.at(i) = cur_cfg.at(i) + t * cur_cfg_vel.at(i);
	}
	getBoundingBoxesForLinksAtCfg(cfg_t);
}

void rampObstacleRobotArm::getBoundingBoxesForLinksAtCfg(const armCfg &cfg)
{
	link_shape_poses.clear();
	/* re-compute poses for each bounding box */
	arm_obs_kine_state->setJointGroupPositions(arm_obs_jnt_model_group, cfg);
	for (std::size_t i = 0; i < links.size(); ++i)
	{
		/* tf from link frame to wam base link */
		const Eigen::Affine3d& tf_from_link_to_arm_base = arm_obs_kine_state->getGlobalLinkTransform(links[i]);
		const Eigen::Vector3d& e = links[i]->getShapeExtentsAtOrigin();
		/* manually offset link collision bounding box to middle of the link*/
		Eigen::Affine3d tf_from_col_center_to_link = Eigen::Affine3d::Identity();
		if (links[i]->getName() == "wam/base_link")
		{
			tf_from_col_center_to_link.translation()[2] = e.z() / 2.0;
		}
		if (links[i]->getName() == "wam/upper_arm_link")
		{
			tf_from_col_center_to_link.translation()[2] = e.z() / 2.0;
		}
		if (links[i]->getName() == "wam/forearm_link")
		{
			tf_from_col_center_to_link.translation()[0] = -e.x() / 2.0;
			tf_from_col_center_to_link.translation()[1] = -(e.y() / 2.0 + 0.15);
		}
		if (links[i]->getName() == "wam/wrist_palm_link")
		{
			tf_from_col_center_to_link.translation()[2] = e.z() / 2.0;
		}

		Eigen::Affine3d tf_from_col_center_to_arm_base = tf_from_link_to_arm_base * tf_from_col_center_to_link;
		Eigen::Affine3d tf_from_col_center_to_world = tf_from_arm_base_to_world * tf_from_col_center_to_arm_base;
		link_shape_poses.push_back(tf_from_col_center_to_world);
	}
}

rampObstacleRobotArm::~rampObstacleRobotArm()
{

}



rampObstacle::rampObstacle( const std::string &obs_type, const std::string &obs_id, const std::string &obs_frame,
							const geometry_msgs::Pose &obs_pose, const Eigen::Vector3f &dimension)
: type(obs_type), id(obs_id), frame(obs_frame), pose(obs_pose), dimens(dimension)
{
	twist.linear.x = 0.0;
	twist.linear.y = 0.0;
	twist.linear.z = 0.0;
	twist.angular.x = 0.0;
	twist.angular.y = 0.0;
	twist.angular.z = 0.0;

	if (type=="box")
		primitive_shape = std::make_shared<shapes::Box>(dimens[0], dimens[1], dimens[2]);
	if (type=="arm")
	{
		ob_arm_ptr = boost::make_shared<rampObstacleRobotArm>(ramp_params.arm_obs_description_name);
		ob_arm_ptr->createBoundingBoxesForLinks();
	}
	twist_update_cnt = 0;
}

const std::string& rampObstacle::getID()
{
	return id;	
}

const std::string& rampObstacle::getType()
{
	return type;
}

const Eigen::Vector3f& rampObstacle::getDimensions()
{
	return dimens;
}

const geometry_msgs::Pose& rampObstacle::getCurrentPose()
{
	return pose;
}

const shapes::ShapePtr& rampObstacle::getPrimitiveShape()
{
	return primitive_shape;
}

void rampObstacle::setSensedPose(const geometry_msgs::Pose &obs_pose)
{
	if (ramp_params.test_case_name == "abb-optitrack")
	{
		/* apply a fixed transform to robot base frame */
		Eigen::Affine3d tf_wrt_tracking_frame;
		rampUtil::convertQuaterniontoAffine3d(obs_pose, tf_wrt_tracking_frame);

		Eigen::Matrix3d rot;
		rot << 0., 0., 1.,
			   1., 0., 0.,
			   0., 1., 0.;
		Eigen::Affine3d tf_from_tracking_frame_to_world_frame;
		tf_from_tracking_frame_to_world_frame = rot;
		tf_from_tracking_frame_to_world_frame.translation() = Eigen::Vector3d(0., 0., 0.);

		Eigen::Affine3d tf_wrt_world_frame = tf_from_tracking_frame_to_world_frame * tf_wrt_tracking_frame;

		geometry_msgs::Pose pose_wrt_world;
		rampUtil::convertAffine3dtoQuaternion(tf_wrt_world_frame, pose_wrt_world);
		sensed_pose = pose_wrt_world;
	}
	else
		sensed_pose = obs_pose;
}

void rampObstacle::setSensedVelocity(const geometry_msgs::Twist &obs_twist)
{
	sensed_twist = obs_twist;
}

void rampObstacle::setSensedDimension(const Eigen::Vector3f &dim)
{
	sensed_dimens = dim;
}

void rampObstacle::updateInternalState()
{
	if (ramp_params.verbose)
	{
		ROS_INFO("Updating Obstacle %s pose to be", id.c_str());
		rampUtil::printGeoMsgPose(sensed_pose);
		ROS_INFO("Updating Obstacle %s twist to be", id.c_str());
		rampUtil::printGeoMsgTwist(sensed_twist);
		// ROS_INFO("Updating Obstacle %s dimension to be", id.c_str());
		// std::cout << sensed_dimens << std::endl;
	}

	if(type == "arm")
	{
		ob_arm_ptr->updateInternalState();
	}
	else
	{
		pose = sensed_pose;
		if (ramp_params.use_published_velocity)
			twist = sensed_twist;
		else
		{
			/* update twist through internal difference */
			if (twist_update_cnt != 0)
			{
				ros::Time now = ros::Time::now();
				double time_elpsd = (now - last_twist_update_time).toSec();
				sensed_twist.linear.x = (sensed_pose.position.x - pose.position.x) / time_elpsd;
				sensed_twist.linear.y = (sensed_pose.position.y - pose.position.y) / time_elpsd;
				sensed_twist.linear.z = (sensed_pose.position.z - pose.position.z) / time_elpsd;
			}
			last_twist_update_time = ros::Time::now();
			++twist_update_cnt;

			twist = sensed_twist;
		}
		// dimens = sensed_dimens;
	}

	if (ramp_params.verbose)
	{
		ROS_INFO("Updated Obstacle %s pose is", id.c_str());
		rampUtil::printGeoMsgPose(pose);
		ROS_INFO("Updated Obstacle %s twist is", id.c_str());
		rampUtil::printGeoMsgTwist(twist);
		// ROS_INFO("Updating Obstacle %s dimension to be", id.c_str());
		// std::cout << sensed_dimens << std::endl;
	}
}

void rampObstacle::getPoseAtTimeT(double time_elapsed, geometry_msgs::Pose &new_pose)
{
	if (type=="arm")
	{
		ob_arm_ptr->getBoundingBoxesForLinksAtTimeT(time_elapsed);
	}
	else
	{
		// make a copy of actual pose
		// so poses used for checking dynamic collision
		// wont affect obstacle actual pose
		new_pose = pose;
		new_pose.position.x += twist.linear.x * time_elapsed;
		new_pose.position.y += twist.linear.y * time_elapsed;
		new_pose.position.z += twist.linear.z * time_elapsed;
	}
}

rampObstacle::~rampObstacle()
{
	
}




