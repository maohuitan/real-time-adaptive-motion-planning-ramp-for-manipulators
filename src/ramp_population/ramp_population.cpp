
/*
 * ramp_population.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_population/ramp_population.h"

rampPopulation::rampPopulation( const boost::shared_ptr<rampSetting>& r_set_ptr, 
								const boost::shared_ptr<rampSensing>& r_sens_ptr,
								const boost::shared_ptr<rampOperators>& r_oprt_ptr)
{
	sub_pop_names_ = ramp_params.population_name;
	if (ramp_params.population_size.size() != ramp_params.population_type.size())
		ROS_ERROR("Number of subpopulaiton required is not equal to the number types required!");
	else if (ramp_params.population_name.size() != ramp_params.population_type.size())
		ROS_ERROR("Number of subpopulaiton names required is not equal to the number types required!");
	else if (ramp_params.population_name.size() != ramp_params.population_size.size())
		ROS_ERROR("Number of subpopulaiton names required is not equal to the number sizes specified!");
	else
	{
		for (int i=0; i<sub_pop_names_.size(); ++i)
		{
			rampSubPopulation sub(sub_pop_names_[i],
								  ramp_params.population_size[i], 
								  ramp_params.init_traj_knot_size, 
								  ramp_params.population_type[i],
								  r_set_ptr,r_sens_ptr,r_oprt_ptr);
			sub_populations_.push_back(sub);
			sub_populations_map_[sub_pop_names_[i]] = i;
		}
		ramp_sensing_p = r_sens_ptr;
	}
	best_traj_id = -1;
	worst_traj_id = -1;
}

void rampPopulation::insertRandomCfgsToSubpopulation(const boost::shared_ptr<rampOperators>& r_oprt_ptr, const std::string &sub_p_name, const int &num_of_cfgs)
{
	ROS_INFO("Adding %d random cfgs to subpopulation %s", num_of_cfgs, sub_p_name.c_str());
	int sub_ind = sub_populations_map_[sub_p_name];
	sub_populations_[sub_ind].insertRandomCfgs(r_oprt_ptr, num_of_cfgs);
}

void rampPopulation::setStartRobotCfgInWholePopulation(const armCfg &start_cfg)
{
	for (int i=0; i<sub_populations_.size(); ++i)
	{
		for (int j=0; j<sub_populations_[i].num_of_trajs_; ++j)
		{
			sub_populations_[i].trajectories_[j]->start_config = start_cfg;
		}
	}
}

void rampPopulation::setGoalInSubPopulation(const boost::shared_ptr<rampSampling> &r_samp_ptr, 
											const geometry_msgs::Pose &goal,
											const std::string &sub_p_name)
{
	int sub_ind = sub_populations_map_[sub_p_name];
	armCfg ik_sol;
	for (int i=0; i<sub_populations_[sub_ind].num_of_trajs_; ++i)
	{
		bool sample_suc = r_samp_ptr->sampleJntCfgGivenEefPose(ik_sol, goal);
		if (sample_suc)
			sub_populations_[sub_ind].trajectories_.at(i)->end_config = ik_sol;
		else
			ROS_ERROR("Goal sampling failed.");
	}
}

void rampPopulation::setGoalsInWholePopulation(const boost::shared_ptr<rampSampling> &r_samp_ptr, const geometry_msgs::Pose &goal)
{
	armCfg ik_sol;
	for (int i=0; i<sub_pop_names_.size(); ++i)
	{
		int sub_ind = sub_populations_map_[sub_pop_names_[i]];
		ROS_INFO("Setting up goals in subpopulation [Name %s]", sub_pop_names_[i].c_str());
		for (int j=0; j<sub_populations_[sub_ind].num_of_trajs_; ++j)
		{
			bool sample_suc = false;
			if (sub_populations_[sub_ind].getType() == "tc")
				sample_suc = r_samp_ptr->sampleJntCfgGivenEefPose(ik_sol, goal);
			else if (sub_populations_[sub_ind].getType() == "ntc")
			{
				if (ramp_params.is_task_constrained) // task-constrained planning and ntc subpopulation
				{
					r_samp_ptr->sampleRandomJointConfiguration(ik_sol);
					sample_suc = true;
				}
				else // non task-constrained planning (to the goal directly)
					sample_suc = r_samp_ptr->sampleJntCfgGivenEefPose(ik_sol, goal);
			}
			else
				ROS_ERROR("Unrecognized subpopulation type.");

			if (sample_suc)
				sub_populations_[sub_ind].trajectories_.at(j)->end_config = ik_sol;
			else
				ROS_ERROR("Goal sampling failued.");
		}
	}
}

void rampPopulation::setEvaluationFuncWeightsInSubPopulation(const costFunctionWeights &new_weights, const std::string &sub_p_name)
{
	int sub_ind = sub_populations_map_[sub_p_name];
	sub_populations_[sub_ind].setEvaluationFunctionWeights(new_weights);
}	

void rampPopulation::generateTrajectories()
{
	for (int i=0; i<sub_populations_.size(); ++i)
		sub_populations_[i].generateTrajectories();
}

void rampPopulation::wholePopulationCollisionCheck()
{
	int total_num_of_trajs = totalNumberOfTrajs();
	int passed_pop_size = 0;
	/* flags for identifying which trajectories have collision already and no need further check */
	std::vector<bool> trajs_cc_finished(total_num_of_trajs, false);
	float cur_time = ramp_params.traj_collision_checking_start_time;
	float traj_dur_max = getTrajectoryDurationMax();
	float col_check_time_limit = 0.;
	if (traj_dur_max < ramp_params.population_collison_checking_max_time)
		col_check_time_limit = traj_dur_max;
	else
		col_check_time_limit = ramp_params.population_collison_checking_max_time;
	while (cur_time < col_check_time_limit)
	{
		/* reset cumulative size counter */
		passed_pop_size = 0;
		/* get planning scene at cur_time */
		ramp_sensing_p->putObsPosesAtTimeT(cur_time);
		/* do collision check with arm cfg at cur_time and planning scene at cur_time*/
		for (int i=0; i<sub_populations_.size(); ++i)
		{
			for (int j=0; j<sub_populations_[i].trajectories_.size(); ++j)
			{
				if (cur_time < sub_populations_[i].trajectories_[j]->getTrajectoryDuration() && !trajs_cc_finished.at(passed_pop_size+j))
				{
					/* generate feasibility and collision cost */
					if (sub_populations_[i].trajectories_[j]->collisionChecking(cur_time))
						trajs_cc_finished.at(passed_pop_size+j) = true;
				}
			}
			passed_pop_size += sub_populations_[i].trajectories_.size();
		}
		cur_time += ramp_params.traj_collison_checking_time_resolution;
	}
}

void rampPopulation::wholePopulationCollisionCheckMultipleThreads()
{
	resetAllTrajectoryFeasibility();
	float cur_time = ramp_params.traj_collision_checking_start_time;
	float traj_dur_max = getTrajectoryDurationMax();
	float col_check_time_limit = 0.;
	if (traj_dur_max < ramp_params.population_collison_checking_max_time)
		col_check_time_limit = traj_dur_max;
	else
		col_check_time_limit = ramp_params.population_collison_checking_max_time;
	while (cur_time < col_check_time_limit)
	{
		/* get planning scene at cur_time */
		ramp_sensing_p->putObsPosesAtTimeT(cur_time);
		/* do collision check with arm cfg at cur_time and planning scene at cur_time*/

		threads.clear();
		for (int i=0; i<sub_populations_.size(); ++i)
		{
			threads.push_back(std::thread(&rampPopulation::subPopulationCollisionCheckThreadSafe, this, i, cur_time));
		}

		for (auto &t:threads)
		{
			t.join();
		}

		cur_time += ramp_params.traj_collison_checking_time_resolution;
	}
}

void rampPopulation::subPopulationCollisionCheckThreadSafe(const int id, const float cur_time)
{
	for (int j=0; j<sub_populations_[id].trajectories_.size(); ++j)
	{
		if (cur_time < sub_populations_[id].trajectories_[j]->getTrajectoryDuration() && sub_populations_[id].trajectories_[j]->feasibility )
		{
			/* generate feasibility and collision cost */
			sub_populations_[id].trajectories_[j]->collisionCheckingThreadSafe(cur_time);
		}
	}
	// ROS_INFO("Subpopulation %d finished collision check at time %f secs", id, cur_time);
}

void rampPopulation::resetAllTrajectoryFeasibility()
{
	for (int i=0; i<sub_populations_.size(); ++i)
	{
		for (int j=0; j<sub_populations_[i].trajectories_.size(); ++j)
		{
			sub_populations_[i].trajectories_[j]->feasibility = true;
		}
	}
}

void rampPopulation::InitialEvaluateWholePopulation()
{
	ROS_INFO("Initial evaluating trajectories in population...");
	/*
		Step 1
		Update obstacle state from sensing information
	*/
	if (ramp_params.is_online_run)
		ramp_sensing_p->updateObstacleInternalState();
	/*
		Step 2
		Set robot and obstacles at time T (into the future)
		and do collision check
	*/
	ROS_INFO("Whole population collision checking...");
	ros::Time t1 = ros::Time::now();
	wholePopulationCollisionCheck();
	ros::Time t2 = ros::Time::now();
	/* 
		Step 3
		Eavluate each subpopulation
	*/
	for (int i=0; i<sub_populations_.size(); ++i)
		sub_populations_[i].evaluateTrajectories();
	ros::Time t3 = ros::Time::now();
	ROS_INFO("col check took %f s", (t2-t1).toSec());
	ROS_INFO("evaluation took %f s", (t3-t2).toSec());
	/* 
		Step 4
		Collect overall results from each subpopulation
	*/
	float best_pop_traj_cost = 1e9;
	float worst_pop_traj_cost = 0.;
	std::string new_best_traj_sub_pop_name, new_worst_traj_sub_pop_name;
	int new_best_traj_id = -1;
	int new_worst_traj_id = -1;
	for (int i=0; i<sub_pop_names_.size(); ++i)
	{
		int sub_pop_ind = sub_populations_map_[sub_pop_names_[i]];
		if (sub_populations_[sub_pop_ind].best_traj_cost_ < best_pop_traj_cost)
		{
			best_pop_traj_cost = sub_populations_[sub_pop_ind].best_traj_cost_;
			new_best_traj_sub_pop_name = sub_pop_names_[i];
		}
		if (sub_populations_[sub_pop_ind].worst_traj_cost_ > worst_pop_traj_cost)
		{
			worst_pop_traj_cost = sub_populations_[sub_pop_ind].worst_traj_cost_;
			new_worst_traj_sub_pop_name = sub_pop_names_[i];
		}
	}
	new_best_traj_id = sub_populations_[ sub_populations_map_[new_best_traj_sub_pop_name] ].best_traj_index_;
	new_worst_traj_id = sub_populations_[ sub_populations_map_[new_worst_traj_sub_pop_name] ].worst_traj_index_;
	
	/* for stats */
	if (best_traj_id != -1)
	{
		if (best_traj_id != new_best_traj_id || best_traj_sub_pop_name_ != new_best_traj_sub_pop_name)
		{
			++stats.num_of_traj_switches;
			if ( !sub_populations_[sub_populations_map_[best_traj_sub_pop_name_]].trajectories_[best_traj_id]->feasibility
			 	&& sub_populations_[sub_populations_map_[new_best_traj_sub_pop_name]].trajectories_[new_best_traj_id]->feasibility)
			 	++stats.traj_switch_due_to_infeas;
			else 
				++stats.traj_switch_due_to_better_traj;
		}
	}
	
	/* set results of this evaluation */
	best_traj_sub_pop_name_ = new_best_traj_sub_pop_name;
	worst_traj_sub_pop_name_ = new_worst_traj_sub_pop_name;
	best_traj_id = new_best_traj_id;
	worst_traj_id = new_worst_traj_id;
	ROS_INFO("Best trajectory [ID %d] in whole population is in [%s] subpopulation", best_traj_id, best_traj_sub_pop_name_.c_str());
	ROS_INFO("Worst trajectory [ID %d] in whole population is in [%s] subpopulation", worst_traj_id, worst_traj_sub_pop_name_.c_str());
	for (int i=0; i<sub_populations_.size(); ++i)
		ROS_INFO("[%s] subpopulation has %zd/%d infeasible trajectories.", sub_pop_names_[i].c_str(), 
				sub_populations_[i].infeasible_traj_indices_.size(), sub_populations_[i].num_of_trajs_);
}

void rampPopulation::sortTrajectoriesInEachSubPopulation()
{
	for (int i=0; i<sub_populations_.size(); ++i)
		sub_populations_[i].sortTrajectoriesByCosts();
	
	/* 
		Collect overall results from each subpopulation
	*/
	float best_pop_traj_cost = 1e9;
	float worst_pop_traj_cost = 0.;
	for (int i=0; i<sub_pop_names_.size(); ++i)
	{
		int sub_pop_ind = sub_populations_map_[sub_pop_names_[i]];
		if (sub_populations_[sub_pop_ind].best_traj_cost_ < best_pop_traj_cost)
		{
			best_pop_traj_cost = sub_populations_[sub_pop_ind].best_traj_cost_;
			best_traj_sub_pop_name_ = sub_pop_names_[i];
			best_traj_id = sub_populations_[sub_pop_ind].best_traj_index_;
		}
		if (sub_populations_[sub_pop_ind].worst_traj_cost_ > worst_pop_traj_cost)
		{
			worst_pop_traj_cost = sub_populations_[sub_pop_ind].worst_traj_cost_;
			worst_traj_sub_pop_name_ = sub_pop_names_[i];
			worst_traj_id = sub_populations_[sub_pop_ind].worst_traj_index_;;
		}
	}
	ROS_INFO("Best trajectory [ID %d] in whole population is in [%s] subpopulation", best_traj_id, best_traj_sub_pop_name_.c_str());
	ROS_INFO("Worst trajectory [ID %d] in whole population is in [%s] subpopulation", worst_traj_id, worst_traj_sub_pop_name_.c_str());
	for (int i=0; i<sub_populations_.size(); ++i)
		ROS_INFO("[%s] subpopulation has %zd/%d infeasible trajectories.", sub_pop_names_[i].c_str(), 
				sub_populations_[i].infeasible_traj_indices_.size(), sub_populations_[i].num_of_trajs_);
}

void rampPopulation::subPopulationTrajectoryEvaluation(const int id)
{
	sub_populations_[id].updateTrajectoriesEvaluation();
	// ROS_INFO("Subpopulation %d finished updating trajectory evaluation", id);
}

void rampPopulation::wholePopulationTrajectoryEvaluationMultipleThreads()
{
	threads.clear();
	for (int i=0; i<sub_populations_.size(); ++i)
	{
		threads.push_back(std::thread(&rampPopulation::subPopulationTrajectoryEvaluation, this, i));
	}

	for (auto &t:threads)
	{
		t.join();
	}
}

void rampPopulation::updateWholePopulationEvaluation()
{
	ROS_INFO("Updating trajectories evaluation in population...");
	/*
		Step 1
		Update obstacle state from sensing information
	*/
	if (ramp_params.is_online_run)
		ramp_sensing_p->updateObstacleInternalState();
	/*
		Step 2
		Set robot and obstacles at time T (into the future)
		and do collision check
	*/
	ROS_INFO("Whole population collision checking...");
	ros::Time t1 = ros::Time::now();
	// wholePopulationCollisionCheck(); // single threaded
	wholePopulationCollisionCheckMultipleThreads();
	ros::Time t2 = ros::Time::now();
	/* 
		Step 3
		Eavluate each subpopulation
	*/
	wholePopulationTrajectoryEvaluationMultipleThreads();

	ros::Time t3 = ros::Time::now();
	ROS_INFO("col check took %f s", (t2-t1).toSec());
	ROS_INFO("evaluation took %f s", (t3-t2).toSec());
	/* 
		Step 4
		Collect overall results from each subpopulation
	*/
	float best_pop_traj_cost = 1e9;
	float worst_pop_traj_cost = 0.;
	std::string new_best_traj_sub_pop_name, new_worst_traj_sub_pop_name;
	int new_best_traj_id = -1;
	int new_worst_traj_id = -1;
	for (int i=0; i<sub_pop_names_.size(); ++i)
	{
		int sub_pop_ind = sub_populations_map_[sub_pop_names_[i]];
		if (sub_populations_[sub_pop_ind].best_traj_cost_ < best_pop_traj_cost)
		{
			best_pop_traj_cost = sub_populations_[sub_pop_ind].best_traj_cost_;
			new_best_traj_sub_pop_name = sub_pop_names_[i];
		}
		if (sub_populations_[sub_pop_ind].worst_traj_cost_ > worst_pop_traj_cost)
		{
			worst_pop_traj_cost = sub_populations_[sub_pop_ind].worst_traj_cost_;
			new_worst_traj_sub_pop_name = sub_pop_names_[i];
		}
	}
	new_best_traj_id = sub_populations_[ sub_populations_map_[new_best_traj_sub_pop_name] ].best_traj_index_;
	new_worst_traj_id = sub_populations_[ sub_populations_map_[new_worst_traj_sub_pop_name] ].worst_traj_index_;

	/* for stats */
	if (best_traj_id != -1)
	{
		if (best_traj_id != new_best_traj_id || best_traj_sub_pop_name_ != new_best_traj_sub_pop_name)
		{
			++stats.num_of_traj_switches;
			if ( !sub_populations_[sub_populations_map_[best_traj_sub_pop_name_]].trajectories_[best_traj_id]->feasibility
			 	&& sub_populations_[sub_populations_map_[new_best_traj_sub_pop_name]].trajectories_[new_best_traj_id]->feasibility)
			 	++stats.traj_switch_due_to_infeas;
			else 
				++stats.traj_switch_due_to_better_traj;
		}
	}
	
	/* set results of this evaluation */
	best_traj_sub_pop_name_ = new_best_traj_sub_pop_name;
	worst_traj_sub_pop_name_ = new_worst_traj_sub_pop_name;
	best_traj_id = new_best_traj_id;
	worst_traj_id = new_worst_traj_id;
	ROS_INFO("Best trajectory [ID %d] in whole population is in [%s] subpopulation", best_traj_id, best_traj_sub_pop_name_.c_str());
	ROS_INFO("Worst trajectory [ID %d] in whole population is in [%s] subpopulation", worst_traj_id, worst_traj_sub_pop_name_.c_str());
	for (int i=0; i<sub_populations_.size(); ++i)
		ROS_INFO("[%s] subpopulation has %zd/%d infeasible trajectories.", sub_pop_names_[i].c_str(), 
				sub_populations_[i].infeasible_traj_indices_.size(), sub_populations_[i].num_of_trajs_);
}

int rampPopulation::totalNumberOfTrajs()
{
	int res = 0;
	for (int i=0; i<sub_populations_.size(); ++i)
		res += sub_populations_[i].num_of_trajs_;
	return res;
}

double rampPopulation::getTrajectoryDurationMax()
{
	double res = 0.;
	for (int i=0; i<sub_populations_.size(); ++i)
		res = std::max(res, sub_populations_[i].longest_trajectory_duration_);
	return res;
}

void rampPopulation::showPopulationInfo()
{
	for (int i=0; i<sub_populations_.size(); ++i)
		sub_populations_[i].printSubPopulationInfo();	
}

void rampPopulation::updatePopulationStartStates(const armCfg &start_cfg, const armCfg &start_cfg_vel)
{
	threads.clear();
	for (int i=0; i<sub_populations_.size(); ++i)
	{
		threads.push_back(std::thread(&rampPopulation::updateSubPopulationStartStates, this, i, start_cfg, start_cfg_vel));
	}

	for (auto &t:threads)
	{
		t.join();
	}
}

void rampPopulation::updateSubPopulationStartStates(const int &id, const armCfg &start_cfg, const armCfg &start_cfg_vel)
{
	sub_populations_[id].updateTrajectoriesStartStates(start_cfg, start_cfg_vel);
}

rampTrajectoryPtr rampPopulation::getTrajectoryBySubPopulationNameAndIndex(const std::string &sub_p_name, const int &index)
{
	int sub_ind = sub_populations_map_[sub_p_name];
	return sub_populations_[sub_ind].trajectories_[index];
}

rampTrajectoryPtr rampPopulation::getBestTrajectoryInPopulation()
{
	int sub_ind = sub_populations_map_[best_traj_sub_pop_name_];
	int best_traj_ind = sub_populations_[sub_ind].best_traj_index_;
	return sub_populations_[sub_ind].trajectories_[best_traj_ind];
}

rampTrajectoryPtr rampPopulation::getBestTrajectoryInSubPopulation(const std::string &sub_p_name)
{
	int sub_ind = sub_populations_map_[sub_p_name];
	int best_traj_ind = sub_populations_[sub_ind].best_traj_index_;
	return sub_populations_[sub_ind].trajectories_[best_traj_ind];
}

const std::string& rampPopulation::getBestTrajectorySubPopulationType()
{
	int sub_ind = sub_populations_map_[best_traj_sub_pop_name_];
	return sub_populations_[sub_ind].getType();
}

const std::string& rampPopulation::getBestTrajectorySubPopulationName()
{
	return best_traj_sub_pop_name_;
}

void rampPopulation::getRandomSubpopulation(std::string &name, std::string &type)
{
	int index = rand() % sub_pop_names_.size();
	name = sub_pop_names_.at(index);
	int sub_ind = sub_populations_map_[name];
	type = sub_populations_[sub_ind].getType();
}

rampTrajectoryPtr rampPopulation::getRandomTrajectory(const std::string &sub_p_name)
{
	int sub_p_ind = sub_populations_map_[sub_p_name];
	return sub_populations_[sub_p_ind].getOneRandomTrajectory();
}

bool rampPopulation::checkDuplicateInSubPopulation(const rampTrajectoryPtr &new_traj, const std::string &sub_p_name)
{
	int sub_p_ind = sub_populations_map_[sub_p_name];
	return sub_populations_[sub_p_ind].checkDuplicate(new_traj);
}

float rampPopulation::getBestTrajectoryCost()
{
	int sub_p_ind = sub_populations_map_[best_traj_sub_pop_name_];
	return sub_populations_[sub_p_ind].best_traj_cost_;
}

float rampPopulation::getWorstTrajectoryCostInSubPopulation(const std::string &sub_p_name)
{
	int sub_p_ind = sub_populations_map_[sub_p_name];
	return sub_populations_[sub_p_ind].worst_traj_cost_;
}

void rampPopulation::replaceOneRandomTrajectoryInSubPopulation(rampTrajectoryPtr &new_traj, const std::string &sub_p_name)
{
	int sub_p_ind = sub_populations_map_[sub_p_name];
	sub_populations_[sub_p_ind].replaceOneRandomTrajectory(new_traj);
}

void rampPopulation::replaceOneInfeasibleTrajectoryInSubPopulation(rampTrajectoryPtr &new_traj, const std::string &sub_p_name)
{
	int sub_p_ind = sub_populations_map_[sub_p_name];
	sub_populations_[sub_p_ind].replaceOneInfeasibleTrajectory(new_traj);
}

bool rampPopulation::isWholePopulationInfeasible()
{
	int num_of_sub_p = sub_pop_names_.size();
	int cnt = 0;
	for (int i=0; i<sub_populations_.size(); ++i)
	{
		if (sub_populations_[i].isWholeSubPopulationInfeasible())
			++cnt;
	}
	if (cnt == num_of_sub_p)
		return true;
	else
		return false;
}

bool rampPopulation::isWholeSubPopulationInfeasible(const std::string &sub_p_name)
{
	int sub_p_ind = sub_populations_map_[sub_p_name];
	return sub_populations_[sub_p_ind].isWholeSubPopulationInfeasible();
}

























