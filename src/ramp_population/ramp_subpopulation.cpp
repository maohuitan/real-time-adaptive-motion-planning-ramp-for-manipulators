
/*
 * ramp_subpopulation.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_population/ramp_subpopulation.h"

rampSubPopulation::rampSubPopulation(const std::string pop_name, const int pop_size, 
									 const int knot_cfg_size, const std::string pop_type,
									 const boost::shared_ptr<rampSetting>& r_set_ptr,
								 	 const boost::shared_ptr<rampSensing>& r_sens_ptr,
									 const boost::shared_ptr<rampOperators>& r_oprt_ptr)
:name_(pop_name), num_of_trajs_(pop_size), type_(pop_type)
{
	/* initialize parameters */
	best_traj_index_ = -1;
	worst_traj_index_ = -1;
	best_traj_cost_ = 1e9;
	worst_traj_cost_ = 1e9;
	longest_trajectory_duration_ = 0.;

	/* initialize trajectories */
	armCfg default_cfg;
	r_set_ptr->getDefaultJointConfiguration(default_cfg);
	/*
		step 1
		construct trajectories
	*/
	for (int i=0; i<num_of_trajs_; ++i)
	{
		rampTrajectoryPtr tmp_traj = boost::make_shared<rampTrajectory>(r_set_ptr, r_sens_ptr);
		tmp_traj->setTrajectory(default_cfg, default_cfg, i);
		trajectories_.push_back(tmp_traj);
	}

	/*
		Step 2
		for each trajectory in population
		insert a specified number of knot cfgs
	*/
	for (int i=0; i<num_of_trajs_; ++i)
	{
		r_oprt_ptr->registerTrajectory(trajectories_.at(i));
		for(int j=0; j<knot_cfg_size; ++j) 
		{
			if (type_ == "ntc")
				r_oprt_ptr->applyRandomInsert();
			else if (type_ == "tc")
				r_oprt_ptr->applyTaskConstrainedInsert();
			else
				ROS_ERROR("Unrecognized subpopulation type.");
		}
		r_oprt_ptr->dropTrajectory();
	}
}

void rampSubPopulation::insertRandomCfgs(const boost::shared_ptr<rampOperators>& r_oprt_ptr, const int &num)
{
	for (int i=0; i<num_of_trajs_; ++i)
	{
		r_oprt_ptr->registerTrajectory(trajectories_.at(i));
		for(int j=0; j<num; ++j) 
		{
			if (type_ == "ntc")
				r_oprt_ptr->applyRandomInsert();
			else if (type_ == "tc")
				r_oprt_ptr->applyTaskConstrainedInsert();
			else
				ROS_ERROR("Unrecognized subpopulation type.");
		}
		r_oprt_ptr->dropTrajectory();
	}
}

void rampSubPopulation::updateTrajectoriesStartStates(const armCfg &start_cfg, const armCfg &start_cfg_vel)
{
	// update trajectory starting states and re-generate trajectories
	ROS_INFO("Updating trajectories starting states in subpopulation [%s]...", type_.c_str());
	int nDofs = trajectories_.front()->getNumOfDofs();
	std::vector<double> e_cfg_vel(nDofs, 0.0);
	
	/********Disabled since Gazebo published velocities are not reliable********/
	/*****velocities are always zero. do not know why. vels are too small?*****/
	// std::vector<double> s_cfg_vel = start_cfg_vel;
	/***************************************************************************/
	std::vector<double> s_cfg_vel(nDofs, 0.0);;

	for (int i=0; i<num_of_trajs_; ++i)
	{
		/*
			step 1
			truncate the executed portion (if any) of the trajectory 
			currently being executed based on current robot cfg (start_cfg)
			This is currently done in rampRendering::renderJointTrajectoryGazebo()
			but this is disabled for now since time-based is very inaccurate
		*/

		/*
			step 2
			update start config (and vel) for trajectories
		*/
		trajectories_.at(i)->start_config = start_cfg;

		/*
			step 3
			re-generate trajectories
		*/
		trajectories_.at(i)->generateTrajectoryFromPath(s_cfg_vel, e_cfg_vel, ramp_params.trajectory_generation_option);
	}
	computeLongestTrajectoryDuration();
}

void rampSubPopulation::updateTrajectoriesEndStates(const armCfg &end_cfg)
{
	// update trajectory starting states and re-generate trajectories
	ROS_INFO("Update trajectories end states in population.");
	int nDofs = trajectories_.front()->getNumOfDofs();
	std::vector<double> e_cfg_vel(nDofs, 0.0);
	for (int i=0; i<num_of_trajs_; ++i)
	{
		/*
			step 1
			truncate the executed portion (if any) of the trajectory 
			currently being executed based on current robot cfg (start_cfg)
			This is currently done in rampRendering::renderJointTrajectoryGazebo()
			but this is disabled for now since time-based is very inaccurate
		*/

		/*
			step 2
			update start config (and vel) for trajectories
		*/
		trajectories_.at(i)->end_config = end_cfg;

		/*
			step 3
			re-generate trajectories
		*/
		trajectories_.at(i)->generateTrajectoryFromPath(e_cfg_vel, e_cfg_vel, ramp_params.trajectory_generation_option);
	}
}

bool rampSubPopulation::replaceOneRandomTrajectory(rampTrajectoryPtr &new_traj)
{
	int rand_ind = rand() % num_of_trajs_;
	/* exclude the best trajectory */
	if (rand_ind == best_traj_index_)
	{
		bool isSame = true;
		while (isSame)
		{
			ROS_INFO("Excluding replacement of the best trajectory in population");
			rand_ind = rand() % num_of_trajs_;
			if (rand_ind != best_traj_index_)
				isSame = false;
		}
	}

	/* actual replacement */
	ROS_INFO("Replacing trajectory[ID %d] %d [cost %.1f] in subpopulation [%s]", 
			 trajectories_.at(rand_ind)->getTrajectoryID(),
			 rand_ind, trajectories_.at(rand_ind)->cost,
			 type_.c_str());

	/* replace content of trajectory randomly selected but keep trajID*/
	new_traj->setTrajectoryID(trajectories_.at(rand_ind)->getTrajectoryID());
	trajectories_.at(rand_ind) = new_traj;
	return true;
}

void rampSubPopulation::replaceOneInfeasibleTrajectory(rampTrajectoryPtr &new_traj)
{
	if (!infeasible_traj_indices_.empty())
	{
		int inf_ind = rand() % infeasible_traj_indices_.size();
		int traj_ind = infeasible_traj_indices_.at(inf_ind);

		/* exclude the best trajectory */
		if (traj_ind == best_traj_index_)
		{
			bool isSame = true;
			while (isSame)
			{
				ROS_INFO("Excluding replacement of the best trajectory in population");
				inf_ind = rand() % infeasible_traj_indices_.size();
				traj_ind = infeasible_traj_indices_.at(inf_ind);
				if (traj_ind != best_traj_index_)
					isSame = false;
			}
		}
		ROS_INFO("Replacing infeasible trajectory[ID %d] %d in population", 
				 trajectories_.at(traj_ind)->getTrajectoryID(),
				 traj_ind);

		/* replace content of an infeasible trajectory but keep trajID*/
		new_traj->setTrajectoryID(trajectories_.at(traj_ind)->getTrajectoryID());
		trajectories_.at(traj_ind) = new_traj;
	}
	else
		ROS_WARN("No infeasible trajectory in population, but one wanted to replace one infeasible trajectory.");
}

void rampSubPopulation::generateTrajectories()
{
	ROS_INFO("Generating trajectories in [%s] population.", type_.c_str());
	int nDofs = trajectories_.front()->getNumOfDofs();
	std::vector<double> e_cfg_vel(nDofs, 0.0);
	for (int i=0; i<num_of_trajs_; ++i)
		trajectories_.at(i)->generateTrajectoryFromPath(e_cfg_vel, e_cfg_vel, ramp_params.trajectory_generation_option);
	computeLongestTrajectoryDuration();
}

void rampSubPopulation::evaluateTrajectories()
{
	ROS_INFO("Evaluating subpopulation [%s]", type_.c_str());
	/* 
		Step 1
		evaluate each trajectory
	 */
	infeasible_traj_indices_.clear();
	costs_.clear();
	traj_ind_sorted_by_cost_.clear();
	for (int i=0; i<num_of_trajs_; ++i)
	{
		/* generate cost value for trajectory */
		trajectories_.at(i)->evaluateTrajectory();
		/* note down costs */
		costs_.push_back(trajectories_.at(i)->cost);
		/* set up trajectory indices container */
		traj_ind_sorted_by_cost_.push_back(i);
		/* note down infeasible trajectories indices*/
		if (!trajectories_.at(i)->feasibility)
			infeasible_traj_indices_.push_back(i);
	}

	/* 
		Step 2
		sort trajectories by costs 
	*/
	std::sort(traj_ind_sorted_by_cost_.begin(), traj_ind_sorted_by_cost_.end(),
			 [&](const int& a, const int& b)
			 {
				return (costs_[a] < costs_[b]);
			 });
	
	ROS_INFO("Trajectories in subpopulation [Type %s] sorted by costs", type_.c_str());
	for (int i=0; i<traj_ind_sorted_by_cost_.size(); ++i)
	{
		std::cout << "Trajectory " << traj_ind_sorted_by_cost_.at(i) <<
		" cost " << costs_[traj_ind_sorted_by_cost_.at(i)] << std::endl;
	}

	ROS_INFO("%zd infeasible trajectories:", infeasible_traj_indices_.size());
	for (int i=0; i<infeasible_traj_indices_.size(); ++i)
	{
		std::cout << "Trajectory " << infeasible_traj_indices_.at(i) << std::endl;
	}

	/* 
		Step 3
		set best and worst trajectory index 
	*/
	best_traj_index_ = traj_ind_sorted_by_cost_.front();
	worst_traj_index_ = traj_ind_sorted_by_cost_.back();
	best_traj_cost_ = costs_.at(best_traj_index_);
	worst_traj_cost_ = costs_.at(worst_traj_index_);
}

void rampSubPopulation::updateTrajectoriesEvaluation()
{
	// ROS_INFO("Updating evaluation of subpopulation [%s]...", type_.c_str());
	/* 
		Step 1
		evaluate each trajectory
	 */
	infeasible_traj_indices_.clear();
	costs_.clear();
	traj_ind_sorted_by_cost_.clear();
	for (int i=0; i<num_of_trajs_; ++i)
	{
		/* generate cost value for trajectory */
		trajectories_.at(i)->updateTrajectoryEvaluation();
		/* note down costs */
		costs_.push_back(trajectories_.at(i)->cost);
		/* set up trajectory indices container */
		traj_ind_sorted_by_cost_.push_back(i);
		/* note down infeasible trajectories indices*/
		if (!trajectories_.at(i)->feasibility)
			infeasible_traj_indices_.push_back(i);
	}

	/* 
		Step 2
		sort trajectories by costs 
	*/
	std::sort(traj_ind_sorted_by_cost_.begin(), traj_ind_sorted_by_cost_.end(),
			 [&](const int& a, const int& b)
			 {
				return (costs_[a] < costs_[b]);
			 });
	
	if(ramp_params.verbose)
	{
		ROS_INFO("Trajectories in subpopulation [Name %s][Type %s] sorted by costs",name_.c_str(), type_.c_str());
		for (int i=0; i<traj_ind_sorted_by_cost_.size(); ++i)
		{
			std::cout << "Trajectory " << traj_ind_sorted_by_cost_.at(i) <<
			" cost " << costs_[traj_ind_sorted_by_cost_.at(i)] << std::endl;
		}

		ROS_INFO("%zd infeasible trajectories:", infeasible_traj_indices_.size());
		for (int i=0; i<infeasible_traj_indices_.size(); ++i)
		{
			std::cout << "Trajectory " << infeasible_traj_indices_.at(i) << std::endl;
		}
	}

	/* 
		Step 3
		set best and worst trajectory index 
	*/
	best_traj_index_ = traj_ind_sorted_by_cost_.front();
	worst_traj_index_ = traj_ind_sorted_by_cost_.back();
	best_traj_cost_ = costs_.at(best_traj_index_);
	worst_traj_cost_ = costs_.at(worst_traj_index_);
}

void rampSubPopulation::sortTrajectoriesByCosts()
{
	infeasible_traj_indices_.clear();
	costs_.clear();
	traj_ind_sorted_by_cost_.clear();
	for (int i=0; i<num_of_trajs_; ++i)
	{
		costs_.push_back(trajectories_.at(i)->cost);
		traj_ind_sorted_by_cost_.push_back(i);
		if (!trajectories_.at(i)->feasibility)
			infeasible_traj_indices_.push_back(i);
	}

	/* 
		Step 1
		sort trajectories by costs 
	*/
	std::sort(traj_ind_sorted_by_cost_.begin(), traj_ind_sorted_by_cost_.end(),
			 [&](const int& a, const int& b)
			 {
				return (costs_[a] < costs_[b]);
			 });
	
	ROS_INFO("Trajectories in subpopulation [Name %s][Type %s] sorted by costs",name_.c_str(), type_.c_str());
	for (int i=0; i<traj_ind_sorted_by_cost_.size(); ++i)
	{
		std::cout << "Trajectory " << traj_ind_sorted_by_cost_.at(i) <<
		" cost " << costs_[traj_ind_sorted_by_cost_.at(i)] << std::endl;
	}

	ROS_INFO("%zd infeasible trajectories:", infeasible_traj_indices_.size());
	for (int i=0; i<infeasible_traj_indices_.size(); ++i)
	{
		std::cout << "Trajectory " << infeasible_traj_indices_.at(i) << std::endl;
	}

	/* 
		Step 3
		set best and worst trajectory index 
	*/
	best_traj_index_ = traj_ind_sorted_by_cost_.front();
	worst_traj_index_ = traj_ind_sorted_by_cost_.back();
	best_traj_cost_ = costs_.at(best_traj_index_);
	worst_traj_cost_ = costs_.at(worst_traj_index_);
}

void rampSubPopulation::computeLongestTrajectoryDuration()
{
	double longest_dur = 0.0;
	for (int i=0; i<num_of_trajs_; ++i)
	{
		if( trajectories_.at(i)->getTrajectoryDuration() > longest_dur)
		{
			longest_dur = trajectories_.at(i)->getTrajectoryDuration();
		}
	}
	longest_trajectory_duration_ = longest_dur;
}

void rampSubPopulation::printSubPopulationInfo(bool verbose)
{
	std::cout << std::endl;
	std::cout << "Subpopulation type [" << type_ << "]" << std::endl;
	std::cout << std::endl;
	for (int i=0; i<num_of_trajs_; ++i)
	{
		trajectories_.at(i)->printTrajectoryInfo(verbose);
		std::cout << std::endl;
	}
}

rampTrajectoryPtr rampSubPopulation::getOneRandomTrajectory()
{
	int ind = rand() % num_of_trajs_;
	return trajectories_.at(ind);
}

bool rampSubPopulation::checkDuplicate(const rampTrajectoryPtr &new_traj)
{
	double cost_new = new_traj->cost;
	double cost_cur = 0.0;
	int num_knot_new = new_traj->knot_cfgs.size();
	int num_knot_cur = 0;
	bool is_dup = false;
	for (int i=0; i<num_of_trajs_; ++i)
	{
		cost_cur = trajectories_.at(i)->cost;
		num_knot_cur = trajectories_.at(i)->knot_cfgs.size();
		if (std::abs(cost_new - cost_cur) < 1.0 && 
			num_knot_new == num_knot_cur)
		{
			ROS_WARN("Duplicate to trajectory [ID %d] in [%s] subpopulation!", trajectories_.at(i)->getTrajectoryID(), type_.c_str());
			is_dup = true;
			return is_dup;
		}
	}
	return is_dup;
}

bool rampSubPopulation::isWholeSubPopulationInfeasible()
{
	if (num_of_trajs_ == infeasible_traj_indices_.size())
		return true;
	else
		return false;
}

void rampSubPopulation::setEvaluationFunctionWeights(const costFunctionWeights &new_weights)
{
	for (int i=0; i<num_of_trajs_; ++i)
		trajectories_.at(i)->setCostFunWeights(new_weights);
}

const std::string& rampSubPopulation::getType()
{
	return type_;
}

rampSubPopulation::~rampSubPopulation()
{

}












