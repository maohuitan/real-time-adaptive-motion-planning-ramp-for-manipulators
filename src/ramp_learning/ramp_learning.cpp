/*
 * ramp_learning.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_learning/ramp_learning.h"

rampLearning::rampLearning(const int n_dofs)
{
	nDofs_ = n_dofs;
}

void rampLearning::readInTrajectories(const std::string &file_name)
{
	// const std::string file_path("/home/huitan/Dropbox/RAMP_DATA//sequentail_knowledge_transfer/");
	const std::string file_path("/home/huitan/Dropbox/RAMP_DATA/saved_trajectories/");
	ROS_INFO("Reading in saved trajectory at %s", (file_path+file_name).c_str()); 

	std::ifstream saved_traj (file_path+file_name);
	double line;
	armCfg cfg(nDofs_, 0.);

	if (saved_traj.is_open())
	{
		int cnt = 0;
		while( saved_traj >> line )
		{
			cfg.at(cnt % nDofs_) = line;
			if(cnt % nDofs_ == (nDofs_-1))
				saved_knot_cfgs_.push_back(cfg);
			++cnt;
		}
		saved_traj.close();  

		/* print trajectory(knot cfgs) info */
		rampUtil::printArmCfgs(saved_knot_cfgs_);
	}
	else 
		ROS_ERROR("Unable to open saved trajectory at %s", (file_path+file_name).c_str()); 
}

void rampLearning::getSavedKnotCfgs(std::vector<armCfg>& cfgs)
{
	cfgs = saved_knot_cfgs_;
}

