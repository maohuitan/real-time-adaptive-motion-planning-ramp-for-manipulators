
/*
 * ramp_rendering.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_rendering/ramp_rendering.h"

rampRendering::rampRendering(const ros::NodeHandle &node_handle)
: fol_jnt_traj_action_client(ramp_params.follow_joint_trajectory_action_gazebo, true)
{
 	nh = node_handle;
 	// topic for visualizing a single joint configuration in rviz
 	jnt_cfg_topic = "/move_group/fake_controller_joint_states";
 	joint_cfg_pub = nh.advertise<sensor_msgs::JointState>(jnt_cfg_topic, 100);  
 	display_pub = nh.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 100, true);
 	pose_array_arrow_pub = nh.advertise<geometry_msgs::PoseArray>("ramp/pose_array",1000);
	pose_array_frame_pub = nh.advertise<visualization_msgs::MarkerArray>("ramp/marker_array",1000);
	one_pose_frame_pub = nh.advertise<geometry_msgs::PoseStamped>("ramp/pose_stamped",1000);

 	// wait for action server to come up
    while(!fol_jnt_traj_action_client.waitForServer(ros::Duration(5.0)))
    {
      ROS_INFO("Waiting for the joint_trajectory_action server from ROS-I in Gazebo");
    }
 	sleep(1.0); //sleep to set up the connection

 	cur_exe_traj_id = -1;
	cur_exe_traj_sub_pop_name = "";
}

// render single joint configuration in rviz
void rampRendering::renderJointConfiguration(const armCfg &cfg, std::vector<std::string> jnt_names)
{
	sensor_msgs::JointState joint_state;
	joint_state.header.stamp = ros::Time::now();
	joint_state.name = jnt_names;
	joint_state.position = cfg;
	joint_cfg_pub.publish(joint_state);
	ros::spinOnce();
	std::cout << "Rendering joint configuration [ ";
	for (int i=0; i<cfg.size(); ++i)
	{
		std::cout << cfg.at(i) << " ";
	}
	std::cout << "]" << std::endl;
	ROS_INFO("Joint Configuration Rendered to Display.");
}

// In rviz, render each configuration in a joint trajectory one by one with duration 
// between configurations stopped in sleep(). One may want to interpolate
// to have a more smooth motion (show one config after another)
void rampRendering::renderJointTrajectory(const rampTrajectory &ramp_traj, std::vector<std::string> jnt_names)
{
	// has to give rviz some time for display otherwise nothing is rendered
	// this may be a problem for time-sensitive trajectories
	float time_delay = 0.; 
	renderJointConfiguration(ramp_traj.start_config, jnt_names);
	sleep(ramp_traj.cfgs_durations.front()+time_delay);
	
	for (int i=0; i<ramp_traj.knot_cfgs.size(); ++i)
	{
		renderJointConfiguration(ramp_traj.knot_cfgs.at(i), jnt_names);
		sleep(ramp_traj.cfgs_durations.at(i+1)+time_delay);
	}

	renderJointConfiguration(ramp_traj.end_config, jnt_names);
	sleep(ramp_traj.cfgs_durations.back()+time_delay);
}

// render joint trajectory by sending the whole trajectory
// to /move_group/display_planned_path using displayTrajectory
// it seems this does not respect time parameterization
void rampRendering::renderJointTrajectoryMsg(const rampTrajectory &ramp_traj,
							  				 std::vector<std::string> jnt_names)
{
	// prepare trajectory
	trajectory_msgs::JointTrajectoryPoint jnt_pnt;
	moveit_msgs::RobotTrajectory traj_msg;
	float time_from_beginning = 0.0;

	traj_msg.joint_trajectory.joint_names = jnt_names;

	jnt_pnt.positions = ramp_traj.start_config;
	time_from_beginning = ramp_traj.cfgs_durations.front();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	for (std::size_t i=0; i<ramp_traj.knot_cfgs.size(); ++i)
	{
		jnt_pnt.positions = ramp_traj.knot_cfgs.at(i);
		time_from_beginning += ramp_traj.cfgs_durations.at(i+1);
		jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
		traj_msg.joint_trajectory.points.push_back(jnt_pnt);
	}

	jnt_pnt.positions = ramp_traj.end_config;
	time_from_beginning += ramp_traj.cfgs_durations.back();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	// prepare start state
	moveit_msgs::RobotState traj_start;
	traj_start.joint_state.name = jnt_names;
	traj_start.joint_state.position = ramp_traj.start_config;

	// make msg
	moveit_msgs::DisplayTrajectory display_trajectory;
	display_trajectory.trajectory.push_back(traj_msg);
	display_trajectory.trajectory_start = traj_start;

	display_pub.publish(display_trajectory);
}

// using follow_joint_trajectory. The trajectory is executed
// in gazebo with controllers and rviz also displays the robot state
void rampRendering::renderJointTrajectoryGazebo(rampTrajectory &ramp_traj, const std::string &traj_sub_pop_name)
{
	if (cur_exe_traj_id == ramp_traj.getTrajectoryID() && cur_exe_traj_sub_pop_name == traj_sub_pop_name)
	{
		ROS_INFO("Conitnue executing the same trajectory[ID %d] in subpopulation [Name %s] in Gazebo as last control cycle.", ramp_traj.getTrajectoryID(), cur_exe_traj_sub_pop_name.c_str());
	}
	else
	{
		cur_exe_traj_id = ramp_traj.getTrajectoryID();
		cur_exe_traj_sub_pop_name = traj_sub_pop_name;
		ROS_INFO("[Trajectory Switch]Sending trajectory[ID %d] for execution in Gazebo", cur_exe_traj_id);

		/*convert to trajectory msg*/
		trajectory_msgs::JointTrajectory traj_msg;
		ramp_traj.convertToTrajectoryMsgs(traj_msg);
		// ramp_traj.printTrajectoryInfo();
		
		/*make control msgs*/
		control_msgs::FollowJointTrajectoryGoal traj_goal;
		traj_goal.trajectory = traj_msg;
		fol_jnt_traj_action_client.sendGoal(traj_goal);
	}
}

void rampRendering::renderPoseArrayUsingArrow(const geometry_msgs::PoseArray &pose_array)
{
	pose_array_arrow_pub.publish(pose_array);
}

void rampRendering::renderPoseArrayUsingFrame(const geometry_msgs::PoseArray &pose_array)
{
	geometry_msgs::PoseStamped pst;
	pst.header.frame_id = "world";
	for (int i=0; i<pose_array.poses.size(); ++i)
	{
		pst.header.stamp = ros::Time::now();
		pst.pose = pose_array.poses.at(i);
		one_pose_frame_pub.publish(pst);
		sleep(0.1);
	}
}

void rampRendering::renderMarkerArray(const geometry_msgs::PoseArray &pose_array)
{
	visualization_msgs::Marker marker;
	visualization_msgs::MarkerArray marker_array;
	marker.header.frame_id = "world";
	marker.type = visualization_msgs::Marker::CYLINDER;
	marker.action = visualization_msgs::Marker::ADD;

	int array_size = pose_array.poses.size();
	for (int i=0; i<array_size; ++i)
	{
		marker.header.stamp = ros::Time();
		// marker.ns = "my_namespace";
		marker.id = i;
		marker.pose = pose_array.poses.at(i);
		marker.scale.x = 0.1;
		marker.scale.y = 0.1;
		marker.scale.z = 0.2;
		marker.color.a = 1.0 - 0.7 * (float) i / array_size; 
		marker.color.r = 0.0;
		marker.color.g = 0.0;
		marker.color.b = 1.0;

		marker_array.markers.push_back(marker);
	}
	pose_array_frame_pub.publish(marker_array);
}

void rampRendering::renderMarkerArray(const geometry_msgs::PoseArray &pose_array, const std::vector<Eigen::Vector3d> &dimension_array)
{
	visualization_msgs::Marker marker;
	visualization_msgs::MarkerArray marker_array;
	marker.header.frame_id = "world";
	marker.type = visualization_msgs::Marker::CUBE;
	marker.action = visualization_msgs::Marker::ADD;

	int array_size = pose_array.poses.size();
	for (int i=0; i<array_size; ++i)
	{
		marker.header.stamp = ros::Time();
		// marker.ns = "my_namespace";
		marker.id = i;
		marker.pose = pose_array.poses.at(i);
		marker.scale.x = dimension_array.at(i)[0];
		marker.scale.y = dimension_array.at(i)[1];
		marker.scale.z = dimension_array.at(i)[2];
		marker.color.a = 1.0 - 0.7 * (float) i / array_size; 
		marker.color.r = 0.0;
		marker.color.g = 0.0;
		marker.color.b = 1.0;

		marker_array.markers.push_back(marker);
	}
	pose_array_frame_pub.publish(marker_array);
}

rampRendering::~rampRendering()
{

}
		



