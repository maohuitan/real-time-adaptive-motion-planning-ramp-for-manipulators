ramp_arm_uav_gazebo.launch: launch task of closing a drawer in gazebo only. should use ramp_config_arm_uav.yaml.

ramp_arm_uav_hardware.launch: ramp_arm_uav_gazebo.launch + launch of ABB robot hardware interface. should use ramp_config_arm_uav.yaml.

ramp_two_arm_gazebo.launch: launch task of water cup in gazebo only. should use ramp_config_two_arm.yaml

ramp_two_arm_hardware.launch: ramp_two_arm_gazebo.launch + launch of ABB robot hardware interface. should use ramp_config_real_arm.yaml

ramp_parameters.launch: load task-specific parameters to the parmeter server

unused now: 
ramp_rviz.launch
ramp_abb_irb120_gazebo.launch
