These yaml files set the ramp parameters and describe the task constraints.

ramp_config_arm_uav.yaml: for the task of closing a drawer in Gazebo. Set is_hardware_enabled to true if you want to have the hardware execute the same trajectories as being executed in Gazebo.

ramp_config_real_arm.yaml: for the task of water cup. It is set to work with hardware only, with sensing and control both on hardware. 

ramp_config_two_arm.yaml: for the task of water cup. It is set to work in Gazebo only.
