/*
 * ramp_rendering.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_RENDERING_H
#define RAMP_RENDERING_H

#include "ramp_core/ramp_common.h"
#include "ramp_trajectory/ramp_trajectory.h"
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/MarkerArray.h>

// action for FollowJointTrajectoryAction
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>

extern rampParameters ramp_params;

class rampRendering
{
	public:
		rampRendering(const ros::NodeHandle &node_handle);
		~rampRendering();
		
		/** 
		 * \brief render a single joint cfg in rviz by publishing a message in topic jnt_cfg_topic
		 *        Does not repect time stamps
		 * \param cfg to be rendered and joint names
		 * \return void
		 */
		void renderJointConfiguration(const armCfg &cfg, std::vector<std::string> jnt_names);

		/** 
		 * \brief render a series of joint cfgs in rviz by rendering one by one
		 *        Does not repect time stamps
		 * \param ramp path to be rendered and joint names
		 * \return void
		 */
		void renderJointTrajectory(const rampTrajectory &ramp_traj, std::vector<std::string> jnt_names);

		/** 
		 * \brief render a series of joint cfgs in rviz by publish the whole trajectory in a topic
		 *        Does not repect time stamps
		 * \param ramp path to be rendered and joint names
		 * \return void
		 */
		void renderJointTrajectoryMsg(const rampTrajectory &ramp_traj, std::vector<std::string> jnt_names);
		
		/** 
		 * \brief send a trajectory for execution in gazebo using specified controllers by a ros action
		 *        rviz re-displays the state of the robot. 
		 *        The execution respects the time stamps and the spcified positons and velocites
		 * \param ramp trajectory to be executed and joint names
		 * \return void
		 */
		void renderJointTrajectoryGazebo(rampTrajectory &ramp_traj, const std::string &traj_sub_pop_name);

		/** 
		 * \brief show sequence of poses using arrows
		 * \param pose array
		 * \return void
		 */
		void renderPoseArrayUsingArrow(const geometry_msgs::PoseArray &pose_array);
		
		/** 
		 * \brief show sequence of poses using frames (but currently only show final frame)
		 * \return void
		 */
		void renderPoseArrayUsingFrame(const geometry_msgs::PoseArray &pose_array);
		
		/** 
		 * \brief show sequence of poses using cylinders attached to the pose
		 * \return void
		 */
		void renderMarkerArray(const geometry_msgs::PoseArray &pose_array);
		void renderMarkerArray(const geometry_msgs::PoseArray &pose_array, const std::vector<Eigen::Vector3d> &dimension_array);
	
	private:
		ros::NodeHandle nh;

		/** 
		 * \brief publisher for dispalying single cfg and the whole path
		 */
		ros::Publisher joint_cfg_pub, display_pub, pose_array_arrow_pub, pose_array_frame_pub, one_pose_frame_pub;

		/** 
		 * \brief topic name for publishing a single joint cfg
		 */
		std::string jnt_cfg_topic;

		/** 
		 * \brief FollowJointTrajectoryAction provided by ros-industrial.
		 */
		actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> fol_jnt_traj_action_client;

		/** 
		 * \brief ID of the trajectory currently being executed
		 */
		int cur_exe_traj_id;
		std::string cur_exe_traj_sub_pop_name;
};


#endif
