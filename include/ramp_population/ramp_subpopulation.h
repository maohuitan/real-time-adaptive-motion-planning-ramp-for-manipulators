/*
 * ramp_subpopulation.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_SUBPOPULATION_H
#define RAMP_SUBPOPULATION_H

#include "ramp_trajectory/ramp_trajectory.h" 
#include "ramp_core/ramp_common.h"
#include "ramp_operators/ramp_operators.h"
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include "ramp_population/ramp_population.h"

extern statistics stats;

class rampSubPopulation
{
    public:
		friend class rampPopulation;
		rampSubPopulation(const std::string pop_name, const int pop_size, 
						  const int knot_cfg_size, const std::string pop_type,
						  const boost::shared_ptr<rampSetting>& r_set_ptr,
						  const boost::shared_ptr<rampSensing>& r_sens_ptr,
						  const boost::shared_ptr<rampOperators>& r_oprt_ptr);
		void updateTrajectoriesStartStates(const armCfg &start_cfg, const armCfg &start_cfg_vel);
		void updateTrajectoriesEndStates(const armCfg &end_cfg);
		bool replaceOneRandomTrajectory(rampTrajectoryPtr &new_traj);
		void replaceOneInfeasibleTrajectory(rampTrajectoryPtr &new_traj);
		void generateTrajectories();
		void setEvaluationFunctionWeights(const costFunctionWeights &new_weights);
		void insertRandomCfgs(const boost::shared_ptr<rampOperators>& r_oprt_ptr, const int &num);
		rampTrajectoryPtr getOneRandomTrajectory();
		
		/** 
		 * \brief evaluating trajectories by explicitly computing all cost terms
		 */
		void evaluateTrajectories();
		/** 
		 * \brief update trajectories evaluation by only recomputing feasibility cost term
		 */
		void updateTrajectoriesEvaluation();
		
		bool checkDuplicate(const rampTrajectoryPtr &new_traj);
		void sortTrajectoriesByCosts();
		bool isWholeSubPopulationInfeasible();
		void printSubPopulationInfo(bool verbose=false);

		const std::string& getType();

		~rampSubPopulation();

	private:
		const int num_of_trajs_;
		const std::string name_;
		const std::string type_;
		/** 
		 * \brief trajectories container
		 */
		std::vector<rampTrajectoryPtr> trajectories_;
		/** 
		 * \brief infeasible trajectories index container
		 */
		std::vector<int> infeasible_traj_indices_;
		/** 
		 * \brief trajectories costs container
		 */
		std::vector<double> costs_;
		/** 
		 * \brief trajectories indices sorted by cost from low to high
		 */
		std::vector<int> traj_ind_sorted_by_cost_; 

		int best_traj_index_, worst_traj_index_;
		double best_traj_cost_, worst_traj_cost_;
		double longest_trajectory_duration_;

		void computeLongestTrajectoryDuration();
};

#endif
