/*
 * ramp_population.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_POPULATION_H
#define RAMP_POPULATION_H
#include <unordered_map>
#include <algorithm>
#include <thread>

#include "ramp_trajectory/ramp_trajectory.h" 
#include "ramp_core/ramp_common.h"
#include "ramp_operators/ramp_operators.h"
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include "ramp_population/ramp_subpopulation.h"

extern statistics stats;
class rampSubPopulation;

class rampPopulation
{
    public:
		rampPopulation( const boost::shared_ptr<rampSetting>& r_set_ptr, 
						const boost::shared_ptr<rampSensing>& r_sens_ptr,
						const boost::shared_ptr<rampOperators>& r_oprt_ptr);
		void setStartRobotCfgInWholePopulation(const armCfg &start_cfg);
		/** 
		 * \brief Set task-cosntrained goal if subpopulation is tc
		 		  else randomly set a goal
		 */
		void setGoalInSubPopulation(const boost::shared_ptr<rampSampling> &r_samp_ptr, const geometry_msgs::Pose &goal, const std::string &sub_p_name);
		void setGoalsInWholePopulation(const boost::shared_ptr<rampSampling> &r_samp_ptr, const geometry_msgs::Pose &goal);
		void updatePopulationStartStates(const armCfg &start_cfg, const armCfg &start_cfg_vel);
		void updateSubPopulationStartStates(const int &id, const armCfg &start_cfg, const armCfg &start_cfg_vel);
		void setEvaluationFuncWeightsInSubPopulation(const costFunctionWeights &new_weights, const std::string &sub_p_name);
		void subPopulationTrajectoryEvaluation(const int id);
		void wholePopulationTrajectoryEvaluationMultipleThreads();

		void generateTrajectories();
		void wholePopulationCollisionCheck();
		void wholePopulationCollisionCheckMultipleThreads();
		void subPopulationCollisionCheckThreadSafe(const int id, const float cur_time);
		void resetAllTrajectoryFeasibility();
		/** 
		 * \brief evaluating the whole population by explicitly computing 
		 * 	      all the cost terms associated with the trajectories.
		 * 		  this is called in the ramp_initialization()
		 */
		void InitialEvaluateWholePopulation();
		/** 
		 * \brief update the evaluation of the whole population by 
		 * 		  recomputing the infeasibility cost terms (without recomputing
		 * 		  other terms because they are not related to new sensing
		 * 		  and we can save computation time)
		 * 		  this is called in the online doControl()
		 */
		void updateWholePopulationEvaluation();
		bool checkDuplicateInSubPopulation(const rampTrajectoryPtr &new_traj, const std::string &sub_p_name);
		void sortTrajectoriesInEachSubPopulation();
		bool isWholePopulationInfeasible();
		bool isWholeSubPopulationInfeasible(const std::string &sub_p_name);
		
		rampTrajectoryPtr getTrajectoryBySubPopulationNameAndIndex(const std::string &sub_p_name, const int &id);
		rampTrajectoryPtr getBestTrajectoryInPopulation();
		rampTrajectoryPtr getBestTrajectoryInSubPopulation(const std::string &sub_p_name);
		const std::string& getBestTrajectorySubPopulationType();
		const std::string& getBestTrajectorySubPopulationName();
		void getRandomSubpopulation(std::string &name, std::string &type);
		rampTrajectoryPtr getRandomTrajectory(const std::string &sub_p_name);
		float getBestTrajectoryCost();
		float getWorstTrajectoryCostInSubPopulation(const std::string &sub_p_name);

		void replaceOneRandomTrajectoryInSubPopulation(rampTrajectoryPtr &new_traj, const std::string &sub_p_name);
		void replaceOneInfeasibleTrajectoryInSubPopulation(rampTrajectoryPtr &new_traj, const std::string &sub_p_name);

		/* utility */
		int totalNumberOfTrajs();
		double getTrajectoryDurationMax();
		void showPopulationInfo();
		void insertRandomCfgsToSubpopulation(const boost::shared_ptr<rampOperators>& r_oprt_ptr, const std::string &sub_p_name, const int &num_of_cfgs);

	private:
		boost::shared_ptr<rampSensing> ramp_sensing_p;
		std::vector<std::string> sub_pop_names_;
		std::vector<rampSubPopulation> sub_populations_;
		std::unordered_map<std::string, int> sub_populations_map_;
		std::string best_traj_sub_pop_name_, worst_traj_sub_pop_name_;
		int best_traj_id, worst_traj_id;

		std::vector<std::thread> threads;
};

#endif
