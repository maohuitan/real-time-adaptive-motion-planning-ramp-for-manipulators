/*
 * ramp_sampling.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_SAMPLING_H
#define RAMP_SAMPLING_H

#include <ros/ros.h>

#include "ramp_core/ramp_common.h"
#include "ramp_setting/ramp_setting.h"


struct eefWorkSpace
{
	double x_min, x_max, y_min, y_max, z_min, z_max;
};

/** 
 * \brief Constraint orientation of eef with a certain tolerance
		  Pose is represented in XYZRPY
		  Axis is the orientation constrained on
		  Tolerance is the constrained amount
 */
class constrainedPose
{
	public:
		constrainedPose(const std::vector<double> &base_pose_vec, const std::string &axis, const double &tolerance, const eefWorkSpace &eef_space);
		/** 
		 * \brief Generate one pose satisfying constraints
		 */
		void getOneConstrainedPose(geometry_msgs::Pose &cstr_pose);
	private:
		std::vector<double> m_base_pose_vec; // base pose in XYZRPY
		std::string m_axis; // constrained axis
		double m_tolerance; // constrained amount
		eefWorkSpace eef_cstr_space;

		void getRandomPositionInWorkSpace(Eigen::Vector3d &rand_pos);
};

class rampSampling
{
	public:
		rampSampling(const boost::shared_ptr<rampSetting> &rs_ptr);
		void sampleRandomJointConfiguration(armCfg &jnt_cfg);
		bool sampleJntCfgGivenEefPose(armCfg &jnt_cfg,const geometry_msgs::Pose &eef_pose);

		void setConstrainedBasePose(const std::vector<double> &base_pose_vec, const std::string &axis, const double &tolerance, const eefWorkSpace &eef_s);
		bool sampleJntCfgUnderTaskCstrs(armCfg &jnt_cfg);

	private:
		boost::shared_ptr<rampSetting> ramp_setting_ptr;
		boost::shared_ptr<constrainedPose> cstr_base_pose;
};

#endif
