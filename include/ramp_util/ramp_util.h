/*
 * ramp_util.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_UTIL_H
#define RAMP_UTIL_H

#include <ros/ros.h>
#include "ramp_core/ramp_common.h"
#include <Eigen/Dense>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <fstream>

namespace rampUtil
{
	void convertXYZRPYtoAffine3d(const std::vector<double> &pose_vec, Eigen::Affine3d &pose_tf);
	void convertAffine3dtoQuaternion(const Eigen::Affine3d &pose_tf, geometry_msgs::Pose &pose_quat);
	void convertAffine3dtoXYZRPY(const Eigen::Affine3d &pose_tf, std::vector<double> &pose_vec);
	void convertXYZRPYtoQuaternion(const std::vector<double> &pose_vec, geometry_msgs::Pose &pose_quat);
	void convertQuaterniontoXYZRPY(const geometry_msgs::Pose &pose_quat, std::vector<double> &pose_vec);
	void convertQuaterniontoAffine3d(const geometry_msgs::Pose &pose_quat, Eigen::Affine3d &pose_tf);

	void printGeoMsgPose(const geometry_msgs::Pose &p);
	void printArmCfg(const armCfg &jv);
	void printArmCfgs(const std::vector<armCfg> &jvs);
	void printGeoMsgTwist(const geometry_msgs::Twist &t);

	bool areSamePoses(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2);
	bool areSamePositions(const geometry_msgs::Pose &pose1, const geometry_msgs::Pose &pose2);
	/* 
	* \brief generate a random float between a and b
	*/
	float randomFloat(float a, float b);

	void saveCfgsToFile(std::vector<armCfg> &cfgs);

}

#endif
