/*
 * ramp_trajectory.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_TRAJECTORY_H
#define RAMP_TRAJECTORY_H
 
#include <ros/ros.h>

#include "ramp_setting/ramp_setting.h"
#include "ramp_sensing/ramp_sensing.h"
#include "ramp_core/ramp_common.h"
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>
//#include <moveit/trajectory_processing/iterative_spline_parameterization.h>

#include <fstream>
#include <random>
#include <boost/shared_ptr.hpp>

// solve Ax=b
#include <Eigen/Cholesky>
#include "ramp_util/ramp_util.h"
#include <chrono>

// cubic polynomial for joint vector
struct cubicPolynomial
{
	Eigen::VectorXd a0, a1, a2, a3;
};

struct costFunctionWeights
{
	double c1, c2, c3, c4, c5;
};

struct costFunctionNormalizationFactor
{
	double a1, a2, a3, a4, a5;
};

extern rampParameters ramp_params;

class rampTrajectory
{
	public:
		rampTrajectory(const boost::shared_ptr<rampSetting> &r_set_p,const boost::shared_ptr<rampSensing> &r_sens_p);

		/** 
		 * \brief Set basic infomation of a trajectory
		 * \param start and end configurations and trajectory ID
		 * \return void
		 */
		void setTrajectory(const armCfg &start_cfg, const armCfg &end_cfg, const int &trajectoryID);

		void setTrajectoryID(const int &id);

		void setCostFunWeights(const costFunctionWeights &new_weights);

		void getJntNames(std::vector<std::string> &jnt_names);

		/** 
		 * \return number of Dofs
		 */
		int getNumOfDofs() const;

		/** 
		 * \return trajectory ID
		 */
		int getTrajectoryID() const;

		/** 
		 * \brief trajectory generation from path with cubic splines
		 * \param start and end configurations velocity
		 * \return void
		 */
		void generateTrajectoryFromPath(std::vector<double> &start_cfg_vel, std::vector<double> &end_cfg_vel, const std::string &option);

		/** 
		 * \brief Get joint cfg at time t
		 * \param time t, armCfg to be filled in, option = "analytically" or "numerically"
		 * \return void
		 */
		void getJntCfgAtTimeT(double t, armCfg &cfg_t);
		void getJntCfgAtTimeT(double t, armCfg &cfg_t, std::string option);

		int locateIntervalByTime(double t);
		
		/** 
		 * \brief trajectory total duration
		 */
		double getTrajectoryDuration() const;

		/** 
		 * \brief trajectory total kinetic energy
		 */
		double getTrajectoryKineticEnergy() const;

		/** 
		 * \brief trajectory average manipulability measure
		 */
		double getTrajectoryAverageManipulabilityMeasure();

		/** 
		 * \brief get infeasibility cost if this trajectory is infeasible
		 		  should be called after collisionChecking() of this trajectory
		 */
		double getInfeasibilityCost();

		double getTrajectoryTaskCstrCost();

		double getTrajectorySmoothnessCost();

		/** 
		 * \brief get two costs in one loop to save computation time
		 */
		void getTrajectoryTaskCstrAndSmoothnessCost(double &task_cstr_cost, double &smoothness_cost);

		/** 
		 * \brief trajectory collision checking with interpolation
		 * 		  collision checking is done against the current planning scene
		 * 		  maintained by ramp_sensing_ptr
		 *        one should update the planning scene to take in new sensing information
		 */
		bool wholeTrajectoryCollisionChecking();
		bool collisionChecking(const double &collision_check_time);
		bool collisionChecking(const double &collision_check_time, std::string &colliding_object_id, armCfg &colliding_cfg);
		bool collisionCheckingThreadSafe(const double &collision_check_time);
		bool collisionCheckingThreadSafe(const double &collision_check_time, std::string &colliding_object_id, armCfg &colliding_cfg);

		/** 
		 * \brief Converts information inside this class to a trajectory_msgs
		 *        to facilitate the use of MoveIt existing functions.
		 * \param trajectory_msgs to be filled in (It also fills in information
		 *        for m_traj_msg)
		 * \return void
		 */
		void convertToTrajectoryMsgs(bool is_first_state_ignored = false);
		void convertToTrajectoryMsgs(trajectory_msgs::JointTrajectory &traj_msg, bool is_first_state_ignored = false);
		
		/** 
		 * \brief Evaluate a trajectory by explicitly computing all cost terms
		 * \param 
		 * \return void
		 */
		void evaluateTrajectory();
		/** 
		 * \brief Update trajectory evaluation by only recomputing feasibility cost term
		 * \param 
		 * \return void
		 */
		void updateTrajectoryEvaluation();

		/** 
		 * \brief Print trajectory information contained in m_traj_msg.
		 *        One needs to make sure the information is up-to-date.
		 *        Or this should only be called after convertToTrajectoryMsgs()
		 *        Positions, velocities and time stamps.
		 * \return void
		 */
		void printTrajectoryInfo(bool verbose=false);

		void saveTrajectoryToFile();

		/* 
		 * \brief trajectory cost after evaluateTrajectory()
	 			  We use the 'cost' to rank  
		 */
		double cost;
		
		/* 
		 * \brief Feasibility of the path, we use it together with 
		 		  the cost to construct a composite key 
		 */
		bool feasibility;

		/* 
		 * \brief colliding time from beginning of trajectory if it is
		 		  infeasible
		 */
		double colliding_time_from_beginning;

		/* 
		 * \brief trajectory start and end configurations
		 */
		armCfg start_config, end_config;

		/* 
		 * \brief trajectory knot configurations
		 */
		std::vector<armCfg> knot_cfgs;

		/* 
		 * \brief duration for each configuration in this trajectory 
		 *        (time from the previous configuration)
		 *        the starting configuration has zero
		 */
		std::deque<double> cfgs_durations; //Duration from previous state (including start_config + knot configs + end_config)
		
		/* 
		 * \brief velocites for each configuration in this trajectory 
		 */
		std::vector<std::vector<double> > cfgs_velocites;

	private:
		/* 
		 * \brief number of Dofs
		 */
		int nDofs;

		/* 
		 * \brief trajectory ID
		 */
		int trajID;

		/* 
		 * \brief joint names
		 */
		std::vector<std::string> jnt_names;

		/* 
		 * \brief joint velocity max
		 */
		std::vector<double> jnt_vel_max;

		boost::shared_ptr<rampSetting> ramp_setting_ptr;
		
		boost::shared_ptr<rampSensing> ramp_sensing_ptr;

		/* 
		 * \brief trajectory information in the form of trajectory_msgs
		 */
		trajectory_msgs::JointTrajectory m_traj_msg;

		/* 
		 * \brief cubic polynomials coefficients
		 */
		std::vector<cubicPolynomial> splines;

		/* 
		 * \brief cost function weights
		 */
		costFunctionWeights cost_weights;

		/* 
		 * \brief moment of inertia for each joint
		 */
		std::vector<double> joints_moment_of_inertia;

		/* 
		 * \brief cost function normalization factor
		 */
		costFunctionNormalizationFactor cost_nrm_factors;

		double energy_cost, time_cost, manipulability_cost, infeasibility_cost, task_cstr_cost, smoothness_cost, goal_reach_cost;
		double tc_til_knot1;
		/** 
		 * \brief Generate trajectory from path (add time stamps and compute velocites)
		 *        This uses the trejectory generator from MoveIt.
		 *        This uses IterativeParabolicTimeParameterization to generate a time-optimal trajectory. 
		 *        One can also use spline functions (MoveIt provided but not incorporated here.)
		 *        It fills information for cfgs_durations and cfgs_velocities
		 * \param 
		 * \return void
		 */
		void generateTrajectoryFromPathNumerically(const std::vector<double> &start_cfg_vel);
		
		/** 
		 * \brief Generate trajectory from path (add time stamps and compute velocites)
		 *        This analytically constructs cubic polynomials.
		 *        It fills information for cfgs_durations and cfgs_velocities
		 * \param start_cfg velocity and end_cfg velocity
		 * \return void
		 */
		void generateTrajectoryFromPathAnalytically(std::vector<double> &start_cfg_vel, std::vector<double> &end_cfg_vel);

		/* 
		 * \brief compute time intervals between two cfgs based on the 
		 		  velocity of the slowest joint.
		 		  This is to be used next for generating trajectory 
		 		  analytically using cubic polynomials
		 */
		void computeTimeIntervalsBasedOnSlowestJoint(double jt_vmax_scale=1.0);

		void getTrajectoryTaskCstrCostUntilKnot1();
};

typedef boost::shared_ptr<rampTrajectory> rampTrajectoryPtr;

#endif
