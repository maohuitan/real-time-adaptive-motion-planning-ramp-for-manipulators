/*
 * ramp_core.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_CORE_H
#define RAMP_CORE_H

#include <ros/ros.h>

#include "ramp_core/ramp_common.h"
#include "ramp_population/ramp_population.h"
#include "ramp_setting/ramp_setting.h"
#include "ramp_goal/ramp_goal.h"
#include "ramp_trajectory/ramp_trajectory.h"
#include "ramp_operators/ramp_operators.h"
#include "ramp_rendering/ramp_rendering.h"
#include "ramp_sensing/ramp_sensing.h"
#include "ramp_learning/ramp_learning.h"
#include "ramp_control/ramp_control.h"

#include <moveit_msgs/ApplyPlanningScene.h>
#include <sensor_msgs/JointState.h>
#include <geometric_shapes/shape_operations.h>
/* 
 * \brief global variables: ramp parameters and experiment stats
 */
rampParameters ramp_params;
statistics stats;

class rampCore
{
	public:
		rampCore();
		/** 
		 * \brief initialize ramp
		 */
		void initialize();
		/** 
		 * \brief do offline planning cycles until the best trajectory 
		 * in population has not improved in 100 iterations (for example)
		 */
		void offlineRun();
		/** 
		 * \brief do online run
		 */
		void run();
		/** 
		 * \brief show population info
		 */
		void showPopulation(bool verbose=false);
		
		/** 
		 * \brief show eef pose along executed traj in rviz
		 * \param collected eef poses
		 */
		void visualizeEefPosesDuringMotion(const geometry_msgs::PoseArray &eef_pose_array);
		void visualizeCurrentPlanningScene();
		/** 
		 * \brief show experiment stats
		 */
		void showExperimentStats(const geometry_msgs::PoseArray &eef_pose_array);
		/** 
		 * \brief robot state subscriber callback functions
		 */
		void robotStateCallback(const sensor_msgs::JointState::ConstPtr &msg);
		void robotStateHardWareCallback(const sensor_msgs::JointState::ConstPtr &msg);
		
	private:
		/* 
		 * \brief ramp node handle
		 */
		ros::NodeHandle nh;
		/* 
		 * \brief current robot cfg (updated by sensing cycle)
		 */
		armCfg robot_cfg;
		/* 
		 * \brief current robot cfg vel (updated by sensing cycle)
		 */
		armCfg robot_cfg_vel;
		/* 
		 * \brief kinematic setting of the robot running RAMP
		 */
		boost::shared_ptr<rampSetting> ramp_setting_ptr;
		/* 
		 * \brief joint cfg sampler
		 */
		boost::shared_ptr<rampSampling> ramp_sampling_ptr;
		/* 
		 * \brief maintain population of trajectories and 
		 * 		  apply operations on population
		 */
		boost::shared_ptr<rampPopulation> ramp_population_ptr;
		/* 
		 * \brief provide sensing topics for outside sensors
		 * 		  to publish and update obstacle information
		 */
		boost::shared_ptr<rampSensing> ramp_sensing_ptr;
		/* 
		 * \brief send trajecotry for execution in gazebo 
		 *        and render other auxilary stuff in rviz
		 */
		boost::shared_ptr<rampRendering> ramp_rendering_ptr;
		/* 
		 * \brief send trajecotry for execution on hardware
		 */
		boost::shared_ptr<rampControl> ramp_control_ptr;
		/* 
		 * \brief ramp genetic operators
		 */
		boost::shared_ptr<rampOperators> ramp_oprt_ptr;
		/* 
		 * \brief read in saved trajectories from previous runs to 
		 * 		  be used in next runs (sequential knowledge transfer)
		 */
		boost::shared_ptr<rampLearning> ramp_learn_ptr;
		/* 
		 * \brief maintain and check if goal has been reached
		 */
		rampGoal ramp_goal, ramp_sub_goal;
		/* 
		 * \brief subscribers updating robot running ramp states
		 * 		  robot_state_sub is configured to receive info from gazebo or hardware
		 *        robot_hw_state_sub is configured to receive info from hardware 
		 * 		  robot_hw_state_sub is useful when we only let hardware execute the trajectories
		 * 		  being executed in gazebo and collect joint states from hardware
		 */
		ros::Subscriber robot_state_sub, robot_hw_state_sub;
		/* 
		 * \brief parameter for configuring robot_state_sub
		 * 		  set from ramp_params
		 */
		std::string robot_state_topic;
		/* 
		 * \brief load ramp parameters from parameter server
		 */
		void loadParameters();
		/* 
		 * \brief do control cycle once
		 */
		void doControl();
		/* 
		 * \brief do planning cycle once
		 */
		void doPlanning();
		/* 
		 * \brief true if the object being manipulated is considered as
		 * 		  an obstacle in planning (detachment happened in drawer 
		 * 		  or water cup placed down)
		 */
		bool manipulate_obj_obstacle_status;

		/* 
		 * \brief water cup conflict resolution
		 */
		bool first_time_ntc1;
		int start_ctrl_cyc;
		geometry_msgs::Pose safe_pose;
		void dropThenPickCup();
		bool jacobianBasedMotionGeneration(const armCfg &start_cfg, const geometry_msgs::Pose &target_eef_pose, 
										   const int &num_of_steps, int &error_code, moveit_msgs::RobotTrajectory &trajectory);
};


#endif
