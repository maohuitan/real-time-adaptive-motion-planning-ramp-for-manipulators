/*
 * ramp_common.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_COMMON_H
#define RAMP_COMMON_H

// C++
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <stdlib.h>

// C++ high resolution timer
#include <ctime>
#include <chrono>

typedef std::vector<double> armCfg;
// ramp project related
#include "ramp_util/ramp_util.h"

struct rampParameters
{
	std::string test_case_name;
	std::vector<int> population_size;
	std::vector<std::string> population_name;
	std::vector<std::string> population_type;
	int init_traj_knot_size;
	int min_offline_iterations;
	int max_offline_iterations;
	double control_frequency;

	// ramp cost function weights
	double traj_energy_weight;
	double traj_energy_nrm_factor;
	double traj_time_weight;
	double traj_time_nrm_factor;
	double traj_manipulability_weight;
	double traj_manipulability_nrm_factor;
	double traj_task_cstr_weight;
	double traj_task_cstr_nrm_factor;
	double traj_smoothness_weight;
	double traj_smoothness_nrm_factor;

	// ramp terminal prints verbose level
	bool verbose;

	// ramp trajectory generation option
	double traj_jnt_vel_max_scale;
	std::string trajectory_generation_option;
	
	// ramp collision check options
	double traj_collison_checking_time_resolution;
	double traj_evaluate_time_resolution;
	double traj_evaluate_max_time;
	double traj_collision_checking_start_time;
	double traj_collison_checking_max_time;
	double population_collison_checking_max_time;

	// robot running ramp settings
	std::string robot_state_topic;
	std::string arm_joint_group_name;
	std::string arm_eef_link_name;
	std::vector<std::string> arm_joint_names;
	std::string follow_joint_trajectory_action_gazebo;
	std::string follow_joint_trajectory_action_hardware;
	bool is_hardware_enabled;

	// ramp sensing options
	std::string sensing_topic;
	// arm-type obstacle info
	std::string arm_obs_description_name;
	std::string arm_obs_sensing_topic;
	bool use_published_velocity;

	// ramp planning goal
	std::vector<double> start_eef_pose;
	std::vector<double> goal_eef_pose;

	// task constraints related
	bool is_task_constrained;
	std::string task_constraint_axis;
	double task_constraint_tolerance;
	std::vector<double> eef_sampling_space;
	std::string push_object_name;

	// set internally in ramp program (no need in yaml)
	bool is_online_run;
	float on_task_cstr_time;
};

struct statistics
{
	int num_of_ctrl_cyc;
	int num_of_plan_cyc;
	int num_of_offline_plan_cyc;
	double ramp_initialization_time;
	double ramp_offline_run_time;
	double ramp_online_run_time;
	double cum_planning_cyc_time;
	double cum_ctrl_cyc_time;
	int num_of_traj_switches;
	int traj_switch_due_to_infeas;
	int traj_switch_due_to_better_traj;
	int num_of_ctrl_cyc_running_ntc_traj;
	int num_of_ctrl_cyc_running_tc_traj;
	std::vector<int> ctrl_cyc_ind_whole_pop_infeas;
	std::vector<armCfg> robot_cfg_vec;
	std::vector<armCfg> robot_cfg_vel_vec;
	int num_of_ins_chg_oper;
	int num_of_success_ins_chg_oper;
	std::vector<armCfg> robot_cfg_hw_vec;
	std::vector<armCfg> robot_cfg_hw_vel_vec;
	std::vector<std::pair<std::string, int> > exe_traj_per_ctrl_cyc; // <sub_pop_type, exe_traj_id> per control cycle
};


#endif
