/*
 * ramp_test.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_TEST_H
#define RAMP_TEST_H

#include <ros/ros.h>

#include "ramp_core/ramp_common.h"
#include "ramp_setting/ramp_setting.h"
#include "ramp_trajectory/ramp_trajectory.h"
#include "ramp_operators/ramp_operators.h"
#include "ramp_rendering/ramp_rendering.h"
#include "ramp_obstacle/ramp_obstacle.h"
#include "ramp_util/ramp_util.h"

#include <moveit_msgs/ApplyPlanningScene.h>

class rampTest
{
	public:
		rampTest();
		void setRampSettingPtr(boost::shared_ptr<rampSetting> &r_set_p);
		void setRampSamplingPtr(boost::shared_ptr<rampSampling> &r_samp_p);
		void setRampTrajectoryPtr(boost::shared_ptr<rampTrajectory> &r_traj_p);
		void setRampOperatorsPtr(boost::shared_ptr<rampOperators> &r_oprt_p);
		void setRampRenderingPtr(boost::shared_ptr<rampRendering> &r_rdr_p);

	private:
		boost::shared_ptr<rampSetting> ramp_setting_ptr;
		boost::shared_ptr<rampSampling> ramp_sampling_ptr;
		boost::shared_ptr<rampTrajectory> ramp_traj_ptr;
		boost::shared_ptr<rampOperators> ramp_oprt_ptr;
		boost::shared_ptr<rampRendering> ramp_rdr_ptr;
};


#endif
