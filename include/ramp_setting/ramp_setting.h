/*
 * ramp_setting.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_SETTING_H
#define RAMP_SETTING_H

#include <map>

// ROS
#include <ros/ros.h>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/kinematics_metrics/kinematics_metrics.h>
// for reading joint bounds
#include <moveit/robot_model/joint_model.h>

#include "ramp_core/ramp_common.h"

extern rampParameters ramp_params;

/*
	class rampSetting maintains information on 
	1) # DOF of the robot considered for planning
	2) robot kinematic chain
	3) robot kinematic limits(joint angle position/velocity/acceleration limits)
*/

class rampSetting
{
	public:
		rampSetting();
		~rampSetting();
		/** 
		 * \brief load robot model from parameter server
		 * \return void
		 */
		void loadRobotModel();

		/** 
		 * \brief set the kinematic_state to the default configuration.
		          kinematic_state is the internal state seen by MoveIt 
		          planners and the planning scene.
		 * \return void
		 */
		void setDefaultJointConfiguration();

		/** 
		 * \brief set the kinematic_state to a random configuration.
		          kinematic_state is the internal state seen by MoveIt 
		          planners and the planning scene.
		 * \return void
		 */
		void setRandomJointConfiguration();

		/** 
		 * \brief set the kinematic_state to specified configuration.
		          kinematic_state is the internal state seen by MoveIt 
		          planners and the planning scene.
		 * \return void
		 */
		void setJointConfiguration(const armCfg &target_cfg);

		/** 
		 * \brief get the default configuration of the kinematic state
		 * \return void
		 */
		void getDefaultJointConfiguration(armCfg &joint_values);

		/** 
		 * \brief get the current configuration of the kinematic state
		 * \return void
		 */
		void getCurrentJointConfiguration(armCfg &joint_values);

		/** 
		 * \brief get eef pose. eef link name is a preset class member
		 * \return void
		 */
		void getEefPose(geometry_msgs::Pose &eef_pose);
		void getEefPose(Eigen::Affine3d &eef_pose);
		/** 
		 * \brief get an IK solution given eef pose and the solution obeys joint limits
		 * \return true or false
		 */
		bool getIKJointConfiguration(armCfg &joint_values, const geometry_msgs::Pose &eef_pose);

		/** 
		 * \brief get all IK solutions given eef pose and they obey joint limits
		 * \return true or false
		 */
		bool getAllIKJointConfigurations(std::vector<armCfg> &vec_joint_values, const geometry_msgs::Pose &eef_pose);

		/** 
		 * \brief get number of dofs of the kinematic chain
		 * \return void
		 */
		void getNumberOfDofs(int &numOfDofs);

		/** 
		 * \brief get a pointer of kinematic_model
		          caution: this may potentionally change the 
		          internal kinematic_state. This function so far
		          has only been used to generateTrajectoryFromPath()
		          in rampTrajectory.
		 * \return void
		 */
		robot_model::RobotModelPtr& getRobotModelPtr();

		/** 
		 * \brief set joint_names for each joint in the kinematic chain. 
		          only active joints are included here (no dummy joints)
		 * \return void
		 */
		void setJointNames(const std::vector<std::string> &jnt_names);

		/** 
		 * \brief get joint names
		 * \return joint names
		 */
		const std::vector<std::string>& getJointNames();

		/**  
		 * \brief get joint model group name
		 * \return joint model group name
		 */
		const std::string& getJointModelGroupName();

		/** 
		 * \brief get bounds for each joint. Bounds include
		 *        position, velocity, acceleration
		 * \return variable bounds for each joint
		 */
		const moveit::core::VariableBounds& getJointBounds(const std::string &joint_name);

		/** 
		 * \brief print bounds info
		 */
		void printJointBoundsInfo();

		/** 
		 * \brief check if robot is colliding through moveit planning_scene_monitor
		 */
		bool isRobotColliding(const armCfg &cfg, const boost::shared_ptr<planning_scene::PlanningScene>& ps);
		bool isRobotCollidingThreadSafe(const armCfg &cfg, const boost::shared_ptr<planning_scene::PlanningScene>& ps);

		/** 
		 * \brief get manipulability = sqrt(det(JJ^T))
		 */
		double getManipulabilityIndex(const armCfg &cfg);
		Eigen::MatrixXd getJacobian(const armCfg &cfg);
		bool isInJointAngleRanges(const armCfg &cfg);

		/** 
		 * \brief set specified task constraint pose
		 		  if ramp_params.is_task_constrained is true
		 */
		void setTaskCstrPose(const geometry_msgs::Pose &tc_p);

		/** 
		 * \brief compare pose deviation as an evaluation of
		 		  task constraint violation
		 */
		double compareCurrentPose(const geometry_msgs::Pose &cur_p);

	private:
		std::vector<std::string> joint_names;
		std::string joint_group_name;
		std::string eef_link_name;
		robot_model_loader::RobotModelLoader robot_model_loader;

		/** 
		 * \brief specifies the kinematic chain
		 */
		robot_model::RobotModelPtr kinematic_model;

		/** 
		 * \brief kinematic state of the kinematic model 
		 *        this is the internal representation used by moveit for
		 *        planning or collision checking
		 */
		robot_state::RobotStatePtr kinematic_state;

		/** 
		 * \brief joint model group
		 */
		const robot_state::JointModelGroup* joint_model_group;

		/** 
		 * \brief map from joint name to VariableBound
		 */
		std::map<std::string, moveit::core::VariableBounds> joint_bounds_maps;

		/** 
		 * \brief set joint bounds mapping from joint names
		 *        This is meant to be only called once in constructor
		 */
		void setJointBoundsMap();

		boost::shared_ptr<kinematics_metrics::KinematicsMetrics> kine_metrics_ptr;

		geometry_msgs::Pose task_cstr_pose;
};

#endif
