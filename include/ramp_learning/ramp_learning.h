/*
 * ramp_learning.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_LEARNING_H
#define RAMP_LEARNING_H

#include <fstream>
#include <ros/ros.h>
#include "ramp_core/ramp_common.h"
#include "ramp_trajectory/ramp_trajectory.h"

/* global variable */
extern rampParameters ramp_params;
extern statistics stats;

class rampLearning
{
	public:
		rampLearning(const int n_dofs);
		/** 
		 * \brief read in knot cfgs from a traj
		 * \param file name 
		 * \return void
		 */
		void readInTrajectories(const std::string &file_name);
		/** 
		 * \brief set cfgs to the read-in cfgs
		 * \param cfgs to be filled in
		 * \return void
		 */
		void getSavedKnotCfgs(std::vector<armCfg> &cfgs);
		
	private:
		int nDofs_;
		/* 
		 * \brief saved knot cfgs to be filled 
		 */
		std::vector<armCfg> saved_knot_cfgs_;
};


#endif
