/*
 * ramp_obstacle.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_OBSTACLE_H
#define RAMP_OBSTACLE_H

#include "ramp_core/ramp_common.h"
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/robot_model/link_model.h>
#include <geometric_shapes/shapes.h>

extern rampParameters ramp_params;

class rampObstacleRobotArm
{
	public:
		rampObstacleRobotArm(const std::string &robot_desp_name);
		~rampObstacleRobotArm();

		void setSensedRobotState(const armCfg &cfg, const armCfg &cfg_v);
		void updateInternalState();

		/* boundbing boxes for all links */
		void createBoundingBoxesForLinks();
		void getBoundingBoxesForLinksAtTimeT(double t);
		void getBoundingBoxesForLinksAtCfg(const armCfg &cfg);

		const std::vector<shapes::ShapeConstPtr>& getLinkShapes();
		const EigenSTL::vector_Affine3d& getLinkShapePoses();

	private:
		robot_model_loader::RobotModelLoader arm_obs_model_loader;
		robot_model::RobotModelPtr arm_obs_kine_model;
		robot_state::RobotStatePtr arm_obs_kine_state;
		const robot_state::JointModelGroup* arm_obs_jnt_model_group;
		std::vector<const robot_model::LinkModel*> links;

		armCfg cur_cfg, cur_cfg_vel;
		armCfg sensed_cfg, sensed_cfg_vel;

		int cfg_update_cnt;
		ros::Time last_cfg_update_time;

		Eigen::Affine3d tf_from_arm_base_to_world;
		std::vector<shapes::ShapeConstPtr> link_shapes;
		EigenSTL::vector_Affine3d link_shape_poses; 
};

typedef boost::shared_ptr<rampObstacleRobotArm> rampObstacleRobotArmPtr;

class rampObstacle
{
	public:
		rampObstacle(const std::string &obs_type, const std::string &obs_id, const std::string &obs_frame,
					 const geometry_msgs::Pose &obs_pose, const Eigen::Vector3f &dimension);
		~rampObstacle();
		const std::string& getID();
		const std::string& getType();
		const Eigen::Vector3f& getDimensions();
		const geometry_msgs::Pose& getCurrentPose();
		const shapes::ShapePtr& getPrimitiveShape();
		/* 
		 * \brief save sensed pose from ramp_sensing to member sensed_pose
		 * 		  without changing acutual pose in the class
		 */
		void setSensedPose(const geometry_msgs::Pose &obs_pose);
		/* 
		 * \brief save sensed twist from ramp_sensing to member sensed_twist
		 * 		  without changing acutual twise in the class
		 */
		void setSensedVelocity(const geometry_msgs::Twist &obs_twist);
		/* 
		 * \brief save sensed dimension from ramp_sensing to member sensed_dimens
		 * 		  without changing acutual dimension in the class
		 */
		void setSensedDimension(const Eigen::Vector3f &dim);
		/* 
		 * \brief update actual pose, twist and dimens using sensed_pose, sensed_twist and sensed_dimens
		 */
		void updateInternalState();
		/* 
		 * \brief set obstacle pose at time T into future in planning scene
		 */
		void getPoseAtTimeT(double time_elapsed, geometry_msgs::Pose &new_pose);

		/* handle arm-type obstacle */
		boost::shared_ptr<rampObstacleRobotArm> ob_arm_ptr;
	
	private:
		std::string id;
		std::string type;
		std::string frame;
		/* actual pose, twist and dimension */
		geometry_msgs::Pose pose; 
		geometry_msgs::Twist twist; 
		Eigen::Vector3f dimens;
		/* latest sensed pose, twist and dimension*/
		geometry_msgs::Pose sensed_pose; 
		geometry_msgs::Twist sensed_twist; 
		Eigen::Vector3f sensed_dimens;

		shapes::ShapePtr primitive_shape;

		int twist_update_cnt;
		ros::Time last_twist_update_time;
};

#endif
