/*
 * ramp_control.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_CONTROL_H
#define RAMP_CONTROL_H

#include "ramp_core/ramp_common.h"
#include "ramp_trajectory/ramp_trajectory.h"
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>

// action for FollowJointTrajectoryAction
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>

extern rampParameters ramp_params;

class rampControl
{
	public:
		rampControl();
		~rampControl();
		
		/** 
		 * \brief send a trajectory for execution on hardware using specified controllers by a ros action
		 *        rviz re-displays the state of the robot. 
		 *        The execution respects the time stamps and the spcified positons and velocites
		 * \param ramp trajectory to be executed and joint names
		 * \return void
		 */
		void executeJointTrajectoryOnHardware(rampTrajectory &ramp_traj, const std::string &traj_sub_pop_name);
	
	private:
		/** 
		 * \brief FollowJointTrajectoryAction provided by ros-industrial.
		 */
		actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> fol_jnt_traj_action_client;

		/** 
		 * \brief ID of the trajectory currently being executed
		 */
		int cur_exe_traj_id;
		std::string cur_exe_traj_sub_pop_name;
};


#endif
