/*
 * ramp_goal.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_GOAL_H
#define RAMP_GOAL_H

#include "ramp_core/ramp_common.h"

class rampGoal
{
	public:
		rampGoal();
		/** 
		 * \brief Set eef pose as goal
		 * \param goal eef pose
		 * \return void
		 */
		void setGoalEefPose(const geometry_msgs::Pose &pose);
		/** 
		 * \brief Set current eef pose, used internally to compare to see if goal is reached
		 * \param current eef pose
		 * \return void
		 */
		void setCurrentEefPose(const geometry_msgs::Pose &pose);
		/** 
		 * \brief check if goal is reached
		 * \param void
		 * \return void
		 */
		bool isGoalReached();

	private:
		geometry_msgs::Pose goal_pose; 
		geometry_msgs::Pose current_pose; 
};


#endif
