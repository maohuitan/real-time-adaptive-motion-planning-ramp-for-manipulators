/*
 * ramp_sensing.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_SENSING_H
#define RAMP_SENSING_H

#include "ramp_obstacle/ramp_obstacle.h"
#include "ramp_core/ramp_common.h"
#include "gazebo_msgs/ModelStates.h"
#include <sensor_msgs/JointState.h>
#include <map>
#include <moveit/planning_scene/planning_scene.h>

extern rampParameters ramp_params;

class rampSensing
{
	public:
		rampSensing(robot_model::RobotModelPtr &robot_kinematic_model);
		/* 
		 * \brief add obstacle to the internal obstacle vector
		 */
		void addObstacleToPlanningScene(rampObstacle &rob);
		/* 
		 * \brief ramp_sensing callback function for non-arm type obstacles
		 */
		void callback(const gazebo_msgs::ModelStates::ConstPtr &msg);
		/* 
		 * \brief ramp_sensing callback function for arm type obstacles
		 */
		void obsArmCallback(const sensor_msgs::JointState::ConstPtr &msg);
		void optiTrackCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
		/* 
		 * \brief set obstacles at time T into future in planning scene by calling
		 * 		  ramp_obstacle function
		 * \return
		 */
		void putObsPosesAtTimeT(const float &t);
		/* 
		 * \brief for each obstacle in internal vector
		 * 		  update actual pose, twist and dimens using sensed_pose, sensed_twist and sensed_dimens
		 */
		void updateObstacleInternalState();
		/* 
		 * \brief add/remove obstacle(specified by ID) into/from planning scene
		 */
		void addObsIntoPlanningSceneByID(const std::string &obs_id);
		void removeObsFromPlanningSceneByID(const std::string &obs_id);
		bool hasObjectByID(const std::string &obs_id);

		const boost::shared_ptr<planning_scene::PlanningScene>& getPlanningScenePtr();

	private:
		std::vector<rampObstacle> obstacles;
		ros::Subscriber sub, sub_obs_arm, sub_optitrack;;
		std::string sensing_topic;
		int num_of_sensing_cyc;
		std::map<std::string, int> obs_id_map;
		boost::shared_ptr<planning_scene::PlanningScene> planning_scene_ptr;
};


#endif
