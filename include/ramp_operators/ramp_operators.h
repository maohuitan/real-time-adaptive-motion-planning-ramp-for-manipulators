/*
 * ramp_operators.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_OPERATORS_H
#define RAMP_OPERATORS_H

#include "ramp_trajectory/ramp_trajectory.h"
#include "ramp_sampling/ramp_sampling.h"
#include "ramp_core/ramp_common.h"

extern rampParameters ramp_params;
extern statistics stats;

class rampOperators
{
	public:
		rampOperators(const boost::shared_ptr<rampSampling> &ramp_spl_ptr);
		void registerTrajectory(const boost::shared_ptr<rampTrajectory> &candidate_traj_1);
		void dropTrajectory();
		bool applyRandomInsert();
		bool applyTaskConstrainedInsert();
		bool applyDelete();
		bool applyRandomChange();
		bool applyTaskConstrainedChange();
		bool applySwap();
		bool applyCrossOver(rampTrajectory &candidate_traj_2);
		
		/** 
		 * \brief stop operator should only be applied on a time-stamped trajectory
		          i.e. after rampTrajectory.generateTrajectoryFromPath() is called.
		 */
		void applyStop();

	private:
		boost::shared_ptr<rampTrajectory> traj_ptr;
		boost::shared_ptr<rampSampling> ramp_sampling_ptr;
};


#endif
