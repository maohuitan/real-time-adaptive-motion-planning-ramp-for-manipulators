# Real-time Adaptive Motion Planning (RAMP) for Manipulators

This repository implements the algorithm of Real-time Conflict Resolution of Task-constrained Manipulator Motion in Unforeseen Dynamic Environments. 

* Key words: real-time robot motion planning, task constraints, conflict resolution, dynamic obstacle avoidance
* Paper: TRO 2019 https://ieeexplore.ieee.org/abstract/document/8759084
* Dependency: ROS and MoveIt!

## ramp_core
* initialization (with ramp_setting ramp_goal)
* planning cycle
* control cycle
* sensing cycle
* rendering (graphic display with ramp_rendering)

## ramp_setting
* specify kinematic chain (through MoveIt!)
* specify kinematic constraints
* collsion checking with FCL (through MoveIt!)

## ramp_goal
* specify goal pose for eef and other task constraints
* specify evaluation function

## ramp_trajectory
* trajecotry representation
* start/end arm configuration
* knot configurations
* duration 
* trajectory generation from paths with velocity/acceleration limits
* trajectory evaluation 
* feasibility checking

## ramp_operators
* genetic operators insert/delete/change/swap/crossover/stop/task-constrained insert/task-constrained change

## ramp_population 
* diversity creation
* diversity presvation 

## ramp_sensing 
* sense robot current state
* sense obstacle and predict their motion (with ramp_obstacle)

## ramp_control
* interface with robot hardware controller

## ramp_obstacle
* obstacle pose and velocity
* obstacle representation for collision checking

## ramp_sampling
* sample random joint configurations
* sample joint configurations given eef pose
* data structures for constrained pose (under soft constraints)

## ramp_rendering
* maintain simulation scene
* visualize robot info/obstacle info/planning scene/

## ramp_util
* utility functions 

## ramp_learning
* reserved for future extension





